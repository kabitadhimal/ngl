<!--  ---------------------------------------------------------     -->
<!--  NOTE: Ajoutez l'élément <META> suivant à votre page <HEAD>. Si nécessaire,       -->
<!--  modifiquez le paramètre charset afin de préciser l'ensemble        -->
<!--  de caractères de votre page HTML.             -->
<!--  ---------------------------------------------------------     -->

 <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
<!--  ---------------------------------------------------------------      --> <!--  NOTE: Ajoutez l'élément <FORM> suivant à votre page.    --> <!--  -------------------------------------------------------------       -->

<html>
<head>

</head>
<body style="background-color:White">

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://secure.inescrm.com/styles/sites.css">
<link rel="stylesheet" type="text/css" href="https://secure.inescrm.com/styles/bande.css">
<script type="text/javascript" src="https://secure.inescrm.com/js/Base64.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/AjaxHelper.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/form_validation.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/md5.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/jcap.js"></script>
<script type="text/javascript">function checkformContactForm(){ var str;
        /*if (str=validateMandatoryTextField('TextBox_73676','Veuillez saisir une valeur pour le champ obligatoire "Société > Société - Nom"',1)){ alert(str); return false; };*/
        if (str=validateMandatoryTextField('TextBox_73677','Veuillez saisir une valeur pour le champ obligatoire "Contact > Nom du contact"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('TextBox_73678','Veuillez saisir une valeur pour le champ obligatoire "Contact > Prénom"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('Select_73679','Veuillez saisir une valeur pour le champ obligatoire "Société > Pays"',1)){ alert(str); return false; };
        if (str=validateEmail('TextBox_73680','Format de mail incorrect pour "Contact > Email 1"','Veuillez saisir une valeur pour le champ obligatoire "Contact > Email 1"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('TextArea_73681','Veuillez saisir une valeur pour le champ obligatoire "Website message > Message"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('TextBox_73682','Veuillez saisir une valeur pour le champ obligatoire "Details > Job title"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('TextBox_73684','Veuillez saisir une valeur pour le champ obligatoire "Société > Code postal"',1)){ alert(str); return false; };
        /*if (str=validateMandatoryTextField('TextArea_73683','Veuillez saisir une valeur pour le champ obligatoire "Société > Adresse 1"',1)){ alert(str); return false; };*/
        /*

        */
        if (str=validateMandatoryTextField('TextBox_73685','Veuillez saisir une valeur pour le champ obligatoire "Société > Société - Ville"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('TextBox_73686','Veuillez saisir une valeur pour le champ obligatoire "Contact > Téléphone de Contact"',1)){ alert(str); return false; };
        if (str=validateMandatoryTextField('Checkbox_76010','Veuillez saisir une valeur pour le champ obligatoire "Website message > Newsletter"',0)){ alert(str); return false; };
        document.getElementById('submit').disabled = true;
        var capOK = jcap();
        if (!capOK)
            document.getElementById('submit').disabled = false;
        return capOK }</script>

<section class="ipro-block ipro-block--form ipro-block--contactForm-new">
    <div class="ipro-container">
        <div class="ipro-form__wrap">
            <form id="WebLeadsForm_NGLCRM" action="https://extend.inescrm.com/InesWebFormHandler/Main.aspx" method="POST" onsubmit="return checkformContactForm()" >

                <input type=hidden name="controlid" id="controlid" value="1989267957" />
                <input type=hidden name="oid" id="oid" value="e13f94c9-f977-495a-aaa4-590676109ce0" />
                <input type=hidden name="formid" id="formid" value="4104" />
                <input type=hidden name="retURL" id="retURL" value="www.ngl-group.com" />
                <input type=hidden name="data" id="data" value="" />
                <input type=hidden name="Alias" id="Alias" value="NGLCRM" />
                <!--  ----------------------------------------------------------------------      -->

                <div class="row form-gap">
                    <div class="col-sm-6">
                        <!--<label id="Label_2" > Pr&#233;nom <span class="texte4">*</span></label>-->
                        <input id="TextBox_73678" class="txt" name="TextBox_73678" maxlength="40" size="45" placeholder="First name new" type="text"  required/>
                    </div>

                    <div class="col-sm-6">
                        <!--<label id="Label_1" > Nom du contact <span class="texte4">*</span></label>-->
                        <input id="TextBox_73677" class="txt" name="TextBox_73677" maxlength="40" size="45" placeholder="Last name" type="text"  required />
                    </div>
                </div>

                <div class="row form-gap">
                    <div class="col-sm-6">
                        <!--<label id="Label_6" > Job title</label>-->
                        <input id="TextBox_73682" class="txt" name="TextBox_73682" maxlength="100" size="45" placeholder="Job Title *" type="text"  required />
                    </div>

                    <div class="col-sm-6">
                        <!--<label id="Label_0" > Soci&#233;t&#233; - Nom <span class="texte4">*</span></label>-->
                        <input id="TextBox_73676" class="txt" name="TextBox_73676" maxlength="50" size="45" placeholder="Company name *" type="text"   required/>
                    </div>

                </div>

                <div class="row form-gap">
                    <div class="col-sm-6">
                        <!--<label id="Label_7" > Adresse 1</label>-->
                        <!--<textarea id="TextArea_73683" name="TextArea_73683" class="txt" placeholder=" Adresse 1" rows="2" onKeyDown="javascript:checkLen('TextArea_73683',200);" onKeyUp="javascript:checkLen('TextArea_73683',200);" ></textarea>-->
                        <input type="text" id="TextArea_73683" name="TextArea_73683" class="txt" placeholder="Address *" rows="2" onKeyDown="javascript:checkLen('TextArea_73683',200);" onKeyUp="javascript:checkLen('TextArea_73683',200);" required >
                    </div>

                    <div class="col-sm-6">
                        <!--<label id="Label_8" > Code postal</label>-->
                        <input id="TextBox_73684" class="txt" name="TextBox_73684" maxlength="25" size="45" placeholder="Postal code *" type="text" required />
                    </div>

                </div>

                <div class="row form-gap">
                    <div class="col-sm-6">
                        <!--<label id="Label_9" > Soci&#233;t&#233; - Ville</label>-->
                        <input id="TextBox_73685" class="txt" name="TextBox_73685" maxlength="20" size="45" placeholder=" City"  type="text" required />
                    </div>

                    <div class="col-sm-6">
                        <!--<label id="Label_3" > Pays <span class="texte4">*</span></label>-->
                        <select id="Select_73679" name="Select_73679" class="ipro-form__dropdown--primary"  required >
                            <option> Country </option>
                            <option id="Select_73679_AD" value="AD"  >AD</option>
                            <option id="Select_73679_AE" value="AE"  >AE</option>
                            <option id="Select_73679_AF" value="AF"  >AF</option>
                            <option id="Select_73679_AG" value="AG"  >AG</option>
                            <option id="Select_73679_AI" value="AI"  >AI</option>
                            <option id="Select_73679_AL" value="AL"  >AL</option>
                            <option id="Select_73679_AM" value="AM"  >AM</option>
                            <option id="Select_73679_AO" value="AO"  >AO</option>
                            <option id="Select_73679_AQ" value="AQ"  >AQ</option>
                            <option id="Select_73679_AR" value="AR"  >AR</option>
                            <option id="Select_73679_AS" value="AS"  >AS</option>
                            <option id="Select_73679_AT" value="AT"  >AT</option>
                            <option id="Select_73679_AU" value="AU"  >AU</option>
                            <option id="Select_73679_AW" value="AW"  >AW</option>
                            <option id="Select_73679_AX" value="AX"  >AX</option>
                            <option id="Select_73679_AZ" value="AZ"  >AZ</option>
                            <option id="Select_73679_BA" value="BA"  >BA</option>
                            <option id="Select_73679_BB" value="BB"  >BB</option>
                            <option id="Select_73679_BD" value="BD"  >BD</option>
                            <option id="Select_73679_BE" value="BE"  >BE</option>
                            <option id="Select_73679_BF" value="BF"  >BF</option>
                            <option id="Select_73679_BG" value="BG"  >BG</option>
                            <option id="Select_73679_BH" value="BH"  >BH</option>
                            <option id="Select_73679_BI" value="BI"  >BI</option>
                            <option id="Select_73679_BJ" value="BJ"  >BJ</option>
                            <option id="Select_73679_BL" value="BL"  >BL</option>
                            <option id="Select_73679_BM" value="BM"  >BM</option>
                            <option id="Select_73679_BN" value="BN"  >BN</option>
                            <option id="Select_73679_BO" value="BO"  >BO</option>
                            <option id="Select_73679_BR" value="BR"  >BR</option>
                            <option id="Select_73679_BS" value="BS"  >BS</option>
                            <option id="Select_73679_BT" value="BT"  >BT</option>
                            <option id="Select_73679_BV" value="BV"  >BV</option>
                            <option id="Select_73679_BW" value="BW"  >BW</option>
                            <option id="Select_73679_BY" value="BY"  >BY</option>
                            <option id="Select_73679_BZ" value="BZ"  >BZ</option>
                            <option id="Select_73679_CA" value="CA"  >CA</option>
                            <option id="Select_73679_CC" value="CC"  >CC</option>
                            <option id="Select_73679_CD" value="CD"  >CD</option>
                            <option id="Select_73679_CF" value="CF"  >CF</option>
                            <option id="Select_73679_CG" value="CG"  >CG</option>
                            <option id="Select_73679_CH" value="CH"  >CH</option>
                            <option id="Select_73679_CI" value="CI"  >CI</option>
                            <option id="Select_73679_CK" value="CK"  >CK</option>
                            <option id="Select_73679_CL" value="CL"  >CL</option>
                            <option id="Select_73679_CM" value="CM"  >CM</option>
                            <option id="Select_73679_CN" value="CN"  >CN</option>
                            <option id="Select_73679_CO" value="CO"  >CO</option>
                            <option id="Select_73679_CR" value="CR"  >CR</option>
                            <option id="Select_73679_CU" value="CU"  >CU</option>
                            <option id="Select_73679_CV" value="CV"  >CV</option>
                            <option id="Select_73679_CX" value="CX"  >CX</option>
                            <option id="Select_73679_CY" value="CY"  >CY</option>
                            <option id="Select_73679_CZ" value="CZ"  >CZ</option>
                            <option id="Select_73679_DE" value="DE"  >DE</option>
                            <option id="Select_73679_DJ" value="DJ"  >DJ</option>
                            <option id="Select_73679_DK" value="DK"  >DK</option>
                            <option id="Select_73679_DM" value="DM"  >DM</option>
                            <option id="Select_73679_DO" value="DO"  >DO</option>
                            <option id="Select_73679_DZ" value="DZ"  >DZ</option>
                            <option id="Select_73679_EC" value="EC"  >EC</option>
                            <option id="Select_73679_EE" value="EE"  >EE</option>
                            <option id="Select_73679_EG" value="EG"  >EG</option>
                            <option id="Select_73679_EH" value="EH"  >EH</option>
                            <option id="Select_73679_ER" value="ER"  >ER</option>
                            <option id="Select_73679_ES" value="ES"  >ES</option>
                            <option id="Select_73679_ET" value="ET"  >ET</option>
                            <option id="Select_73679_FI" value="FI"  >FI</option>
                            <option id="Select_73679_FJ" value="FJ"  >FJ</option>
                            <option id="Select_73679_FK" value="FK"  >FK</option>
                            <option id="Select_73679_FM" value="FM"  >FM</option>
                            <option id="Select_73679_FO" value="FO"  >FO</option>
                            <option id="Select_73679_FR" value="FR"  >FR</option>
                            <option id="Select_73679_GA" value="GA"  >GA</option>
                            <option id="Select_73679_GB" value="GB"  >GB</option>
                            <option id="Select_73679_GD" value="GD"  >GD</option>
                            <option id="Select_73679_GE" value="GE"  >GE</option>
                            <option id="Select_73679_GF" value="GF"  >GF</option>
                            <option id="Select_73679_GG" value="GG"  >GG</option>
                            <option id="Select_73679_GH" value="GH"  >GH</option>
                            <option id="Select_73679_GI" value="GI"  >GI</option>
                            <option id="Select_73679_GL" value="GL"  >GL</option>
                            <option id="Select_73679_GM" value="GM"  >GM</option>
                            <option id="Select_73679_GN" value="GN"  >GN</option>
                            <option id="Select_73679_GP" value="GP"  >GP</option>
                            <option id="Select_73679_GQ" value="GQ"  >GQ</option>
                            <option id="Select_73679_GR" value="GR"  >GR</option>
                            <option id="Select_73679_GS" value="GS"  >GS</option>
                            <option id="Select_73679_GT" value="GT"  >GT</option>
                            <option id="Select_73679_GU" value="GU"  >GU</option>
                            <option id="Select_73679_GW" value="GW"  >GW</option>
                            <option id="Select_73679_GY" value="GY"  >GY</option>
                            <option id="Select_73679_HK" value="HK"  >HK</option>
                            <option id="Select_73679_HM" value="HM"  >HM</option>
                            <option id="Select_73679_HN" value="HN"  >HN</option>
                            <option id="Select_73679_HR" value="HR"  >HR</option>
                            <option id="Select_73679_HT" value="HT"  >HT</option>
                            <option id="Select_73679_HU" value="HU"  >HU</option>
                            <option id="Select_73679_ID" value="ID"  >ID</option>
                            <option id="Select_73679_IE" value="IE"  >IE</option>
                            <option id="Select_73679_IL" value="IL"  >IL</option>
                            <option id="Select_73679_IM" value="IM"  >IM</option>
                            <option id="Select_73679_IN" value="IN"  >IN</option>
                            <option id="Select_73679_IO" value="IO"  >IO</option>
                            <option id="Select_73679_IQ" value="IQ"  >IQ</option>
                            <option id="Select_73679_IR" value="IR"  >IR</option>
                            <option id="Select_73679_IS" value="IS"  >IS</option>
                            <option id="Select_73679_IT" value="IT"  >IT</option>
                            <option id="Select_73679_JE" value="JE"  >JE</option>
                            <option id="Select_73679_JM" value="JM"  >JM</option>
                            <option id="Select_73679_JO" value="JO"  >JO</option>
                            <option id="Select_73679_JP" value="JP"  >JP</option>
                            <option id="Select_73679_KE" value="KE"  >KE</option>
                            <option id="Select_73679_KG" value="KG"  >KG</option>
                            <option id="Select_73679_KH" value="KH"  >KH</option>
                            <option id="Select_73679_KI" value="KI"  >KI</option>
                            <option id="Select_73679_KM" value="KM"  >KM</option>
                            <option id="Select_73679_KN" value="KN"  >KN</option>
                            <option id="Select_73679_KP" value="KP"  >KP</option>
                            <option id="Select_73679_KR" value="KR"  >KR</option>
                            <option id="Select_73679_KW" value="KW"  >KW</option>
                            <option id="Select_73679_KY" value="KY"  >KY</option>
                            <option id="Select_73679_KZ" value="KZ"  >KZ</option>
                            <option id="Select_73679_LA" value="LA"  >LA</option>
                            <option id="Select_73679_LB" value="LB"  >LB</option>
                            <option id="Select_73679_LC" value="LC"  >LC</option>
                            <option id="Select_73679_LI" value="LI"  >LI</option>
                            <option id="Select_73679_LK" value="LK"  >LK</option>
                            <option id="Select_73679_LR" value="LR"  >LR</option>
                            <option id="Select_73679_LS" value="LS"  >LS</option>
                            <option id="Select_73679_LT" value="LT"  >LT</option>
                            <option id="Select_73679_LU" value="LU"  >LU</option>
                            <option id="Select_73679_LV" value="LV"  >LV</option>
                            <option id="Select_73679_LY" value="LY"  >LY</option>
                            <option id="Select_73679_MA" value="MA"  >MA</option>
                            <option id="Select_73679_MC" value="MC"  >MC</option>
                            <option id="Select_73679_MD" value="MD"  >MD</option>
                            <option id="Select_73679_ME" value="ME"  >ME</option>
                            <option id="Select_73679_MF" value="MF"  >MF</option>
                            <option id="Select_73679_MG" value="MG"  >MG</option>
                            <option id="Select_73679_MH" value="MH"  >MH</option>
                            <option id="Select_73679_MK" value="MK"  >MK</option>
                            <option id="Select_73679_ML" value="ML"  >ML</option>
                            <option id="Select_73679_MM" value="MM"  >MM</option>
                            <option id="Select_73679_MN" value="MN"  >MN</option>
                            <option id="Select_73679_MO" value="MO"  >MO</option>
                            <option id="Select_73679_MP" value="MP"  >MP</option>
                            <option id="Select_73679_MQ" value="MQ"  >MQ</option>
                            <option id="Select_73679_MR" value="MR"  >MR</option>
                            <option id="Select_73679_MS" value="MS"  >MS</option>
                            <option id="Select_73679_MT" value="MT"  >MT</option>
                            <option id="Select_73679_MU" value="MU"  >MU</option>
                            <option id="Select_73679_MV" value="MV"  >MV</option>
                            <option id="Select_73679_MW" value="MW"  >MW</option>
                            <option id="Select_73679_MX" value="MX"  >MX</option>
                            <option id="Select_73679_MY" value="MY"  >MY</option>
                            <option id="Select_73679_MZ" value="MZ"  >MZ</option>
                            <option id="Select_73679_NA" value="NA"  >NA</option>
                            <option id="Select_73679_NC" value="NC"  >NC</option>
                            <option id="Select_73679_NE" value="NE"  >NE</option>
                            <option id="Select_73679_NF" value="NF"  >NF</option>
                            <option id="Select_73679_NG" value="NG"  >NG</option>
                            <option id="Select_73679_NI" value="NI"  >NI</option>
                            <option id="Select_73679_NL" value="NL"  >NL</option>
                            <option id="Select_73679_NO" value="NO"  >NO</option>
                            <option id="Select_73679_NP" value="NP"  >NP</option>
                            <option id="Select_73679_NR" value="NR"  >NR</option>
                            <option id="Select_73679_NU" value="NU"  >NU</option>
                            <option id="Select_73679_NZ" value="NZ"  >NZ</option>
                            <option id="Select_73679_OM" value="OM"  >OM</option>
                            <option id="Select_73679_PA" value="PA"  >PA</option>
                            <option id="Select_73679_PE" value="PE"  >PE</option>
                            <option id="Select_73679_PF" value="PF"  >PF</option>
                            <option id="Select_73679_PG" value="PG"  >PG</option>
                            <option id="Select_73679_PH" value="PH"  >PH</option>
                            <option id="Select_73679_PK" value="PK"  >PK</option>
                            <option id="Select_73679_PL" value="PL"  >PL</option>
                            <option id="Select_73679_PM" value="PM"  >PM</option>
                            <option id="Select_73679_PN" value="PN"  >PN</option>
                            <option id="Select_73679_PR" value="PR"  >PR</option>
                            <option id="Select_73679_PS" value="PS"  >PS</option>
                            <option id="Select_73679_PT" value="PT"  >PT</option>
                            <option id="Select_73679_PW" value="PW"  >PW</option>
                            <option id="Select_73679_PY" value="PY"  >PY</option>
                            <option id="Select_73679_QA" value="QA"  >QA</option>
                            <option id="Select_73679_RE" value="RE"  >RE</option>
                            <option id="Select_73679_RO" value="RO"  >RO</option>
                            <option id="Select_73679_RS" value="RS"  >RS</option>
                            <option id="Select_73679_RU" value="RU"  >RU</option>
                            <option id="Select_73679_RW" value="RW"  >RW</option>
                            <option id="Select_73679_SA" value="SA"  >SA</option>
                            <option id="Select_73679_SB" value="SB"  >SB</option>
                            <option id="Select_73679_SC" value="SC"  >SC</option>
                            <option id="Select_73679_SD" value="SD"  >SD</option>
                            <option id="Select_73679_SE" value="SE"  >SE</option>
                            <option id="Select_73679_SG" value="SG"  >SG</option>
                            <option id="Select_73679_SH" value="SH"  >SH</option>
                            <option id="Select_73679_SI" value="SI"  >SI</option>
                            <option id="Select_73679_SJ" value="SJ"  >SJ</option>
                            <option id="Select_73679_SK" value="SK"  >SK</option>
                            <option id="Select_73679_SL" value="SL"  >SL</option>
                            <option id="Select_73679_SM" value="SM"  >SM</option>
                            <option id="Select_73679_SN" value="SN"  >SN</option>
                            <option id="Select_73679_SO" value="SO"  >SO</option>
                            <option id="Select_73679_SR" value="SR"  >SR</option>
                            <option id="Select_73679_SS" value="SS"  >SS</option>
                            <option id="Select_73679_ST" value="ST"  >ST</option>
                            <option id="Select_73679_SV" value="SV"  >SV</option>
                            <option id="Select_73679_SX" value="SX"  >SX</option>
                            <option id="Select_73679_SY" value="SY"  >SY</option>
                            <option id="Select_73679_SZ" value="SZ"  >SZ</option>
                            <option id="Select_73679_TC" value="TC"  >TC</option>
                            <option id="Select_73679_TD" value="TD"  >TD</option>
                            <option id="Select_73679_TF" value="TF"  >TF</option>
                            <option id="Select_73679_TG" value="TG"  >TG</option>
                            <option id="Select_73679_TH" value="TH"  >TH</option>
                            <option id="Select_73679_TJ" value="TJ"  >TJ</option>
                            <option id="Select_73679_TK" value="TK"  >TK</option>
                            <option id="Select_73679_TL" value="TL"  >TL</option>
                            <option id="Select_73679_TM" value="TM"  >TM</option>
                            <option id="Select_73679_TN" value="TN"  >TN</option>
                            <option id="Select_73679_TO" value="TO"  >TO</option>
                            <option id="Select_73679_TR" value="TR"  >TR</option>
                            <option id="Select_73679_TT" value="TT"  >TT</option>
                            <option id="Select_73679_TV" value="TV"  >TV</option>
                            <option id="Select_73679_TW" value="TW"  >TW</option>
                            <option id="Select_73679_TZ" value="TZ"  >TZ</option>
                            <option id="Select_73679_UA" value="UA"  >UA</option>
                            <option id="Select_73679_UG" value="UG"  >UG</option>
                            <option id="Select_73679_UM" value="UM"  >UM</option>
                            <option id="Select_73679_US" value="US"  >US</option>
                            <option id="Select_73679_UY" value="UY"  >UY</option>
                            <option id="Select_73679_UZ" value="UZ"  >UZ</option>
                            <option id="Select_73679_VA" value="VA"  >VA</option>
                            <option id="Select_73679_VC" value="VC"  >VC</option>
                            <option id="Select_73679_VE" value="VE"  >VE</option>
                            <option id="Select_73679_VG" value="VG"  >VG</option>
                            <option id="Select_73679_VI" value="VI"  >VI</option>
                            <option id="Select_73679_VN" value="VN"  >VN</option>
                            <option id="Select_73679_VU" value="VU"  >VU</option>
                            <option id="Select_73679_WF" value="WF"  >WF</option>
                            <option id="Select_73679_WS" value="WS"  >WS</option>
                            <option id="Select_73679_YE" value="YE"  >YE</option>
                            <option id="Select_73679_YT" value="YT"  >YT</option>
                            <option id="Select_73679_ZA" value="ZA"  >ZA</option>
                            <option id="Select_73679_ZM" value="ZM"  >ZM</option>
                            <option id="Select_73679_ZW" value="ZW"  >ZW</option>
                        </select>
                    </div>
                </div>


                <div class="row form-gap">
                    <div class="col-sm-6">
                        <!--<label id="Label_10" > T&#233;l&#233;phone de Contact</label>-->
                        <input id="TextBox_73686" class="txt" name="TextBox_73686" maxlength="25" size="45" placeholder=" Phone number *" type="text"  />
                    </div>

                    <div class="col-sm-6">
                        <!--<label id="Label_4" > Email 1 <span class="texte4">*</span></label>-->
                        <input id="TextBox_73680" class="txt" name="TextBox_73680" maxlength="320" size="45" placeholder="Email address *" type="text"  required />

                    </div>
                </div>




                <div class="row form-gap">
                    <div class="col-sm-12">
                        <!--<label id="Label_5" > Message <span class="texte4">*</span></label>-->
                        <textarea id="TextArea_73681" name="TextArea_73681" class="txt" placeholder="Message *" rows="10" style="width:100%; resize: vertical"  required></textarea>
                    </div>
                </div>

                <!--<div>
                <label id="Label_6" > Job title</label>
                <input id="TextBox_73682" class="txt" name="TextBox_73682" maxlength="100" size="45" placeholder=" Job title" type="text" style="text-align:left;;text-transform:" />
                <span class="texte4">*</span>
                <br />
                </div>-->




                <div class="row form-gap">
                    <div class="col-xs-4 col-sm-6">
                        <label id="Label_11">Subscribe to the newsletter</label>
                    </div>
                    <div class="col-sm-6 col-xs-8 ">
                        <!--        <input type="radio" id="Checkbox_76010_Vrai" name="Checkbox_76010" value="1">&nbsp;Vrai&nbsp;
                                <input type="radio" id="Checkbox_76010_Faux" name="Checkbox_76010" value="0">&nbsp;Faux&nbsp;-->
                        <div class="row">
                            <div class="ipro-form__group ipro-form__group--custom col-xs-6 col-sm-4">
                                <input type="radio" id="Checkbox_76010_Vrai" name="Checkbox_76010" checked class="hidden" value="1" required>
                                <label for="Checkbox_76010_Vrai">Yes</label>
                            </div>

                            <div class="ipro-form__group ipro-form__group--custom col-xs-6 col-sm-4">
                                <input type="radio" id="Checkbox_76010_Faux" name="Checkbox_76010"  class="hidden" value="0">
                                <label for="Checkbox_76010_Faux">No</label>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row form-gap">
                    <div class="col-sm-6">
                        <div>Please enter the following code</div>
                        <div id="captchaDiv-contact"></div><script type="text/javascript">sjcap('captchaDiv-contact','txtCaptcha-contact',null);</script>
                    </div>
                </div>

                <!--<input type="submit" name="submit" id="submit">-->
                <div class="ipro-form__group ipro-form__group--action text-center">
                    <a href="javascript:void(0);" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium">
                        <input type="submit" name="submit" id="submit" value="Submit">
                    </a>
                </div>
            </form>
        </div>
    </div>
</section>

</body>
</html>
<!-- D'AUTRES COMMENTAIRES PEUVENT ÊTRE INSÉRÉS ICI! -->

