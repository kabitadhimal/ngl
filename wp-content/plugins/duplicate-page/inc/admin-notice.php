<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<div class="notice notice-success is-dismissible dup_admin_notice">
<p><?php _e( '<strong>Recommended for you:</strong> Get to know <a href="https://wordpress.org/plugins/swipe/" target="_blank">Swipe!</a> Your website security could benefit from adding two factor authentication, so why not try the latest innovation in RSA encryption to keep your site safe. With Swipe there is no need to remember random passwords or one time codes. Give <a href="https://wordpress.org/plugins/swipe/" target="_blank">Swipe a try!</a> ', 'duplicate-page' ); ?></p>
</div>
<script>
jQuery(document).ready(function(e) {
jQuery(document).on('click','.dup_admin_notice .notice-dismiss', function(e) {
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>'; 
    var data = {
			'action': 'dup_hide_admin_notice',
			'val': 'hide'
		};
		jQuery.post(ajaxurl, data, function(response) {			
		});
});
});
</script>