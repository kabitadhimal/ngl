<?php /*Template Name: New Homepage Template */?>
<?php get_header(); ?>
<?php
while (have_posts()): the_post();
    $blocks = get_field('home_flexible_contents');
 if($blocks):
        foreach ($blocks as $block):
            $tpl = __DIR__.'/../partial/home-flex-content/'.strtolower($block['acf_fc_layout']).'.php';
            if (file_exists($tpl)) {
                include $tpl;
            }
        endforeach;
    endif;
endwhile;
?>
<?php get_footer();?>
