<?php /*Template Name: Reset Password Template */?>
<?php get_header();

while(have_posts()):the_post();
?>

 <section class="ipro-block ipro-block--form">
<div class="ipro-container ipro-container--main">
<!-- NGL post form -->
	    <div class="ipro-form-row text-left">

	    <?php $guid = $_GET['guid'];
				$args = array( 
				  'meta_query' => array(
				    array(
				        'key' => 'guid',
				        'value'=>$guid       
				    ),
				  ) 
				);
				$user= get_users($args);
				if(!empty($user[0]->data)):
	    		?>
	        			<h3><?php the_title();?></h3>

				        <div class="ipro-form__wrap">
				        <div class="successDiv success"></div>
				            <form class="ipro-form ipro-form--account" id="reset_password_form">
				                <div class="ipro-form__group">
				                    <input type="password" class="ipro-form__control" id="password" name="password" placeholder="<?php echo __('Password*','ngl')?>" />
				                </div>
				                <div class="ipro-form__group">
				                    <input type="password" class="ipro-form__control" name="retype_password" placeholder="<?php echo __('Retype Password','ngl')?>" />
				                </div>
				                <input type="hidden" name="guid" value="<?php echo $user[0]->data->ID?>">
				                <div class="ipro-form__group ipro-form__group--action text-right">
				                    <button type="submit" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium"><?php echo __('Change Password','ngl')?></button>
				                </div>
				            </form>
				        </div>
	        <?php else:?>

			<div class=""><?php echo __('Sorry, the password link isnot valid.','ngl');?></div>

			<?php endif; ?>
	    </div><!-- NGL post form -->
    </div>
</section>

<?php endwhile;?>
 <script type="text/javascript">
                        $(function(){   
                     var passwordChangeText = "<?php echo __('Change Password','ngl')?>";
                        $("#reset_password_form").validate({
                            rules: {
                               password: "required",
                                retype_password: {
                                    equalTo: "#password"
                                }
                            },             
                            submitHandler: function(form) {
                                //form.submit();
                                            
                                $('.successDiv').text('');               
                             

                                var input_data = $('#reset_password_form').serialize();
                                var datas = {action: 'reset_password', data: input_data};
                                //console.log(datas);
                                ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                //$('.submit').attr('disabled', true);
                                $('.ipro__loader-wrap').show();
                                $('.submit').text('Wait...');
                                $.ajax({
                                    url: ajaxurl,
                                    type: "POST",
                                    data: datas,
                                    dataType: "json",
                                    success: function(response) {
                                        //console.log(response);

                                        if (response.status == 'error') {
                                             $('.ipro__loader-wrap').hide();
                                           // $('.submit').attr('disabled', false);
                                            $('.submit').text(passwordChangeText);
                                            $('.errorDiv').html(response.msg).show();
                                            $("html, body").animate({ scrollTop: 0 }, "slow");
                                            return false;
                                        } else if (response.status == 'success') {
                                            $('.ipro__loader-wrap').hide();
                                             $('.submit').text(passwordChangeText);
                                            $('.successDiv').html(response.msg).show();
                                            $('input[type="password"]').val('');
                                            $("html, body").animate({ scrollTop: 0 }, "slow");
                                        } else {
                                            $('.ipro__loader-wrap').hide();
                                             $('.submit').text(passwordChangeText);
                                            //$('.submit').attr('disabled', false);
                                            $('.errorDiv').html('Something went wrong.').show();
                                            return false;
                                        }


                                    }
                                });
                                return false;
                            }
                        });
                    });
    </script>

<?php get_footer();?>