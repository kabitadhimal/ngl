<?php /*Template Name: Home Template */?>

<?php get_header();

 while(have_posts()): the_post(); 
 	
 $blocks = get_field('home_contents');

  if(!empty($blocks)):
  		 foreach ($blocks as $block):?>

			<?php if(!empty($block['acf_fc_layout']) && $block['acf_fc_layout']=='slider'):?>

<!-- NGL home page hero slider/banner -->
	<section id="ipro-home-heroSlider" class="ipro-heroSlider ipro-heroSlider--home text-center" data-theme="theme-default">
    <div class="ipro-slider__row">

        <!-- If users/clients opt to display image slider then show this slider programtically -->
        <!-- NGL home page slider row -->
        <div class="swiper-container ipro-slider ipro-slider--full ipro-slider--loading" data-autoplay-speed="6000" data-slide-effect="fade">
            <div class="swiper-wrapper ipro-slider__swiper-wrapper">
                <!-- NGL slider slides group -->
                <?php
                if (!empty($block['home_slider'])):
                    foreach ($block['home_slider'] as $slider):
                        ?>
                        <div class="ipro-slider__slide swiper-slide">
                            <img src="<?php echo $slider['image']['url'];?>" class="ipro-valign--middle" alt="" />
                            <div class="ipro-slider__caption">
                                <h1><?php echo $slider['title'];?></h1>
                                <h6><?php echo $slider['text'];?></h6>
                                <a href="<?php echo $slider['button_link'];?>" class="btn btn--stromGrey btn--large"><?php echo $slider['button'];?></a>
                            </div>
                        </div>

                        <?php
                    endforeach;
                    ?>


                <?php endif; ?>

                
            </div>

            <!-- Slider pagination -->
            <div class="swiper-pagination ipro-slider__pagination"></div><!-- /.# slider pagination -->

            <!-- Slider controls -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>

        </div><!-- NGL home page slider row -->

    </div>
	</section>

<?php endif;?>

<?php if(!empty($block['acf_fc_layout']) && $block['acf_fc_layout']=='history_and_expertise_section'):?>

	<!-- NGL CMS blocks :: Shortcodes twohalf - Years of experience -->
<section class="ipro-block ipro-block--twoHalf ipro-block--full js-anim-init"  data-theme="theme-palma-stormgrey" data-background="true" data-animation="fadein">
    <div class="ipro-block__row">

        <div class="clearfix ipro-row ipro-flex">

            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block--bkg ipro-block__gap--medium text-left js-anim-init" data-background="true" data-delay="0.25" data-animation="fadein slideInDown" style="background-image: url(<?php echo $block['left_background_image']['url']?>);">
            </div>

            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block--isoverlay ipro-block__gap--medium text-left js-anim-init" data-background="true" data-animation="fadein slideInDown" style="background-image: url(<?php //echo $block['right_background_image']['url']?>);">
                <div class="ipro-col__content js-anim-init" data-delay="1" data-animation="fadein slideInDown">
                    <h2><?php echo $block['title']?></h2>
                    <p><?php echo $block['short_description']?></p>

                    <a href="<?php echo $block['link_']?>" class="ipro-link ipro-link--underline ipro-link--palma"><?php //echo $block['button_text']?></a>
                </div>
            </div>
        </div><!-- /.# NGL tow half grid wrap -->

    </div><!-- /.# NGL container -->
</section>



    <section class="ipro-block ipro-block--twoHalf ipro-block--full ipro-block--logo js-anim-init" data-theme="theme-default" data-background="true" data-animation="fadein" style="background-image: url(<?php echo TEMP_DIR_URI ?>/images/qService-bkg.jpg);">
        <div class="ipro-block__row">

            <div class="clearfix ipro-row ipro-flex">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--small text-right">
                    <div class="ipro-col__content">
                        <ul class="ipro-block__list-group">
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">Cleaning processes<br/><span>& chemicals</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">Water recycling<br/><span>processes & chemicals</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">decoating <br/> <span>technology</span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--small text-left">
                    <div class="ipro-col__content">
                        <ul class="ipro-block__list-group">
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">ultrasonics<br/><span>& spray chemicals</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">Water-based<br/><span>surface preparation</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">ecological <br/> <span>cleaning solutions</span></li>
                        </ul>
                    </div>
                </div>

            </div><!-- /.# NGL tow half grid wrap -->

            <!-- /.#NGL CMS blocks :: two half - logo -->
            <div class="ipro-block__logo js-anim-init" data-animation="fadein">
                <img src="<?php echo TEMP_DIR_URI ?>/images/swiss-qty-logo.png" alt="" class="ipro-valign--middle" />
            </div><!-- /.#NGL CMS blocks :: two half - logo -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: two half - quality services with logo -->

<?php endif;?>

<?php if(!empty($block['acf_fc_layout']) && $block['acf_fc_layout']=='subsidiaries'):?>
	 <!-- NGL CMS blocks :: Home page map section -->
    <section class="ipro-block ipro-block--map ipro-block--full js-anim-init text-center" data-theme="theme-palma" data-delay="0.15" data-animation="fadein" style="background: url(<?php echo TEMP_DIR_URI ?>/images/map-bkg-patter.png);">
        <div class="ipro-block__row">

          
            <div class="ipro-block--map__row">

                <figure>
                    <img src="<?php echo TEMP_DIR_URI ?>/images/map-bkg.png" class="ipro-valign--middle map-image" alt="" />
                </figure>

             
                <div id="ipro-block--map__marker" class="ipro-block--map__marker">


                    <?php if(!empty($block['add_subsidiary'])):
                    ?>

                    <?php
                        foreach($block['add_subsidiary'] as $ind=>$map):
                           
                            ?>
                         <a href="javascript:void(0);" class="icon icon-map-pin map__marker map__marker--<?php echo $ind==0?'geneva':''?> map__marker--<?php echo $ind==0?'large':'small'?> map__marker--<?php echo $ind==0?'goldenrod':'palma'?> js-anim-init" data-delay="<?php echo $ind==0?'0.0005':'0.375'?>" data-animation="fadeIn"  style="top:<?php echo $map['x_co-ordinates']?>%;left:<?php echo $map['y_co-ordinates']?>%"></a>
                            <?php endforeach;
                    endif;?> 
                   
                    
                </div>

             
                <div class="ipro-block--map__caption js-anim-init" data-delay="0.025" data-animation="fadein slideInDown">
                    <div id="ipro-block--map__title" class="ipro-block--map__title" data-delay="800">
                        <span class="ipro-title ipro-title--big"><?php echo $block['title']?></span>
                    </div>
                    <div class="ipro-block--map__button">
                        <a href="<?php echo $block['button_url']?>" class="btn btn--large btn--goldenrod"><?php echo $block['button_text']?></a>
                    </div>
                </div>
            </div>

        </div>
    </section>
<?php endif;?>

<?php if(!empty($block['acf_fc_layout']) && $block['acf_fc_layout']=='products_section'):?>

	
 
    <section class="ipro-block ipro-block--kwirksBox ipro-block--full js-anim-init" data-theme="theme-default" data-animation="fadein">
        <div class="ipro-block__row">

            <div class="ipro-row clearfix ipro-block--kwirksBox__row kwicks kwicks-horizontal">
            	<?php if(!empty($block['blocks'][0]['acf_fc_layout']) && $block['blocks'][0]['acf_fc_layout']=='left_block'):?>
	
                <div class="col-sm-4 col-xs-12 ipro-block__col ipro-block__col--noPadding kwirks-expand js-winWidth" data-box="kwirks-box" data-order="1">
                    <div class="kwirks-box__row clearfix">

                        <div class="kwirks-box__hidden kwirks-box__hidden--list">
                            <span class="closer-x"></span>
                            <div class="kwirks-box__hidden-content clearfix">
                                <div class="col-sm-12 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                    <?php if(!empty($block['blocks'][0]['1st_column'])):
                                    			foreach($block['blocks'][0]['1st_column'] as $product):
                                    		?>
                                       			 <li><a href="<?php echo $product['link']?>"><?php echo $product['text']?></a></li>
                                    <?php 		endforeach;
                                    	endif;?>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="kwirks-box__inner">
                            <div class="ipro-col__content ipro-product__bkg js-anim-init" data-animation="fadein" style="background-image: url(<?php echo $block['blocks'][0]['image']['url']?>)">
                                <div class="ipro-block__col-caption js-anim-init" data-animation="fadein slideInDownx">
                                    <h2>
                                        <span><?php echo $block['blocks'][0]['title']?></span>
                                        <i class="ipro-chevron ipro-chevron--right visible-xs"></i>
                                    </h2>
                                </div>
                            </div>
                            <a href="" class="ipro-link--overlay ipro-kwirks__mbl-trigger"></a>
                        </div>

                    </div>
                </div>

                <?php endif;?>

                <?php if(!empty($block['blocks'][1]['acf_fc_layout']) && $block['blocks'][1]['acf_fc_layout']=='right_block'):?>
                <div class="col-sm-4 col-xs-12 ipro-block__col ipro-block__col--noPadding kwirks-expand js-winWidth" data-box="kwirks-box" data-order="2">
                    <div class="kwirks-box__row clearfix">

                        <div class="kwirks-box__hidden kwirks-box__hidden--list">
                            <span class="closer-x"></span>
                            <div class="kwirks-box__hidden-content clearfix">
                                <div class="col-sm-12 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                    <?php if(!empty($block['blocks'][1]['1st_column'])):
                                                foreach($block['blocks'][1]['1st_column'] as $product):
                                            ?>
                                                 <li><a href="<?php echo $product['link']?>"><?php echo $product['text']?></a></li>
                                    <?php       endforeach;
                                        endif;?>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="kwirks-box__inner">
                            <div class="ipro-col__content js-anim-init ipro-product__bkg" data-animation="fadein" style="background-image: url(<?php echo $block['blocks'][1]['background_image']['url']?>)">
                                <div class="ipro-block__col-caption js-anim-init" data-animation="fadein slideInDownx">
                                    <h2>
                                        <span><?php echo $block['blocks'][1]['title']?></span>
                                        <i class="ipro-chevron ipro-chevron--right visible-xs"></i>
                                    </h2>
                                </div>
                            </div>
                            <a href="" class="ipro-link--overlay ipro-kwirks__mbl-trigger"></a>
                        </div>

                    </div>
                </div>
                <?php endif;?>
			<?php if(!empty($block['blocks'][2]['acf_fc_layout']) && $block['blocks'][2]['acf_fc_layout']=='middle_column'):?>
                <div class="col-sm-4 col-xs-12 ipro-block__col ipro-block__col--noPadding js-winWidth kwirks-expand" data-box="kwirks-box" data-order="3">
                    <div class="kwirks-box__row clearfix">

                        <div class="kwirks-box__hidden kwirks-box__hidden--list">
                            <span class="closer-x"></span>
                            <div class="kwirks-box__hidden-content clearfix">
                                <div class="col-sm-12 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                    <?php if(!empty($block['blocks'][2]['1st_column'])):
                                                foreach($block['blocks'][2]['1st_column'] as $product):
                                            ?>
                                                 <li><a href="<?php echo $product['link']?>"><?php echo $product['text']?></a></li>
                                    <?php       endforeach;
                                        endif;?>
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="kwirks-box__inner">
                            <div class="ipro-col__content js-anim-init ipro-product__bkg" data-animation="fadein" style="background-image: url(<?php echo $block['blocks'][2]['background_image']['url']?>)">
                                <div class="ipro-block__col-caption js-anim-init" data-animation="fadein slideInDownx">
                                    <h2>
                                        <span><?php echo $block['blocks'][2]['title']?></span>
                                        <i class="ipro-chevron ipro-chevron--right visible-xs"></i>
                                    </h2>
                                </div>
                            </div>
                            <a href="" class="ipro-link--overlay ipro-kwirks__mbl-trigger"></a>
                        </div>

                    </div>
                </div>
            <?php endif;?>

            </div>

        </div>
    </section>

<?php endif;?>

<?php if(!empty($block['acf_fc_layout']) && $block['acf_fc_layout']=='services_section'):?>
	<section class="ipro-block ipro-block--twoHalf ipro-block--twoHalf-homepage ipro-block--full js-anim-init" data-background="false" data-animation="fadein">
        <div class="ipro-block__row">

            <div class="clearfix ipro-row ipro-flex">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--img text-center">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInDown">
                        <figure>

                            <a href="<?php echo $block['link_left_']?>"><img src="<?php echo $block['image_at_left']['url']?>" class="ipro-valign--middle media__animate media__animate--scale" alt="<?php echo $block['image_at_left']['alt']?>" /></a>

                        </figure>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--text text-left">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInRight">
                        <blockquote><?php echo $block['citation_1']?></blockquote>
                        <h3 class="ipro__h2 ipro__h2--author"><?php echo $block['citation_1_from']?></h3>
                    </div>
                </div>

            </div><!-- /.# NGL tow half grid wrap -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: Shortcodes two half - Image/Text with large gap -->


  
    <!-- NGL CMS blocks :: Shortcodes two half - Image/Text[Inverted] with large gap -->
    <section class="ipro-block ipro-block--twoHalf ipro-block--twoHalf-homepage ipro-block--full ipro-block--invert js-anim-init" data-background="false" data-animation="fadein">
        <div class="ipro-block__row">

            <div class="clearfix ipro-row ipro-flex">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--img text-center">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInDown">
                        <figure>

                             <a href="<?php echo $block['link_right']?>"><img src="<?php echo $block['image_at_right']['url']?>" class="ipro-valign--middle media__animate media__animate--scale" alt="" /></a>


                        </figure>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--text text-right">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInLeft">
                        <blockquote><?php echo $block['citation_2_'];?></blockquote>
                        <h3 class="ipro__h2 ipro__h2--author"><?php echo $block['citation_2_from']?></h3>
                    </div>
                </div>

            </div><!-- /.# NGL tow half grid wrap -->

        </div><!-- /.# NGL container -->
    </section>
<?php endif;?>

<?php if(!empty($block['acf_fc_layout']) && $block['acf_fc_layout']=='newsevents_section'):?>
	    <!-- NGL CMS blocks :: Shortcodes News / Events -->
    <section id="ipro-latestEvent-block" class="ipro-block ipro-block--newsEvents text-center" data-theme="theme-default" data-speed="0.5" data-delay="0.2" style="background-image: url(<?php echo TEMP_DIR_URI?>/images/hp-news-bkg.png);">
        <div class="ipro-container ipro-container--large">

            <header class="ipro-block__header ipro-block__header--small">
                <h2><?php echo __('News / Events','ngl')?></h2>
                <a href="<?php echo home_url('/news-events/')?>" class="ipro-link ipro-link--default ipro-link--underline"><?php echo __('See more news','ngl')?></a>
            </header><!-- /.# NGL CMS Components :: section header -->

            <div class="ipro-block__content ipro-block__gutter ipro-block__gutter--small">
                <div class="row ipro-row ipro-flex clearfix">

			<?php if(!empty($block['select_newsevents'])):
                        //debug($block['select_newsevents']);
                        //Different language events slug
                        $events_slug_arr = array('events-zh-hans','events-de','events-it','events-es','events','events-2');
                        $news_slug_arr = array('news','news-2','news-zh-hans','news-de','news-it','news-es');

					foreach($block['select_newsevents'] as $ind=>$news):
                        $status = get_field('status',$news->ID);
                        $is_locked = get_field('is_locked',$news->ID);
                        $categories = get_the_category( $news->ID );


                        $type='';
                        $category='';
                        $categoryName='';
                        if(!empty($categories)){
                            $category = $categories[0]->slug;                      



                            if(in_array($category,$events_slug_arr)){
                                $type='ipro-newsEvents__col--isevent';
                                $categoryName = 'Events';
                            }elseif(in_array($category,$news_slug_arr)){
                                 $type='ipro-newsEvents__col--isnews';
                                 $categoryName = 'News';
                            }

                          


                        }
                        
                        
                        if($status=='new'):
                            $status_type='newProduct';
                            $statusClass='ipro-newsEvents__col-isnewProduct';  

                        elseif($status=='new_product'):
                            $status_type='recentProduct';
                            $statusClass='ipro-newsEvents__col-isrecent'; 
                            
                        else:
                            $status_type='';
                            $statusClass='';

                        endif;
                    
                        ?>
	                    <article class="col-sm-4 col-xs-12 ipro-newsEvents__col <?php echo $is_locked==1?'ipro-newsEvents__col--isprotected':''?> <?php echo $type?> ipro-newsEvents__col--isstrormGray <?php echo $statusClass?>" data-order="<?php echo ++$ind?>">
	                        <div class="js-sprite-animatex js-anim-init" data-delay="0.035" data-animation="fadeIn">
	                            <figure>
                                <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($news->ID));?>
                                <img src="<?php echo $image_url?>" alt="" class="ipro-valign--middle" />
                                    <?php if(!empty($status_type)): ?>

                                            <div class="ribbon ribbon-abs ribbon-abs--topRight ribbon-<?php echo $status_type?>">
                                                <div class="ribbon__inner">

                                                <?php if(!empty($status) && $status=='new_product'):?>
                                                    <span>New <br/>product</span>
                                                <?php elseif(!empty($status) && $status=='new'): ?>
                                                     <span>New</span>
                                                <?php  endif; ?>
                                                </div>
                                            </div>
                                <?php endif;?>

                                <a href="<?php echo $news->guid;?>" class="ipro-link ipro-link--overlay"></a>
	                            </figure>
	                            <div class="ipro-newsEvents__col-body">
	                                <div class="ipro-newsEvents__meta">
	                                    <span class="ipro-newsEvents__cat"><?php echo $categoryName?></span> - <span class="ipro-newsEvents__date"><?php echo date('d.m.Y',strtotime($news->post_date))?></span>
	                                </div>
	                                <h3><?php echo $news->post_title?></h3>
                                   <p> <?php echo $content = wp_trim_words($news->post_content, 17, $more = '… '); ?></p>

                                    <?php 

                                    if(!empty($category) && in_array($category,$news_slug_arr)):?>
                                    
                                     <a href="<?php echo $news->guid;?>" class="btn btn--goldenrod btn--large btn--large-custom"><?php echo __('Read more','ngl')?></a>
                                 <?php elseif(!empty($category) && in_array($category,$events_slug_arr)): ?>
	                                <a href="<?php echo $news->guid;?>" class="btn btn--stromGrey btn--large btn--large-custom"><?php echo __('Meet us','ngl')?></a>
                                <?php else: ?>
                                     <a href="<?php echo $news->guid;?>" class="btn btn--stromGrey btn--large btn--large-custom"><?php echo __('Read more','ngl')?></a>
                                <?php endif;?>
	                            </div>
                                <?php if($is_locked):?>
	                            <i class="icon icon-abs icon-abs--mid icon-lock"></i>
                            <?php endif;?>
	                        </div>
	                    </article>

                <?php endforeach;
                endif;?>


                </div><!-- /.# NGL CMS Components :: news/events post entry row -->
            </div><!-- /.# NGL CMS Components :: section post content -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: Shortcodes News / Events -->

	<?php endif;?>






	<?php 
	endforeach;
endif;
endwhile;
?>

<!-- /.#NGL home page hero slider/banner -->

<?php get_footer();?>