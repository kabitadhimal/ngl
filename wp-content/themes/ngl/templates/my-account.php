<?php /*Template Name: My Account Template */?>

<?php 
if(!is_user_logged_in()){
	wp_redirect(home_url('create-an-account'));exit();
}
/*Account Deletion*/

if(isset($_POST['delete_user_id'])){
	$user_id = $_POST['delete_user_id'];
	require_once( ABSPATH . '/wp-admin/includes/user.php' );
	require TEMP_DIR . '/partial/account-delete.php';
	
	wp_redirect(home_url('create-an-account'));exit();
}

 get_header();



$user = wp_get_current_user();
$user_detail = $user->data;
//debug($user_detail);
$company = get_user_meta($user->ID, 'company',true);
$office = get_user_meta($user->ID, 'office',true);
$telephone = get_user_meta($user->ID, 'telephone_number',true);
$address = get_user_meta($user->ID, 'address',true);
$postal_code = get_user_meta($user->ID, 'postal_code',true);
$city = get_user_meta($user->ID, 'city',true);
$pays = get_user_meta($user->ID, 'pays',true);
$status = get_user_meta($user->ID, 'status',true);
$guid = get_user_meta($user->ID, 'guid',true);

while(have_posts()):the_post();

 ?>

  <section class="ipro-banner ipro-block--loggedIn ipro-banner--account">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL news details banner title -->
                            <div class="ipro-banner__title text-center">
                                <h2><?php the_title(); ?></h2>
                                <p><em><?php the_content(); ?></em></p>
                            </div><!-- /.#NGL news details banner title block -->

                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL news details title banner -->

                    <!-- NGL News details post content block -->
                    <section class="ipro-block ipro-block--loggedIn ipro-block--loggedIn__myaccount">
                        <div class="ipro-container ipro-container--medium">
                            <div class="ipro-account__row ipro-loggedIn__row">

                                <!-- My account logged in 2 col row -->
                                <div class="row clearfix ipro__gutter--size54">
                                    <aside class="col-sm-4 col-xs-12 ipro__sidebar--loggedIn" role="complementary">
                                        <nav class="ipro__sidebar-nav ipro__accordion--mobile">

                                        <a href="" class="js-mbl-accordion ipro__ui ipro__ui--plus" data-collapse="ipro__accordion--mobile-panel"><?php echo __('Filter News','ngl')?></a>
											<?php get_template_part('after-login-sidebar');?>
                                            

                                        </nav>
                                    </aside>
                                    <div class="col-sm-8 col-xs-12 ipro__content--loggedIn" role="main">
                                        <!-- NGL account page form block -->
                                        <div class="ipro-block ipro-block--form">
                                            <div class="ipro-container ipro-container--main">

                                                <!-- NGL post form -->
                                                <div class="ipro-form-row text-left">
                                                    <h3> <?php echo __('My personal information','ngl')?>:</h3>
										
                                                    <div class="ipro-form__wrap">
                                                    <div class="errorDiv error"></div>
                                                    <div class="successDiv success"></div>
                                                        <form class="ipro-form ipro-form--account" id="registration_update_form">

							                                        <div class="ipro-form__group">
							                                            <input type="text" class="ipro-form__control" value="<?php echo $company?>" name="company" placeholder="<?php echo __('Company','ngl')?>*" />
							                                        </div>
							                                        <div class="ipro-form__group">
							                                            <input type="text" class="ipro-form__control" value="<?php echo $user_detail->display_name?>" name="fullname" placeholder="<?php echo __('First & Last Name','ngl')?>*" />
							                                        </div>
							                                        <div class="ipro-form__group">
							                                            <input type="text" class="ipro-form__control" value="<?php echo $office?>" name="office" placeholder="<?php echo __('Function','ngl')?>*" />
							                                        </div>
							                                        <div class="ipro-form__group">
							                                            <input type="text" class="ipro-form__control" value="<?php echo $address?>" name="address" placeholder="<?php echo __('Address','ngl')?>" />
							                                        </div>
							                                        
							                                        <div class="ipro-form__inline">
							                                            <div class="ipro-form__group ipro-form__group--inline">
							                                                <input type="text" class="ipro-form__control" value="<?php echo $postal_code?>" name="postal_code" placeholder="<?php echo __('Code postal','ngl')?>" />
							                                            </div>
							                                            <div class="ipro-form__group ipro-form__group--inline">
							                                                <input type="tel" class="ipro-form__control" value="<?php echo $city?>" name="city" placeholder="<?php echo __('City','ngl')?>" />
							                                            </div>
							                                        </div>
							                                        <div class="ipro-form__group">
							                                            <select id="newsEvent-filter-year" name="country" class="ipro-form__dropdown ipro-form__dropdown--custom ipro-form__dropdown--primary" style='display:none;width: 100%;'>
							                                               


                                                                              <option  value="" ><?php echo __('country','ngl')?></option>
                                                                                <?php $countries = getCountries();
                                                                                        if($countries):
                                                                                            foreach($countries as $country):
                                                                                ?>
                                                                              
                                                                                    <option  value="<?php echo $country?>" <?php echo $pays==$country?'selected="selected"':''?>><?php echo $country?></option>
                                                                                    <?php           endforeach;
                                                                                            endif;
                                                                                    ?>
                                                
							                                      
							                                            </select>
							                                        </div>
							                                        <div class="ipro-form__inline">
							                                            <div class="ipro-form__group ipro-form__group--inline">
							                                                <input type="email" class="ipro-form__control" value="<?php echo $user_detail->user_email?>" name="email" placeholder="<?php echo __('Email','ngl')?>*" />
							                                            </div>
							                                            <div class="ipro-form__group ipro-form__group--inline">
							                                                <input type="text" class="ipro-form__control" value="<?php echo $telephone?>" name="telephone" placeholder="<?php echo __('Telephone','ngl')?>" />
							                                            </div>
							                                        </div>
							                                        
							                                      <!--   <div class="ipro-form__group ipro-form__group--checkbox">
							                                            <input type="checkbox" name="newsletter" value="1" class="ipro-form__control hidden ipro__control--large" id="newsletter-signup" checked />
							                                            <label for="newsletter-signup"><?php echo __('Newsletter Sign-Up','ngl') ?></label>
							                                        </div> -->
							                                        <input type="hidden" name="user_id" value="<?php echo $user->ID;?>">
							                                        <div class="ipro-form__group ipro-form__group--action text-right">
							                                            <button type="submit" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium submit"><?php echo __('Update Account','ngl')?></button>

                                                                        
							                                        </div>
                                   							 </form>

                                                    </div>
                                                </div><!-- NGL post form -->

                                                <!-- NGL post form -->
                                                <div class="ipro-form-row text-left">
                                                    <h3><?php echo __('Change my password','ngl')?>:</h3>

                                                    <div class="ipro-form__wrap">
                                                        <form class="ipro-form ipro-form--account" id="change_password_form">
                                                            <div class="ipro-form__group">
                                                                <input type="password" class="ipro-form__control" id="password" name="password" placeholder="<?php echo __('Password','ngl')?>" />
                                                            </div>
                                                            <div class="ipro-form__group">
                                                                <input type="password" class="ipro-form__control" name="retype_password" placeholder="<?php echo __('Re-type Password','ngl')?>" />
                                                            </div>
                                                             <input type="hidden" name="guid" value="<?php echo $user->ID;?>">
                                                            <div class="ipro-form__group ipro-form__group--action text-right">
                                                                <button type="submit" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium"><?php echo __('Change Password','ngl')?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div><!-- NGL post form -->

                                                <!-- NGL post form -->
                                                <div class="ipro-form-row text-left">
                                                    <h3><?php echo __('Account deletion','ngl')?> :</h3>

                                                    <div class="ipro-form__wrap">
                                                        <form class="ipro-form ipro-form--account" id="account_delete_form" method="post" action="">
                                                            <div class="ipro-form__group ipro-form__group--action ipro__gap--zeroTop text-right">
                                                             <input type="hidden" name="delete_user_id" value="<?php echo $user->ID;?>">
                                                                <button type="submit" class="btn btn--montana btn--large ipro-form__action" onclick="return confirm('Are you sure you want to delete this account?')"><?php echo __('Delete Account','ngl')?></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div><!-- NGL post form -->
                                                
                                            </div><!-- /.# NGL main container -->
                                        </div><!-- /.#NGL account page form block -->
                                    </div>
                                </div><!-- /.#My account logged in 2 col row -->

                            </div>                            
                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL News details post content block -->
<?php endwhile;?>





                    <script type="text/javascript">
                    	 
                       $(function(){
                         $("#registration_update_form").validate({
                            rules: {
                                email: {
                                    required: true,
                                    email: true
                                },
                                'company': {
                                    required: true,                                   
                                },
                                'office': {
                                    required: true                                    
                                },
                                'fullname': {
                                    required: true                                    
                                },
                                password: "required",
                                retype_password: {
                                    equalTo: "#password"
                                }
                            },
                            
                            submitHandler: function(form) {
                                //form.submit();
                                $('.errorDiv').text('');
                                
                                //console.log(error);

                                var input_data = $('#registration_update_form').serialize();
                                var datas = {action: 'account_update', data: input_data};
                                //console.log(datas);
                                ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                //$('.submit').attr('disabled', true);
                                //$('.submit').val('Wait...');
                                $.ajax({
                                    url: ajaxurl,
                                    type: "POST",
                                    data: datas,
                                    dataType: "json",
                                    success: function(response) {
                                        //console.log(response);

                                        if (response.status == 'error') {

                                          //  $('.submit').attr('disabled', false);
                                           // $('.submit').val('Submit');
                                            $('.errorDiv').html(response.msg).show();
                                            $("html, body").animate({ scrollTop: 0 }, "slow");
                                            return false;
                                        } else if (response.status == 'success') {
                                        	 $('.successDiv').html(response.msg).show();
                                              $("html, body").animate({ scrollTop: 0 }, "slow");
                                            //window.location.href = "<?php echo home_url('create-account-success'); ?>";
                                        } else {

                                            //$('.submit').attr('disabled', false);
                                            $('.errorDiv').html('Something went wrong.').show();
                                            return false;
                                        }


                                    }
                                });
                                return false;
                            }
                        });

                          /*change password*/
				 $("#change_password_form").validate({
                            rules: {
                               password: "required",
                                retype_password: {
                                    equalTo: "#password"
                                }
                            },             
                            submitHandler: function(form) {
                                //form.submit();
                                            
                                $('.successDiv').text('');               
                             

                                var input_data = $('#change_password_form').serialize();
                                var datas = {action: 'reset_password', data: input_data};
                                //console.log(datas);
                                ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                //$('.submit').attr('disabled', true);
                               // $('.submit').val('Wait...');
                                $.ajax({
                                    url: ajaxurl,
                                    type: "POST",
                                    data: datas,
                                    dataType: "json",
                                    success: function(response) {
                                        //console.log(response);

                                        if (response.status == 'error') {

                                           // $('.submit').attr('disabled', false);
                                            //$('.submit').val('Submit');
                                            $('.errorDiv').html(response.msg).show();
                                            $("html, body").animate({ scrollTop: 0 }, "slow");
                                            return false;
                                        } else if (response.status == 'success') {
                                            $('.successDiv').html(response.msg).show();
                                            $('input[type="password"]').val('');
                                            $("html, body").animate({ scrollTop: 0 }, "slow");
                                        } else {

                                            //$('.submit').attr('disabled', false);
                                            $('.errorDiv').html('Something went wrong.').show();
                                            return false;
                                        }


                                    }
                                });
                                return false;
                            }
                        });

                     });

                      

                       
                    </script>
<?php get_footer();?>