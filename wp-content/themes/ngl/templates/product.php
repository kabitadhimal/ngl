<?php /*Template Name: Product Template */?>
<?php get_header();
$productLabel_content = get_field('product_content');
//debug($productLabel_content);
if(!empty($productLabel_content)):
    foreach($productLabel_content as $content):
        ?>

        <?php if(!empty($content['acf_fc_layout']) && $content['acf_fc_layout']=='banner'):

        $image = $content['background_image']['url'];
        $image = \App\getImageManager()->resize(\App\getImageDirectoryPath($image), \App\IMAGE_PRODUCT_BANNER);
        ?>
        <section class="ipro-block ipro-block--heroBanner ipro-block--heroBanner-lg text-center" data-parallax="true" style="background-image: url(<?php echo $image; ?>);">
            <div class="ipro-block__row">
                <div class="ipro-block__caption ipro-block__caption--hero">
                    <div class="ipro-block__content js-anim-init" data-delay="1" data-animation="fadeIn slideInDown">
                        <h1 style="color:<?php echo $content['title_color']?>"><?php echo $content['title']?></h1>
                        <p class="ipro__para ipro__para--big"><?php echo $content['sub_title']?> </p>
                    </div>
                </div><!-- /.#NGL hero banner caption -->
            </div><!-- /.#NGL CMS block row -->
        </section><!-- /.#NGL Product page :: Hero banner -->
    <?php endif;?>

        <?php if(!empty($content['acf_fc_layout']) && $content['acf_fc_layout']=='table'):?>
        <section class="ipro-block ipro-block--gapSmall ipro-block--productGuideTable js-anim-init" data-delay="0.05" data-animation="fadeIn slideInUp" data-theme="theme-palma-stormgrey">
            <div class="ipro-container ipro-container--main">
                <header class="ipro-block__header ipro-block__header--marBtm70 text-center">
                    <h2><?php echo $content['table_title']?></h2>
                </header>
                <?php if(!empty($content['button_text'])):?>
                    <div class="ipro__productAction-row ipro__row ipro__row--marTop105 text-center">
                        <a href="<?php echo $content['download_file']['url']?>" download class="btn btn btn--goldenrod btn--large"><?php echo $content['button_text']?></a>
                    </div>
                <?php endif;?>

                <div class="ipro__productGuideTable-row ipro__row">
                    <?php   if(!empty($content['sliders'])):
                        $sliderCount = count($content['sliders']);
                        ?>
                        <div class="product-slider__wrapper <?php echo $sliderCount <=1?'slider-off':''?>" data-nav-visible="true">
                            <div class="swiper-container  ipro-slider product-slider ipro-slider--full ipro-slider--loading ipro-lightGallery" data-autoplay-speed="4000" data-slide-effect="slide" data-loop="<?php echo $sliderCount<=1?'false':'true'?>">
                                <div class="swiper-wrapper ipro-slider__swiper-wrapper">
                                    <?php foreach ($content['sliders'] as $slider):
                                        ?>
                                        <div class="product-slider__slide swiper-slide">
                                            <a class="lightbox" href="<?php echo $slider['slide_image']['url']?>"><img src="<?php echo $slider['slide_image']['url']?>" class="ipro-valign--middle" alt="<?php echo $slider['slide_image']['alt']?>" /></a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="swiper-pagination ipro-slider__pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    <?php endif;    ?>
                </div>
            </div>
        </section>
    <?php endif;?>

        <?php if(!empty($content['acf_fc_layout']) && $content['acf_fc_layout']=='download_section'):?>
        <section class="ipro-block ipro-block--gapSmall ipro-block--productTable ipro-block--grey js-anim-init" data-delay="0.05" data-animation="fadeIn" data-theme="theme-palma-stormgrey">
            <div class="ipro-container ipro-container--main">
                <header class="ipro-block__header ipro-block__header--marBtm0 ipro-container--text text-center">
                    <h2><?php echo __('DATA SHEETS','ngl'); ?></h2>
                    <p><em><?php echo __('Download the NGL Product Data Sheet for your needs:','ngl');?></em></p>
                </header><!-- /.#NGL news details banner title block -->

                <?php
                $productLabel = "";
                if(ICL_LANGUAGE_CODE=="en"):
                    $productLabel = "Product";
                endif;
                if(ICL_LANGUAGE_CODE=="fr"):
                    $productLabel = "Produit";
                endif;
                if(ICL_LANGUAGE_CODE=="de"):
                    $productLabel = "Produkt";
                endif;
                if(ICL_LANGUAGE_CODE=="it"):
                    $productLabel = "Prodotto";
                endif;
                if(ICL_LANGUAGE_CODE=="es"):
                    $productLabel = "Producto";
                endif;
                if(ICL_LANGUAGE_CODE=="zh-hans"):
                    $productLabel = "产品";
                endif;

                $technicalLabel = "";
                if(ICL_LANGUAGE_CODE=="fr"):
                    $technicalLabel = "Fiche technique";
                endif;
                if(ICL_LANGUAGE_CODE=="de"):
                    $technicalLabel = "Technische Daten";
                endif;
                if(ICL_LANGUAGE_CODE=="en"):
                    $technicalLabel = "Technical sheet";
                endif;
                if(ICL_LANGUAGE_CODE=="it"):
                    $technicalLabel = "Scheda Tecnica";
                endif;
                if(ICL_LANGUAGE_CODE=="es"):
                    $technicalLabel = "Ficha Técnica";
                endif;
                if(ICL_LANGUAGE_CODE=="zh-hans"):
                    $technicalLabel = " 技術論文";
                endif;

                ?>

                <div class="ipro__productTable-row ipro__row ipro__row--marTop40 js-anim-init" data-delay="0.25" data-animation="fadeIn slideInUp">
                    <div class="ipro-table ipro-table--responsive ipro-table--striped">
                        <table class="ipro-productTable">
                            <thead>
                            <tr>
                                <th class="ipro-table__cell ipro-table__cell--caps ipro-table__cell--4"><?php echo $productLabel;?></th>
                                <th class="ipro-table__cell ipro-table__cell--caps ipro-table__cell--8"><?php echo $technicalLabel;?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if(ICL_LANGUAGE_CODE!='fr'){
                                $default_lang_id = icl_object_id( $post->ID, 'product', true, 'fr' );
                            }else{
                                $default_lang_id = $post->ID;
                            }
                            $content1 = get_field('product_content',$default_lang_id);

                            if(!empty($content1[2]['products'])):
                                $count = 1;
                                foreach($content1[2]['products'][0]['select_product'] as $productLabel1):
                                    $productLabel_id = $productLabel1->ID;
                                    ?>
                                    <tr>
                                        <td align="left" class="padding-large"><?php echo $productLabel1->post_title;?></td>
                                        <td>
                                            <?php
                                              if(ICL_LANGUAGE_CODE=="en") {
                                                  $desc = get_field('product_description_en',$productLabel_id);
                                              }elseif (ICL_LANGUAGE_CODE=="es") {
                                                  $desc = get_field('product_description_es',$productLabel_id);
                                              }elseif (ICL_LANGUAGE_CODE=="de"){
                                                  $desc = get_field('product_description_de',$productLabel_id);
                                              }elseif (ICL_LANGUAGE_CODE=="it"){
                                                  $desc = get_field('product_description_it',$productLabel_id);
                                              }elseif (ICL_LANGUAGE_CODE=="zh-hans"){
                                                  $desc = get_field('product_description_zh_hans',$productLabel_id);
                                              }else {
                                                  $desc = get_field('product_description_fr',$productLabel_id);
                                              }
                                            ?>
                                            <a data-toggle="collapse" href="#collapse-<?=$count?>" role="button" aria-expanded="false" aria-controls="collapse-<?=$count?>">
                                               <?=$desc?>
                                            </a>
                                            <div class="collapse" id="collapse-<?=$count?>">
                                                <table class="ipro-subTable">
                                                    <tr>
                                                        <td class="ipro-table__cell ipro-table__cell--3">
                                                            <i class="ipro-picto ipro-picto--small ipro-picto--pdf"></i>
                                                        </td>
                                                        <td class="ipro-table__cell ipro-table__cell--9">
                                                            <ul class="ipro__list ipro__list--lang product-listing">
                                                                <?php
                                                                $file = get_field('product_file',$productLabel_id);
                                                                $file_en = get_field('product_file_en',$productLabel_id);
                                                                $file_es = get_field('product_file_es',$productLabel_id);
                                                                $file_de = get_field('product_file_de',$productLabel_id);
                                                                $file_it= get_field('product_file_it',$productLabel_id);
                                                                $file_zh= get_field('product_file_zh_hans',$productLabel_id);
                                                                //Download attribute to download the document directly
                                                                /* <li><a href="" download class="ipro-link ipro-link--default" target="_blank">Link title </a></li>*/
                                                                ?>
                                                                <li><a href="<?php echo $file['url']?>" class="ipro-link ipro-link--default" target="_blank"><?php echo get_respective_countryName(ICL_LANGUAGE_CODE,'fr');?></a></li>
                                                                <?php if(!empty($file_en)):?>
                                                                    <li><a href="<?php echo $file_en['url']?>" class="ipro-link ipro-link--default" target="_blank"><?php echo get_respective_countryName(ICL_LANGUAGE_CODE,'en');?></a></li>
                                                                <?php endif;?>
                                                                <?php if(!empty($file_de)):?>
                                                                    <li><a href="<?php echo $file_de['url']?>" class="ipro-link ipro-link--default" target="_blank"><?php echo get_respective_countryName(ICL_LANGUAGE_CODE,'de');?></a></li>
                                                                <?php endif;?>
                                                                <?php if(!empty($file_it)):?>
                                                                    <li><a href="<?php echo $file_it['url']?>" class="ipro-link ipro-link--default" target="_blank"><?php echo get_respective_countryName(ICL_LANGUAGE_CODE,'it');?></a></li>
                                                                <?php endif;?>
                                                                <?php if(!empty($file_es)):?>
                                                                    <li><a href="<?php echo $file_es['url']?>" class="ipro-link ipro-link--default" target="_blank"><?php echo get_respective_countryName(ICL_LANGUAGE_CODE,'es');?></a></li>
                                                                <?php endif;?>
                                                                <?php if(!empty($file_zh)):?>
                                                                    <li><a href="<?php echo $file_zh['url']?>" class="ipro-link ipro-link--default" target="_blank"><?php echo get_respective_countryName(ICL_LANGUAGE_CODE,'zh-hans');?></a></li>
                                                                <?php endif;?>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                <?php $count++; endforeach;
                            endif;?>
                            </tbody>
                        </table>
                        <!--New Layout-->


                    </div>
                </div>
            </div><!-- /.# NGL main container -->
        </section><!-- /.#NGL Product page table block -->
    <?php endif;?>
    <?php endforeach;
endif;?>
<?php get_footer();?>