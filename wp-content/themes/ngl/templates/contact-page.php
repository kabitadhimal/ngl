<?php /*Template Name: Contact Template */?>

<?php get_header();?>

 <?php while(have_posts()): the_post(); ?>
    <section class="ipro-banner ipro-block--loggedIn ipro-banner--account">
        <div class="ipro-container ipro-container--main">
            <div class="ipro-banner__title text-center">
                <h2><?php the_title()?></h2>
                <?php the_content(); ?>
            </div>
        </div>
    </section>
 <?php $sections = get_field('contact_');
  if(!empty($sections)):
  		foreach ($sections as  $section):?>
			<?php if(!empty($section['acf_fc_layout']) && $section['acf_fc_layout']=='icons'):
                include get_template_directory().'/src/contact-form.php';
               // include get_template_directory().'/src/new-contact-form.php';
		        endif; ?>
	<?php if(!empty($section['acf_fc_layout']) && $section['acf_fc_layout']=='headquarter_section'):?>
            <section class="ipro-block ipro-block--full ipro-block--goldenrod ipro-block--quote ipro-block--headerquarter text-center" data-delay="0.0005" data-theme="theme-default"> <!--removed Class (js-anim-init) &  data-animation="fadeIn"-->
                <div class="ipro-block__row">
                    <div class="ipro-container ipro-container--main">

                        <div class="ipro-block__text" data-delay="0.25">  <!--removed Class (js-anim-init) &  data-animation="fadeIn slideInDown"-->
                            <h2><?php echo $section['title']?></h2>
                            <address>
                                <?php echo strip_tags($section['headquarter_information']);?> / <a href="tel:<?php echo $section['phone_number']?>"><?php echo __('Tel.','ngl')?><?php echo $section['phone_number']?></a>
                            </address>
                        </div>
                    </div><!-- /.#NGL CMS block container -->
                </div><!-- /.#NGL CMS block row -->
            </section><!-- /.#NGL Subsidiaries page contact details block -->
	<?php endif;?>

	<?php if(!empty($section['acf_fc_layout']) && $section['acf_fc_layout']=='subsidiaries_section'):?>

        <section class="ipro-block ipro-block--contactDetails ipro-block--gapTiny" data-delay="0.0005" data-theme="theme-palma-stormgrey"> <!--removed Class (js-anim-init) &  data-animation="fadeIn"-->
                <div class="ipro-container ipro-container--main">
                    <header class="ipro-block__header ipro-block__header--marBtm80 text-center">
                        <h2><?php echo $section['title']?></h2>
                    </header><!-- /.#NGL news details banner title block -->
                    <div class="ipro__row ipro__row--contactDetails ipro__row-marBtn80">
                        <!-- NGL contact details grid row -->
                        <div class="row clearfix ipro-flex ipro-flex--wrap ipro-flex--gutter90">
                            <?php foreach($section['subsidaries'] as $subsidary):?>
                            <div class="col-sm-4 col-xs-12 ipro-flex__col ipro-contact__col js-last-col" data-delay="0.05"> <!--removed Class (js-anim-init) &   data-animation="fadeIn slideInUp"-->
                                <div class="ipro-col__content ipro__pad--lefRgt30">
                                    <h3><?php echo $subsidary['country']?></h3>
                                    <address>
                                       <?php echo $subsidary['address']?>
                                    </address>
                                    <div class="ipro-contact__meta">
                                        <span class="ipro__contact ipro__contact--tel">
                                     <!--    <a href="tel:<?php //echo $subsidary['phone_number']?>" class="ipro-link ipro-link--darkGoldenrod ipro-link--underline"> -->
                                        <?php echo __('Tel.','ngl')?> <?php echo $subsidary['phone_number']?>

                                      <!--   </a> -->
                                        </span>
                                        <!--<span class="ipro__contact ipro__contact--mail"><a href="mailto:<?php /*echo $subsidary['email_address']*/?>" class="ipro-link ipro-link--darkGoldenrod ipro-link--underline"><?php /*echo $subsidary['email_address']*/?></a></span>-->
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                        </div><!-- /.#NGL contact details grid row -->
                    </div><!-- /.#NGL contact details row -->
                </div><!-- /.#NGL CMS block container -->
            </section>
	<?php endif;
  		endforeach;
  	endif;
  	endwhile; ?>
<?php get_footer();