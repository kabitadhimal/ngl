<?php /*Template Name: Create Account New Template */?>
<?php get_header();

while(have_posts()): the_post();



?>

     <script type="text/javascript" src="https://secure.inescrm.com/js/jcap.js"></script>               
         <!-- NGL news details title banner -->
                    <section class="ipro-banner ipro-block--loggedIn ipro-banner--account">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL news details banner title -->
                            <div class="ipro-banner__title text-center">
                                <h2><?php echo the_title()?></h2>
                                <p><em><?php echo the_content()?></em></p>
                            </div><!-- /.#NGL news details banner title block -->

                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL news details title banner -->

                    <!-- NGL News details post content block -->
                    <section class="ipro-block ipro-block--form ipro-block--signup">
                        <div class="ipro-container ipro-container--medium">
                            <div class="ipro-account__row ipro-signup__row">

                                <!-- NGL post form -->
                                <div class="ipro-form__wrap">
                                    <!-- Displays errors thrown by server -->
                                    <div  class="errorDiv error"></div>
                                    <!-- /.#Displays errors thrown by server -->

                                    <form class="ipro-form ipro-form--account" id="registration_form">

                                            <input type=hidden name="controlid" id="controlid" value="722600388" />
                                            <input type=hidden name="oid" id="oid" value="e13f94c9-f977-495a-aaa4-590676109ce0" />
                                            <input type=hidden name="formid" id="formid" value="4103" />
                                            <input type=hidden name="retURL" id="retURL" value="https://www.ngl-group.com"/>
                                            <input type=hidden name="data" id="data" value="" />
                                            <input type=hidden name="Alias" id="Alias" value="nglcrm" />
                                            <input type=hidden name="ngl__" value=""/>

                                        <div class="ipro-form__group">
                                            <input type="text" class="ipro-form__control" name="TextBox_73661" placeholder="<?php echo __('Company Name *','ngl')?>" />
                                            
                                        </div>

                                          <div class="ipro-form__group">
                                            <input type="text" class="ipro-form__control" name="TextBox_73663" placeholder="<?php echo __('First Name *','ngl')?>" />
                                        </div>

                                         <div class="ipro-form__group">
                                            <input type="text" class="ipro-form__control" name="TextBox_73662" placeholder="<?php echo __('Name *','ngl')?>" />
                                        </div>

                                          <div class="ipro-form__group">
                                            <input type="tel" class="ipro-form__control" name="TextBox_73670" placeholder="<?php echo __('Job Title *','ngl')?>" />
                                        </div>
                                      

                                           <div class="ipro-form__group">
                                            <input type="text" class="ipro-form__control" name="TextArea_73665" placeholder="<?php echo __('Address 1','ngl')?>" />
                                        </div>

                                        <div class="ipro-form__inline">
                                            <div class="ipro-form__group ipro-form__group--inline">
                                                <input type="text" class="ipro-form__control" name="TextBox_73666" placeholder="<?php echo __('Code postal','ngl')?>" />
                                            </div>

                                            <div class="ipro-form__group ipro-form__group--inline">
                                                <input type="tel" class="ipro-form__control" name="TextBox_73667" placeholder="<?php echo __('City','ngl')?>" />
                                            </div>
                                        </div>
                                      

                                     
                                        
                                        
                                        <div class="ipro-form__group">
                                            <select id="newsEvent-filter-year" name="TextBox_73668" required class="ipro-form__dropdown ipro-form__dropdown--custom ipro-form__dropdown--primary" style='display:none;width: 100%;'>
                                                <option  value="" ><?php echo __('Country *','ngl')?></option>

                                             <?php 
                                                $countries = getCountries();
                                             if($countries):
                                                        foreach($countries as $country):
                                                                                ?>                                                                            
                                                            <option  value="<?php echo $country?>"><?php echo $country?></option>
                                                <?php endforeach;
                                                  endif;
                                                ?>                          
                                            </select>
                                        </div>


                                        <div class="ipro-form__group">
                                       
                                        <input type="email" class="ipro-form__control" name="TextBox_73664" placeholder="<?php echo __('E-mail *','ngl')?>" />
                                          
                                            
                                        </div>
                                       
                                       
                                        <div class="ipro-form__group">
                                            <input type="tel" class="ipro-form__control" name="TextBox_73669" placeholder="<?php echo __('Telephone','ngl')?>" />
                                        </div>

                                      
                                        <div class="ipro-form__group">
                                            <input type="password" class="ipro-form__control" id="password" name="password" placeholder="<?php echo __('Password *','ngl')?>" />
                                        </div>
                                         <div class="ipro-form__group">
                                            <input type="password" class="ipro-form__control" name="retype_password" placeholder="<?php echo __('Re-type Password','ngl')?>" />
                                        </div>
                                        <div class="ipro-form__group ipro-form__group--checkbox">

                                        <label for="newsletter-signup"><?php echo __('Newsletter Sign-Up','ngl') ?></label>

                                            <div class="ipro__utilities--form">
                                                <div class="ipro-form__group ipro-form__group--custom">
                                                    <input type="radio" value="1" name="Checkbox_76009" class="ipro-form__control hidden" id="Checkbox_76009_Vrai" checked>
                                                    <label for="Checkbox_76009_Vrai"><?php echo __('Yes','ngl')?></label>
                                                </div>
                                                <div class="ipro-form__group ipro-form__group--custom">
                                                    <input type="radio" value="0" name="Checkbox_76009" class="ipro-form__control hidden" id="Checkbox_76009_Faux">
                                                    <label for="Checkbox_76009_Faux"><?php echo __('No','ngl')?></label>
                                                </div>
                                            </div>                                           

                                        </div>
                                        <div class="ipro-form__inline gap-top20">
                                         <div class="ipro-form__group ipro-form__group--inline"> Veuillez recopier le code ci-dessous</div>
                                          <div class="ipro-form__group ipro-form__group--inline"> <div id="captchaDiv"></div></div>
                                             <script type="text/javascript">sjcap('captchaDiv','txtCaptcha',null);</script>
                                        </div>
                                        <div class="ipro-form__group ipro-form__group--action text-center">
                                            <button type="submit" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium submit"><?php echo __('Create an Account','ngl')?></button>
                                        </div>
                                    </form>

                                </div><!-- NGL post form -->

                            </div>                            
                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL News details post content block -->
<?php endwhile; ?>

                    <script type="text/javascript">
                    	 
                       $(function(){


                        
                        // var $select2Elem = $('select + .select2')
                        // $.validator.addMethod('selectCheck', function (value) {
                        //     return (value == '');
                        // }, "year required");

                         jQuery("#registration_form").validate({
                            ignore: [],
                            errorPlacement: function(error, element) {
                                error.appendTo($(element).closest('.ipro-form__group'));
                            },
                            rules: {
                                TextBox_73664: {
                                    required: true,
                                    email: true
                                },
                                'TextBox_73661': {
                                    required: true,                                   
                                },
                                'TextBox_73662': {
                                    required: true                                    
                                },
                                'TextBox_73663': {
                                    required: true                                    
                                },
                                // 'TextArea_73665': {
                                //     required: true                                    
                                // },
                                // 'TextBox_73666': {
                                //     required: true                                    
                                // },
                                // 'TextBox_73667': {
                                //     required: true                                    
                                // },
                                'TextBox_73668': {
                                    required: true,
                                    //selectCheck: true                                   
                                },
                                // 'TextBox_73669': {
                                //     required: true                                    
                                // },
                                'TextBox_73670': {
                                    required: true                                    
                                },
                                password: "required",
                                retype_password: {
                                    equalTo: "#password"
                                }
                            },
                            submitHandler: function(form) {
                                ignore: [],
                                //form.submit();
                                jQuery('.errorDiv').text('');
                                
                                //console.log(error);
                                $('.ipro__loader-wrap').show();
                                var input_data = jQuery('#registration_form').serialize();
                                var datas = {action: 'account_creation_new', data: input_data};
                                //console.log(datas);
                                ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                //jQuery('.submit').attr('disabled', true);
                                //jQuery('.submit').val('Wait...');
                                jQuery.ajax({
                                    url: ajaxurl,
                                    type: "POST",
                                    data: datas,
                                    dataType: "json",
                                    success: function(response) {
                                        //console.log(response);

                                        if (response.status == 'error') {
                                             $('.ipro__loader-wrap').hide();
                                           // jQuery('.submit').attr('disabled', false);
                                            //jQuery('.submit').val('Submit');
                                            jQuery('.errorDiv').html(response.msg).show();
                                            jQuery("html, body").animate({ scrollTop: 0 }, "slow");
                                            return false;
                                        } else if (response.status == 'success') {
                                             $('.ipro__loader-wrap').hide();
                                            window.location.href = "<?php echo home_url('create-account-success'); ?>";
                                        } else {
                                             $('.ipro__loader-wrap').hide();
                                            //jQuery('.submit').attr('disabled', false);
                                            jQuery('.errorDiv').html('Something went wrong.').show();
                                            return false;
                                        }


                                    }
                                });
                                return false;
                            }
                        });
                     });
            
                    </script>
<?php get_footer();?>