<?php /*Template Name: Forget Password Template */?>

<?php get_header();
while(have_posts()):the_post();
?>


<!-- NGL news details title banner -->
                    <section class="ipro-banner ipro-block--loggedIn ipro-banner--account">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL news details banner title -->
                            <div class="ipro-banner__title text-center">
                                <h2><?php the_title(); ?></h2>
                                <p><em><?php the_content(); ?></em></p>
                            </div><!-- /.#NGL news details banner title block -->

                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL news details title banner -->

                    <!-- NGL News details post content block -->
                    <section class="ipro-block ipro-block--form ipro-block--changePassword">
                        <div class="ipro-container ipro-container--medium">
                            <div class="ipro-account__row ipro-changePassword__row">

                                <!-- NGL post form -->
                                <div class="ipro-form__wrap">
                                    <div class="successDiv success"></div>
                                    <div class="errorDiv div"></div>
                                    
                                    <form class="ipro-form ipro-form--account" id="forget_password_form">
                                        <div class="ipro-form__group">
                                            <input type="text" class="ipro-form__control" id="email" name="email" placeholder="<?php echo __('Your email address*','ngl')?>" />
                                        </div>
                                        <div class="ipro-form__group ipro-form__group--action text-center">
                                            <button type="submit" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium submit"><?php echo __('Change Password','ngl')?></button>
                                        </div>
                                    </form>

                                </div><!-- NGL post form -->

                            </div>                            
                        </div><!-- /.# NGL main container -->
                    </section><!-- /.#NGL News details post content block -->
<?php endwhile;?>

                      <script type="text/javascript">
                         $(function(){

                            var passwordChangeText = "<?php echo __('Change Password','ngl')?>";
                     
                            $("#forget_password_form").validate({
                                rules: {
                                    email: {
                                        required: true,
                                        email: true
                                    }
                                },             
                                submitHandler: function(form) {
                                    //form.submit();
                                    $('.errorDiv').text('');               
                                    $('.successDiv').text('');               
                                 
                                    $('.ipro__loader-wrap').show();
                                    var input_data = $('#forget_password_form').serialize();
                                    var datas = {action: 'forget_password', data: input_data};
                                    //console.log(datas);
                                    ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                                    //$('.submit').attr('disabled', true);
                                    //$('.submit').text('Wait...');
                                    $.ajax({
                                        url: ajaxurl,
                                        type: "POST",
                                        data: datas,
                                        dataType: "json",
                                        success: function(response) {
                                            //console.log(response);

                                            if (response.status == 'error') {

                                                //$('.submit').attr('disabled', false);
                                                $('#email').val('');
                                                $('.submit').text(passwordChangeText);
                                                $('.ipro__loader-wrap').show();
                                                $('.errorDiv').html(response.msg).show();
                                                $("html, body").animate({ scrollTop: 0 }, "slow");
                                                return false;
                                            } else if (response.status == 'success') {
                                                 $('.ipro__loader-wrap').hide();
                                                 $('#email').val('');
                                                $('.submit').text(passwordChangeText);
                                                $('.successDiv').html(response.msg).show();
                                                $("html, body").animate({ scrollTop: 0 }, "slow");
                                            } else {
                                                 $('.ipro__loader-wrap').hide();
                                                  $('.submit').text(passwordChangeText);
                                                //$('.submit').attr('disabled', false);
                                                $('.errorDiv').html('Something went wrong.').show();
                                                return false;
                                            }


                                        }
                                    });
                                    return false;
                                }
                            });
                        });
                                           </script>
<?php get_footer();?>