<?php /*Template Name: News/Event Template */?>
<?php get_header(); ?>
    <style>
        .spinner-category-wrap{
            position: relative;
        }
        .spinner-category{
            position: absolute; top: 0; left: 50%; transform: translateX(-50%);
        }
        .spinner-category img{
            width: 80px !important;
        }
    </style>

    <section id="ipro-eventListing-block" class="ipro-block ipro-block--newsEvents ipro-block--iseventListing text-center" >
        <div class="ipro-container ipro-container--large">
            <header class="ipro-block__header ipro-block__header--small">
                <h2><?php the_title();?></h2>
            </header>
            <div id="ipro-filter__controls" class="ipro-filter__controls ipro-filter__control--newsEvents ipro__accordion--mobile">
                <a href="" class="js-mbl-accordion ipro__ui ipro__ui--plus" data-collapse="ipro__accordion--mobile-panel"><?php echo __('Filter News','ngl')?></a>
                <div class="ipro-form--flex ipro__accordion--mobile-panel">
                    <div class="ipro-form__group ipro-form__group--filter ipro-form__group--inline checkboxes-group all-news">
                        <input type="checkbox" value="all" name="category[]"  class="js-filter-checkbox ipro-form__control ipro-form__control--custom hidden" id="tous" />
                        <label for="tous"><?php echo __('All','ngl')?></label>
                    </div>
                    <?php
                    $categories = get_categories( ['hide_empty'  => 1, 'suppress_filters'  =>  false]);
                    foreach ($categories as $cat) {
                        ?>
                        <div class="ipro-form__group ipro-form__group--filter ipro-form__group--inline checkboxes-group">
                            <input type="checkbox" value="<?=$cat->term_id?>" name="category[]" class="ipro-form__control ipro-form__control--custom hidden js-filter-checkbox" id="<?=$cat->term_id?>" />
                            <label for="<?=$cat->term_id?>"><?=$cat->name?></label>
                        </div>
                    <?php } ?>
                    <div class="ipro-form__group ipro-form__group--filter ipro-form__group--inline">
                        <select id="newsEvent-filter-year" class="ipro-form__dropdown" style='display:none;width: 100%;'>
                            <option id="" value="" data-year=""><?php echo __('Date','ngl')?></option>
                            <?php
                            $years = $wpdb->get_results( "SELECT YEAR(post_date) AS year FROM ngl_posts WHERE post_type = 'post' AND post_status = 'publish' GROUP BY year DESC" );
                            foreach($years as $year):?>
                                <option value="<?php echo $year->year; ?>" ><?=$year->year?></option>
                            <?php endforeach;?>

                        </select>
                    </div>
                </div>
            </div>

            <div class="ipro__loader-wrap hide" id="loader">
                <div class="loader">Loading..</div>
            </div>

            <div class="ipro-block__content ipro-block__gutter ipro-block__gutter--small" style="margin-top: 80px">
                <div id="ipro-filter--newsEventsListing " class="row ipro-flex ipro-flex--wrap ipro-filter--newsEventsListing js-news-list">
                </div>
                <div class="ipro-filter__messages ipro-filter__messages--noresults js-not-found hide">
                    <span class="messages--noresult">  <p><?php _e('No matching results!!! Please try something else.', 'ngl'); ?></p></span>
                </div>

            </div>
        </div>
    </section>

    <script>
        jQuery(function(){
            jQuery('#newsEvent-filter-year').select2().on('change', function () {
                submitForm();
            });

            [].forEach.call(document.querySelectorAll('.js-filter-checkbox'), function(element){
                element.addEventListener('change', function(event){
                    var inputElement = event.currentTarget;
                    var count = document.querySelectorAll('.js-filter-checkbox:checked').length;

                    if(inputElement.checked && inputElement.value != 'all'){
                        document.querySelector('.js-filter-checkbox[value="all"]').checked = false;
                    }

                    if(inputElement.checked && inputElement.value == 'all'){
                        document.querySelectorAll('.js-filter-checkbox:not([value="all"])').forEach(function(item){
                            item.checked = false;
                        });
                    }
                    if(count == 0) document.querySelector('.js-filter-checkbox[value="all"]').checked = true;
                    submitForm();
                })
            });


        });

        function submitForm() {

            var selectedCategories = [];
            var catInput ="" ;
            var filterCheckbox =   document.querySelectorAll(".js-filter-checkbox");

            if(filterCheckbox.length > 0 )  {
                [].forEach.call(filterCheckbox, function (item) {
                    if (item.checked) selectedCategories.push(item.value);
                });
                if(selectedCategories.length > 0) {
                    catInput = selectedCategories.join(",");
                }
            }

            var dateInput = document.getElementById("newsEvent-filter-year");
            if(dateInput) {
                var year = dateInput.options[dateInput.selectedIndex].value;
                console.log("current date is "+year);
            }

            var frmData = 'cat='+catInput +'&date='+year+'&action=display_news_contents';
            var ajaxURL = "<?=admin_url('admin-ajax.php')?>";
            $.ajax({
                url: ajaxURL,
                data: frmData,
                beforeSend: function(){
                    $("#loader").removeClass("hide");

                },
                complete: function(){
                 //  $('#loading-image').hide();
                    $("#loader").addClass("hide");
                },
                success: function(response){
                    if (response.length > 0) {
                        $(".js-news-list").show();
                        $(".js-news-list").html(response);
                        isProtectedNews($('.ipro-newsEvents__col--isprotected'));
                        // Message to display
                        var newsMessage = '<?php  _e('You need to have an account to read this news,<br/> <a href="#">please log to your account or create one</a>','ngl'); ?>';
                        function isProtectedNews($news) {
                            if($news.length === 0) return;
                            $news.each(function() {
                                var $self = $(this),
                                    $selfChild = $self.find('>div');
                                $selfChild.on('mouseover', function() {
                                    if($self.find('.protected-news-message').length === 0) {
                                        $('<div>', {'class': 'protected-news-message'}).prependTo($selfChild);
                                        $('.protected-news-message').html('<h4>' + newsMessage + '</h4>');
                                    }
                                });
                                $self.on('mouseleave', function() {
                                    $('.protected-news-message').remove();
                                });
                            });
                        }
                    }else {
                        $(".js-news-list").hide();
                        $(".js-not-found").removeClass("ipro-filter__messages");
                        $(".js-not-found").removeClass("hide");
                    }
                }
            });
        }
   jQuery( document ).ready(function() {
       $('#tous').prop('checked', true);
       submitForm();
   });

    </script>
<?php get_footer();