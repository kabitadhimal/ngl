<?php /*Template Name: Concept Template */?>



<?php get_header(); ?>







<?php $theme_url = get_template_directory_uri(); ?>

    <!-- Concept page building blocks begins -->



    <section class="ipro-block ipro-block--heroBanner ipro-block--heroBanner-sm text-center" data-parallax="true" style="background-image: url(<?php echo $theme_url; ?>/images/concept/concept-banner01.jpg);">

        <div class="ipro-block__row">

            <!-- NGL hero banner caption -->

            <div class="ipro-block__caption ipro-block__caption--hero">

                <div class="ipro-block__content">

                    <h1><?=__('WHAT IS YOUR NEED ?','concept');?></h1>

                    <p class="ipro__para ipro__para--big"><?=__('Expert in chemistry for precision cleaning in industrial environments, we offer you our ‘Swiss know-how’ to bring you tailored solutions, respectful of the person and the environment.','concept')?></p>

                </div>

            </div><!-- /.#NGL hero banner caption -->

        </div><!-- /.#NGL CMS block row -->

    </section><!-- /.#NGL Concept page :: Hero banner -->



    <!-- NGL concept page cleaning problem block -->

    <section id="scene-1" class="ipro-block-x concept-block-x ipro-block--cleaning-problem-x story" data-theme="no-default" data-bgk-color="white">

        <div class="story-sep">

            <i class="picto-circle picto-blue story-sep__icon"><img src="<?=$theme_url?>/assets-concept/images/picto/icon-q.png" /></i>

        </div>

        <div class="story-path story-path--cr story-path--top">

            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#f7f7f7" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-1-path-1" fill="none" stroke="#1C79B2" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>



        <div class="story-row  story-row--disable">

            <div class="story-col story-col--order-first  story-col--sm">

                <div class="story-col-txt story-ani">

                    <h2 class="txt-dark txt-xl txt-xl story-ani__i"><?=__('PROBLEMS WITH CLEANING OR SURFACE PREPARATION ?','concept');?></h2>

                    <div class="story-ani__i"><strong><?=__('Chemistry & processes adapted to your needs','concept')?></strong> </div>

                    <div class="story-ani__i"><?=__('NGL formulates, manufactures and markets chemicals, mainly water-based, for precision cleaning and surface preparation in industrial environments.','concept');?> </div>

                </div>

            </div>

            <div class="story-col story-col--lg ">

                <div class=" story-slides">

                    <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/lab/lab-1.jpg" />

                    <img alt="" id="story-lab-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/lab/lab-2.jpg" />

                </div>

            </div>

        </div>



        <div class="story-path story-path--rc story-path--btm">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#f7f7f7" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-1-path-2" fill="none" stroke="#1C79B2" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>

    </section><!-- /.#NGL concept page cleaning problem block -->



    <!-- NGL concept page concept support block -->

    <section id="scene-2" class="ipro-block-x concept-block-x concept-block--full-x concept-block--is-inverted-x ipro-block--concept-support-x story" data-theme="theme-default" data-parallax="true" data-bgk-color="green" style="background-image: url(<?php echo $theme_url; ?>/images/concept/concpet-support-bkg.jpg);">

        <div class="story-sep">

            <i class="picto-circle picto-green story-sep__icon"><img src="<?=$theme_url?>/assets-concept/images/picto/icon-ac.png" /></i>

        </div>

        <div class="story-path story-path--cl story-path--top story-path--reverse">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none" stroke-width="10" stroke="#8bd45f" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-2-path-1" fill="none" stroke="#ffffff" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>



        <div class="story-row  story-row--disable">

            <div class="story-col story-col--lg ">

                <div class=" story-map-slides">

                    <img alt="" id="story-map-slides-1" class="story-map-slides__i" src="<?=$theme_url?>/assets-concept/images/map/map-gray.png" />

                    <img alt="" id="story-map-slides-2" class="story-map-slides__i" src="<?=$theme_url?>/assets-concept/images/map/map-lines.png" />





                    <img alt="" id="story-map-slides-4"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-pins-fr.png" />

                    <img alt="" id="story-map-slides-5"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-pins-dk.png" />

                    <img alt="" id="story-map-slides-6"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-pins-de.png" />





                    <img alt="" id="story-map-slides-7"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-pins-sg.png" />

                    <img alt="" id="story-map-slides-8"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-pins-us.png" />

                    <img alt="" id="story-map-slides-9"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-pins-c.png" />



                    <img alt="" id="story-map-slides-3"  class="story-map-slides__i"  src="<?=$theme_url?>/assets-concept/images/map/map-marker.png" />

                </div>

            </div>

            <div class="story-col story-col--order-first  story-col--sm flex-align-left">

                <div class="story-col-txt  story-ani">

                    <h2 class=" txt-xl story-ani__i"><?=__('NEED LOCAL TECHNICAL SUPPORT ?','concept');?></h2>

                    <div class=" story-ani__i"><?=__('<strong>An international Application Centre network</strong>:<br />Free technical support and service, available in all of our branches, to respond quickly to your needs.','concept');?></div>

                </div>

            </div>

        </div>



        <div class="story-path story-path--lc story-path--btm">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

                <path fill="none"  stroke-width="10" stroke="#8bd45f"  d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-2-path-2" fill="none" stroke="#ffffff" stroke-width="10" class="story-path__l st0" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>



</svg>

        </div>

    </section><!-- /.#NGL concept page concept support block -->



    <!-- NGL concept page bath control block -->

    <section id="scene-3" class="ipro-block-x concept-block-x ipro-block--bath-control-x story" data-theme="no-default" data-parallax="true" data-bgk-color="white" style="background-image: url(<?php echo $theme_url; ?>/assets-concept/images/biglab/biglab-bck.jpg);">

        <div class="story-sep">

            <i class="picto-circle picto-silver story-sep__icon"><img src="<?=$theme_url?>/assets-concept/images/picto/icon-beaker.png" /></i>

        </div>

        <div class="story-path story-path--cr story-path--top">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#dcdcdc" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-3-path-1" fill="none" stroke="#1C79B2" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>



        <div class="story-row  story-row--disable">

            <div class="story-col story-col--order-first story-col--smx">

                <div class="story-col-txt story-ani">

                    <h2 class="txt-dark txt-xl story-ani__i story-col-txt__i-lg"><?=__('NEED TO MASTER & CONTROL BATHS ?','concept');?></h2>

                    <div class="story-ani__i"><strong><?=__('NGL offers analysis and monitoring devices:','concept');?></strong></div>

                    <div class="story-ani__i"><?=__('-	UPC 3000, Aquasnap ... to measure key parameters<br />-	Nanoclean EW, RW ... to filter, recycle network water','concept');?></div>

                </div>

            </div>

            <div class="story-col story-col--lgx ">

                <div class="story-slides">

                    <img alt="" id="story-blab-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/biglab/biglab-1.png" />

                    <img alt="" id="story-blab-slides-2" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/biglab/biglab-2.png" />

                </div>

            </div>

        </div>





        <div class="story-path story-path--rc story-path--btm">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#dcdcdc" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-3-path-2" fill="none" stroke="#1C79B2" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>

    </section><!-- /.#NGL concept page bath control block -->



    <!-- NGL concept page residual water block -->

    <section id="scene-4"  class="ipro-block-x concept-block-x concept-block--full-x concept-block--is-inverted-x ipro-block--residual-water-x story" data-theme="theme-default" data-bgk-color="green">

        <div class="story-sep">

            <i class="picto-circle picto-light story-sep__icon"><img src="<?=$theme_url?>/assets-concept/images/picto/icon-idea.png" /></i>

        </div>

        <div class="story-path story-path--cl story-path--top story-path--reverse">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#91d361" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-4-path-1" fill="none" stroke="#ffffff" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>



        <div class="story-row  story-row--disable">

            <div class="story-col ">

                <div class="story-slides">

                    <?php if(ICL_LANGUAGE_CODE=='en'): ?>
                    <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-1-en.jpg" />
                    <img alt="" id="story-filtre-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-2-en.jpg" />

                    <?php elseif(ICL_LANGUAGE_CODE=='de'):?>

						<img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-1-de.jpg" />
                   	 <img alt="" id="story-filtre-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-2-de.jpg" />
                     <?php elseif(ICL_LANGUAGE_CODE=='es'):?>

                        <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-1-es.jpg" />
                     <img alt="" id="story-filtre-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-2-es.jpg" />
                     <?php elseif(ICL_LANGUAGE_CODE=='it'):?>

                        <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-1-it.jpg" />
                     <img alt="" id="story-filtre-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-2-it.jpg" />
                     <?php elseif(ICL_LANGUAGE_CODE=='zh-hans'):?>

                        <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-1-cn.jpg" />
                     <img alt="" id="story-filtre-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filter-2-cn.jpg" />
                     <?php else: ?>
                        <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filtre-1.jpg" />
                    <img alt="" id="story-filtre-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/filtre/filtre-2.jpg" />

                <?php endif; ?>

                </div>

            </div>

            <div class="story-col story-col--order-first  flex-align-left">

                <div class="story-col-txt story-ani">

                    <h2 class=" txt-xl story-ani__i"><?=__('NEED TO TREAT RESIDUAL WATER ?','concept')?></h2>

                    <div class="story-ani__i"><?=__("Tailor-made solutions before discharge to the public pipeline:<br />Physical-chemical treatment with our DECOFLOC process (pH correction, insolubility, coagulation ...)<br /><br />Physical treatment with our ECOSTILL process (concentration of evaporation baths)",'concept');?></div>

                </div>

            </div>

        </div>



        <div class="story-path story-path--lc story-path--btm">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

                <path fill="none"  stroke-width="10" stroke="#91d361" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-4-path-2" fill="none" stroke="#ffffff" stroke-width="10" class="story-path__l st0" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>



</svg>

        </div>

    </section><!-- /.#NGL concept page residual water block -->



    <!-- NGL concept page enrich knowledge block -->

    <section  id="scene-5" class="ipro-block-x concept-block-x ipro-block--enrich-idea-x story" data-theme="no-default" data-bgk-color="white">

        <div class="story-sep">

            <i class="picto-circle picto-grey story-sep__icon"><img src="<?=$theme_url?>/assets-concept/images/picto/icon-m-aca.png" /></i>

        </div>

        <div class="story-path story-path--cr story-path--top">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#f7f7f7" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-5-path-1" fill="none" stroke="#1C79B2" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>



        <div class="story-row  story-row--disable">

            <div class="story-col story-col--order-first  story-col--sm">

                <div class="story-col-txt  story-ani">

                    <h2 class="txt-dark txt-xl story-ani__i"><?=__('NEED TO EXPAND YOUR KNOWLEDGE ?','concept')?></h2>

                    <div class="story-ani__i"><strong><?=__('The NGL Academy transmits its knowledge through tailor-made training sessions.','concept')?></strong></div>

                </div>

            </div>

            <div class="story-col story-col--lg ">

                <div class="story-slides">

                    <img alt="" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/present/present-1.jpg" />

                    <img alt="" id="story-present-slides-1" class="story-slides__i" src="<?=$theme_url?>/assets-concept/images/present/present-2.jpg" />

                </div>

            </div>

        </div>



        <div class="story-path story-path--rc story-path--btm">



            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                 viewBox="0 0 418.2 140"  xml:space="preserve">

<path fill="none"  stroke-width="10" stroke="#f7f7f7" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

                <path id="scene-5-path-2" fill="none" stroke="#1C79B2" stroke-width="10" class="story-path__l" d="M6,0.1c0.3,18.1,0.1,36.3,0.1,54.4c0,10.9,4.2,15.1,15.1,15.1c47.6,0,188,0,287.9,0l10,0c34,0,42.6,0,76.6,0

	c12.7,0,16.4,3.7,16.4,16.3c0,18-0.1,36,0.1,54"/>

</svg>

        </div>

    </section><!-- /.#NGL concept page enrich knowledge block -->



    <!-- NGL Concept page contact details block -->

    <section id="scene-6" class="ipro-block ipro-block--full ipro-block--goldenrod ipro-block--quote text-center js-anim-init" data-delay="0.0005" data-animation="fadeIn" data-theme="theme-default">

        <div class="story-sep">

            <i class="picto-circle picto-grey story-sep__icon"><img src="<?=$theme_url?>/assets-concept/images/picto/icon-tick.png" /></i>

        </div>

        <div class="ipro-block__row">

            <div class="ipro-container ipro-container--main">

                <!-- ipro-block__text--small -->

                <div class="ipro-block__text js-anim-init" data-delay="0.25" data-animation="fadeIn slideInDown">

                    <h2><?=__('Contact Us','concept')?></h2>

                    <div class="ipro-box--s270 gap-top35">

                        <ul class="ipro-iconList__inline ipro-iconList__group ipro-iconList__group--lg">

                            <li class="ipro-iconList__item">

                                <a href="javascript:;" class="phone_number" data-tel-num="+41 22 365 46 66">
                                    <i class="icon icon-lg icon-contact-us"></i>
                                    <span class="i-label phone_number_display"><?=__('Phone','ngl');?></span>
                                </a>

                            </li>

                            <li class="ipro-iconList__item">

                                <a href="<?php echo home_url()?>/contact">

                                    <i class="icon icon-lg icon-envolpe"></i>

                                    <span class="i-label"><?=__('Email','concept');?></span>

                                </a>

                            </li>

                        </ul>

                    </div>

                </div>



            </div><!-- /.#NGL CMS block container -->

        </div><!-- /.#NGL CMS block row -->

    </section><!-- /.#NGL Concept page contact details block -->



    <!-- /.#Concept page building blocks end -->



    <link rel="stylesheet" href="<?=$theme_url?>/assets-concept/css/style.css" type="text/css">

    <script type="text/javascript" src="<?=$theme_url?>/assets-concept/js/TweenMax.min.js"></script>

    <script type="text/javascript" src="<?=$theme_url?>/assets-concept/js/ScrollMagic.min.js"></script>

    <script src="<?=$theme_url?>/assets-concept/js/main.js"></script>

    <script type="text/javascript">
    //tel:+41 22 365 46 66
        $(function(){


            telMobLink($('.phone_number')); 
            function telMobLink($item){

                     if($item.length === 0) return;
                        $item.each(function() {
                            var $self = $(this),
                                dataTelNum = $self.attr('data-tel-num'),
                                isTel = (typeof $self.attr('data-tel-num') !== 'undefined') ? true : false;         

                            if(typeof(dataTelNum) != undefined && !$self.hasClass('full-number-shown')) {               
                                $self.on('click', function(e) {
                                    //phoneNumString = phoneNumHolderText.split('/');
                                    $self.addClass('full-number-shown');
                                    $self.attr('href', 'tel:' + dataTelNum).find('.phone_number_display').text(dataTelNum);
                                    // e.preventDefault();
                                });
                            }
                        });
            }
        });
    </script>

<?php get_footer();?>