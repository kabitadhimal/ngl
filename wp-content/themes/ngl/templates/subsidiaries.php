<?php /*Template Name: Subsidiaries Template */?>
<?php get_header();


?>
 <?php while(have_posts()): the_post(); 

 $subsidiaries = get_field('subsidiaries');
	//debug($subsidiaries);
 ?>
<!-- NGL news details title banner -->
<section class="ipro-banner ipro-block--loggedIn ipro-banner--account">
    <div class="ipro-container ipro-container--main">

        <!-- NGL news details banner title -->
        <div class="ipro-banner__title text-center">
            <h2><?php the_title();?></h2>
        </div><!-- /.#NGL news details banner title block -->

    </div><!-- /.# NGL main container -->
</section><!-- /.#NGL news details title banner -->

<!-- NGL CMS blocks :: Home page map section -->
<section class="ipro-block ipro-block--map ipro-block--map-scroller ipro-block--full js-anim-init text-center" data-theme="theme-palma" data-delay="0.15" data-animation="fadein" style="background: url(<?php echo TEMP_DIR_URI ?>/images/map-bkg-patter.png);">
    <div class="ipro-block__row">

        <!-- NGL homepage map row -->
        <div class="ipro-block--map__row">

            <figure>
                <img src="<?php echo TEMP_DIR_URI ?>/images/map-bkg.png" class="ipro-valign--middle map-image" alt="" />
            </figure>

            <!-- Map marker group -->
            <div id="ipro-block--map__marker" class="ipro-block--map__marker tooltips-elem" data-tooltips="true">

                <a href="" class="icon icon-map-pin map__marker map__marker--geneva map__marker--large map__marker--goldenrod js-anim-init" data-scroll-target="ipro-block--headerquarter" data-delay="0.005" data-animation="fadeIn" style="top:<?php echo $map['x_co-ordinates']?>%;left:<?php echo $map['y_co-ordinates']?>%" data-tooltips-parent="tooltips-elem" data-caption="<?= $title = get_field('headquater_title'); ?>">
                </a>
                <?php if(!empty($subsidiaries)):
                			foreach ($subsidiaries as $key => $subsidiary):                                    			
                			
                ?>
                <a href="javascript:;" class="icon icon-map-pin map__marker map__marker--subsidiary_<?php echo $key?> map__marker--small map__marker--palma js-anim-init" data-scroll-target="ipro-block--subsidiary_<?php echo $key?>" data-delay="0.040" data-animation="fadeIn" style="top:<?php echo $subsidiary['x-co-ordinate']?>%;left:<?php echo $subsidiary['y_co-ordinates']?>%" data-tooltips-parent="tooltips-elem" data-caption="<?= $subsidiary['subsidiary_name']; ?>"></a>
            <?php 
            				endforeach;
           				 endif;?>


               <!--  <a href="" class="icon icon-map-pin map__marker map__marker--allemagna map__marker--small map__marker--palma js-anim-init" data-scroll-target="ipro-block--sanghai" data-delay="0.70" data-animation="fadeIn"></a>
                <a href="" class="icon icon-map-pin map__marker map__marker--denmark map__marker--small map__marker--palma js-anim-init" data-scroll-target="ipro-block--france" data-delay="0.70" data-animation="fadeIn"></a>
                <a href="" class="icon icon-map-pin map__marker map__marker--china map__marker--small map__marker--palma js-anim-init" data-scroll-target="ipro-block--pacific" data-delay="0.70" data-animation="fadeIn"></a>
                <a href="" class="icon icon-map-pin map__marker map__marker--singapore map__marker--small map__marker--palma js-anim-init" data-scroll-target="ipro-block--cleaningGMBH" data-delay="0.70" data-animation="fadeIn"></a>

                <a href="" class="icon icon-map-pin map__marker map__marker--usa map__marker--small map__marker--palma js-anim-init" data-scroll-target="ipro-block--usa" data-delay="0.75" data-animation="fadeIn"></a> -->
            </div><!-- Map marker group -->
        </div><!-- /.#NGL homepage map row -->

    </div><!-- /.#NGL block row -->
</section><!-- /.#NGL CMS blocks :: Home page map section -->

<!-- NGL Subsidiaries page contact details block -->
<section class="ipro-block ipro-block--full ipro-block--goldenrod ipro-block--quote ipro-block--headerquarter text-center js-anim-init" data-delay="0.0005" data-animation="fadeIn" data-theme="theme-default">
    <div class="ipro-block__row">
        <div class="ipro-container ipro-container--main">
			<!-- ipro-block__text--small -->
            <div class="ipro-block__text js-anim-init" data-delay="0.25" data-animation="fadeIn slideInDown">
                <h2><?php echo $title = get_field('headquater_title'); ?></h2>
                <address>
                <?php $headquater = get_field('headquater_address');?>
                    <?php echo $headquater?>
                </address>
                <?php $headquater_telephone = get_field('headquater_telephone'); ?>
                <span class="ipro__contact ipro__contact--tel"><strong><a href="tel:<?php echo $headquater_telephone?>">Tel. <?php echo $headquater_telephone?></a></strong></span>
                <span class="ipro__contact ipro__contact--mail ipro__gap--top40">
                <?php //$headquater_email = get_field('headquater_email'); ?>
                <a href="<?php echo home_url().'/contact'?>" class="ipro-link ipro-link--underline ipro-link--default"><em><?php echo __('Contact us','ngl')?></em></a>
                   <!--  <a href="mailto:<?php echo $headquater_email?>" class="ipro-link ipro-link--underline ipro-link--default"><em><?php echo $headquater_email?></em></a> -->
                    <!-- <a href="http://www.ngl-group.com" class="ipro-link ipro-link--underline ipro-link--default"><em>www.ngl-group.com</em></a> -->
                </span>
            </div>

        </div><!-- /.#NGL CMS block container -->
    </div><!-- /.#NGL CMS block row -->
</section><!-- /.#NGL Subsidiaries page contact details block -->

<!-- NGL subsidiaries page location block section -->
<section class="ipro-block ipro-block--twoHalf ipro-block--subsidiaries ipro-block--full js-anim-init" data-theme="theme-default" data-background="false" data-animation="fadein">
    <div class="ipro-block__row">

        <div class="clearfix ipro-row ipro-flex ipro-flex--wrap">
			 <?php if(!empty($subsidiaries)):
                			foreach ($subsidiaries as $key => $subsidiary):
                			
                			
                ?>
            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large text-center ipro-block--subsidiary_<?php echo $key?>" data-grayscale="true">
                <!-- Gray scale image -->
                <figure class="ipro__grayscale-holder js-anim-init" data-delay="0.05" data-animation="fadeIn">
                    <img src="<?php echo $subsidiary['subsidiary_background_image']['url']?>" class="ipro-media--grayscale ipro-valign--middle" alt="<?php echo $subsidiary['subsidiary_background_image']['alt']?>" />
                </figure><!-- /.#Gray scale image -->

                <!-- Col caption -->
                <div class="ipro-col__content ipro-col-subsidiaries__caption ipro-container--text-small js-anim-init" data-delay="0.025" data-animation="fadein slideInDown">
                    <h2><?php echo $subsidiary['subsidiary_name']?></h2>
                    <h5><?php echo $subsidiary['subsidiary_information']?></h5>
                    <address>
                        <span class="ipro__contact ipro__contact--tel"><a href="tel:<?php echo $subsidiary['phone_number']?>"><?php echo __('Tel.','ngl')?><?php echo $subsidiary['phone_number']?></a></span>
                         <?php if(!empty($subsidiary['email'])):?>
                        <span class="ipro__contact ipro__contact--mail"><a href="mailto:<?php echo $subsidiary['email'];?>"><?php echo $subsidiary['email']?></a></span>
                    <?php endif; ?>
                    </address>
                </div><!-- /.#Col caption -->
            </div>

        <?php endforeach;
       	 endif;
        ?>

      
          


          

        </div><!-- /.# NGL tow half grid wrap -->

    </div><!-- /.# NGL container -->
</section>

   <?php endwhile; ?>
<?php get_footer();?>