(function () {
    tinymce.create('tinymce.plugins.wp_procab', {
        init: function (ed, url) {

            ed.addButton('br', {
                title: 'Line Break',
                cmd: 'br',
                text: 'Line Break'
            });

             ed.addButton('link-readmore', {
                title: 'Readmore Link',
                cmd: 'link-readmore',
                text: 'Readmore Link'
            });
             ed.addButton('btn-readmore', {
                title: 'Readmore Button',
                cmd: 'btn-readmore',
                text: 'Readmore Button'
            });
             ed.addButton('audio-player', {
                title: 'Audio',
                cmd: 'audio-player',
                text: 'Audio'
            });
             ed.addButton('team', {
                title: 'Team',
                cmd: 'team',
                text: 'Team'
            });
             ed.addButton('download-pdf', {
                title: 'Download PDF',
                cmd: 'download-pdf',
                text: 'Download PDF'
            });
           
          

            ed.addCommand('br', function () {

                ed.execCommand('mceInsertContent', 0, '[br]');
            });

             ed.addCommand('link-readmore', function () {

                ed.execCommand('mceInsertContent', 0, '[a url="--url--"]--Anchor Text--[/a]');
            });

               ed.addCommand('btn-readmore', function () {

                ed.execCommand('mceInsertContent', 0, '[button url="--button url--"]--Button Text--[/button]');
            });            
            ed.addCommand('audio-player', function () {

                ed.execCommand('mceInsertContent', 0, '[audio-player url="--Audio url--"][/audio-player]');
            }); 

             ed.addCommand('team', function () {

                ed.execCommand('mceInsertContent', 0, '[team size="--small/medium/large--"]--Title--[/team]');
            }); 
             ed.addCommand('download-pdf', function () {

                ed.execCommand('mceInsertContent', 0, '[download-pdf]--PDF URL--[/download-pdf]');
            }); 

            
             
           

        },
        createControl: function (n, cm) {
            return null;
        },
        getInfo: function () {
            return {
                longname: 'Wp Procab Buttons'
            };
        }
    });

    tinymce.PluginManager.add('wp_procab', tinymce.plugins.wp_procab);
})();