<?php


function ngl_br() {
    return '<br />';
}


function ngl_button($args, $content = ''){
    extract(shortcode_atts(array(
        'url' => "#"     
        ), $args));
     

    $tar = 'target="_blank"';

  
    $html='<a href="'.$url.'" class="btn btn--goldenrod btn-gapTop--medium btn--medium" ' . $tar . '>'.do_shortcode($content).'</a>';
    return $html;
}

 
function ngl_anchor($args, $content = '') {
    extract(shortcode_atts(array(
        'url' => "#"       
        ), $args));

    $tar = 'target="_blank"';
    $html='<a href="'.$url.'" class="ipro-link ipro-link--underline ipro-link__gap--large ipro-link--darkGoldenrod"' . $tar . '>'.do_shortcode($content).'</a>';
    return $html;
}

function ngl_team($args,$content = '') {
   
    extract(shortcode_atts(array(
        'size' => "large"       
        ), $args));

      $img='';
      if($size=='small'){
         $img = 'img03';
      }elseif($size=='medium'){
        $img = 'img02';
      }else{
        $img = 'img01';
      }
      $html= '<div class="ipro-flex__col--team"><div class="ipro-col__content">
                                        <figure>
                                            <img src="'.TEMP_DIR_URI.'/images/team-'. $img .'.png" class="ipro-valign--middle" alt="" />
                                        </figure>
                                        <figcaption><span>'.$content.'</span></figcaption>
                                    </div></div>';
   
    return $html;
}

function ngl_pdf($args,$content = '') {
 
  $html= '<a href="'.$content.'" class="ipro__picto-link" target="_blank" ><i class="ipro__picto ipro__picto--pdfBlack"></i><span>Download PDF</span></a>';
  return $html;
}

function ngl_audio($args, $content = ''){

   extract(shortcode_atts(array(
        'url' => ""       
        ), $args));
  $html .='<div class="ipro-audio__wrap js-audio-wrap">
                                            <audio controls="controls" preload="auto" class="ipro-audio__elem js-audio">
                                                <source src="'.$url.'" type="audio/mpeg" />
                                            </audio>
                                        </div>';

  return $html;
}




function register_shortcodes() {
   
    add_shortcode('br', 'ngl_br');
    add_shortcode('button', 'ngl_button');
    add_shortcode('a', 'ngl_anchor');
    add_shortcode('audio-player', 'ngl_audio');
    add_shortcode('team', 'ngl_team');
    add_shortcode('download-pdf', 'ngl_pdf');
    
}

add_action('init', 'register_shortcodes');
?>