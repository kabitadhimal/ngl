 <ul class="ipro-widgets ipro-widgets--sidebarNav ipro__accordion--mobile-panel">
    <li class="ipro-widgets__item"><a href="<?php echo home_url('news-events')?>"><?php echo __('Private News','ngl')?></a></li>
    <!-- <li class="ipro-widgets__item"><a href="#"><?php echo __('Documents','ngl')?></a>
			<ul>
				<li><a href="<?php echo home_url('news-events?type=new-product')?>"><?php echo __('New products','ngl')?></a></li>
				<li><a href="<?php echo home_url('news-events?type=events')?>"><?php echo __('Events','ngl')?></a></li>
			</ul>
    </li> -->
    <li class="ipro-widgets__item current"><a href="<?php echo home_url('my-account')?>"><?php echo __('My Information','ngl')?></a></li>
    <li class="ipro-widgets__item"><a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo __('Logout','ngl')?></a></li>
    
 </ul>