<?php

use App\Psr4AutoloaderClass;

if(!class_exists('App\Psr4AutoloaderClass')) {
    require __DIR__ . '/src/autoloader.php';
    Psr4AutoloaderClass::getInstance()->addNamespace('Procab', __DIR__.'/src/Procab');
    Psr4AutoloaderClass::getInstance()->addNamespace('App', __DIR__.'/src/App');
}

$include_files = [
    'src/helpers.php'
];

array_walk($include_files, function($file){
    if(!locate_template($file, true, true)){
        trigger_error(sprintf('Couldnot find %s', $file), E_USER_ERROR);

    }
});
unset($include_files);

//
define('TEMP_DIR_URI', get_template_directory_uri());
define('TEMP_DIR', get_template_directory());
define('SITEURL', site_url());
define('HOMEURL', home_url());
/*Email footer link*/
define('LINKEDIN_URL', 'https://www.linkedin.com/');
define('CONTACT_URL', HOMEURL.'/contact/');
//define('NEWSLETTER_URL', HOMEURL.'/contact/');
define('DEMAND_URL', HOMEURL.'/contact/');
define('CURRENT_YEAR', date('Y'));
define('ADMIN_URL', admin_url());


/*END Email footer link*/

define('PENDING','0');
define('APPROVED','1');

//include TEMP_DIR_URI.'/functions/function-helpers.php';
require TEMP_DIR . '/functions/function-helpers.php';
require TEMP_DIR . '/functions/function-account.php';

//require TEMP_DIR . '/functions/function-map-helpers.php';

//include TEMP_DIR . '/functions/social-helpers.php';

//AJAX 

require TEMP_DIR . '/ajax/create-update-account.php';
require TEMP_DIR . '/ajax/create-update-account-new.php';
require TEMP_DIR . '/ajax/forget-password.php';
require TEMP_DIR . '/ajax/reset-password.php';
require TEMP_DIR . '/ajax/user-login.php';





/* ngl shortcodes */
require TEMP_DIR . '/shortcodes/shortcodes.php';
require TEMP_DIR . '/shortcodes/shortcode-functions.php';




/**
 * ngl functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ngl
 */
if (!function_exists('debug')) {


    function debug($var = '', $die = true) {
        $array = debug_backtrace();
        echo '<br/>Debugging from ' . $array[0]['file'] . ' line: ' . $array[0]['line'];

        echo '<pre>';
        print_r($var);
        echo '</pre>';

        if ($die !== FALSE) {
            die();
        }
    }

}




if (!function_exists('ngl_setup')) :

    function ngl_setup() {

        load_theme_textdomain('ngl', get_template_directory() . '/languages');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');

        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'ngl'),
            'footer_menu' =>'Footer Menu'
        ));
        

        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'author',
            'gallery',
            'status',
            'audio',
        ));
    }

endif;
add_action('after_setup_theme', 'ngl_setup');

function ngl_content_width() {
    $GLOBALS['content_width'] = apply_filters('ngl_content_width', 640);
}

add_action('after_setup_theme', 'ngl_content_width', 0);

function ngl_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'ngl'),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'ngl_widgets_init');



/* ================================================================================
  ENQUEUE SCRIPTS AND STYLES.
  ================================================================================== */

function ngl_scripts() {


     
    //wp_enqueue_style('ngl-style', get_stylesheet_uri());
    wp_enqueue_style('main-style', TEMP_DIR_URI . '/css/style.css','','2.3');

    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', TEMP_DIR_URI . "/js/jquery-2.2.3.min.js", false, null);
        wp_enqueue_script('jquery');
    }


    wp_enqueue_script('imagesloaded-jquery', TEMP_DIR_URI . '/js/imagesloaded.pkgd.min.js', array('jquery'), '1.0');
    wp_enqueue_script('easing-jquery', TEMP_DIR_URI . '/js/jquery.easing.1.3.js', array('jquery'), '1.0');
    wp_enqueue_script('waypoints-jquery', TEMP_DIR_URI . '/js/jquery.waypoints.js', array('jquery'), '1.0');

    if (is_page_template( 'templates/news.php' )):

     wp_enqueue_script('imagesloaded', TEMP_DIR_URI . '/js/imagesloaded.pkgd.min.js', array('jquery'), '1.0',true);
     wp_enqueue_script('isotope', TEMP_DIR_URI . '/js/isotope.pkgd.min.js', array('jquery'), '1.0',true);
    endif;

    
    wp_enqueue_script('ihead-jquery', TEMP_DIR_URI . '/js/ihead-script.min.js', array('jquery'), '1.0');



       
      wp_enqueue_script('bootstrap', TEMP_DIR_URI . '/js/bootstrap.min.js', array('jquery'), '1.0',true);
      wp_enqueue_script('respond', TEMP_DIR_URI . '/js/respond.js', array('jquery'), '1.0',true);
      wp_enqueue_script('swiper', TEMP_DIR_URI . '/js/swiper.min.js', array('jquery'), '1.0',true);
      wp_enqueue_script('niceSelect', TEMP_DIR_URI . '/js/jquery.niceSelect.js', array('jquery'), '1.0',true);
      wp_enqueue_script('nicescroll', TEMP_DIR_URI . '/js/jquery.nicescroll.js', array('jquery'), '1.0',true);
      wp_enqueue_script('stellar', TEMP_DIR_URI . '/js/jquery.stellar.min.js', array('jquery'), '1.0',true);
      wp_enqueue_script('lightgallery', TEMP_DIR_URI . '/js/lightgallery-all.min.js', array('jquery'), '1.0',true);


      wp_enqueue_script('smoothscroll', TEMP_DIR_URI . '/js/smoothscroll.min.js', array('jquery'), '1.0',true);
    
      wp_enqueue_script('audio-video', TEMP_DIR_URI . '/js/audio-video.min.js', array('jquery'), '1.0',true);

      wp_enqueue_script('jquery-validate', TEMP_DIR_URI . '/js/jquery.validate.js', array('jquery'), '1.0',true);
      wp_enqueue_script('validate-wrapper', TEMP_DIR_URI . '/js/validation/jquery.validate.wrapper.js', array('jquery'), '1.0',true);

      if(ICL_LANGUAGE_CODE=='fr'){
         wp_enqueue_script('fr-validation-message-wrapper', TEMP_DIR_URI . '/js/validation/localization/messages_fr.js', array('validate-wrapper'), '1.0',true);
      }elseif(ICL_LANGUAGE_CODE=='de'){
         wp_enqueue_script('de-validation-message-wrapper', TEMP_DIR_URI . '/js/validation/localization/messages_de.js', array('validate-wrapper'), '1.0',true);
      }elseif(ICL_LANGUAGE_CODE=='it'){
         wp_enqueue_script('it-validation-message-wrapper', TEMP_DIR_URI . '/js/validation/localization/messages_it.js', array('validate-wrapper'), '1.0',true);
      }elseif(ICL_LANGUAGE_CODE=='es'){
         wp_enqueue_script('es-validation-message-wrapper', TEMP_DIR_URI . '/js/validation/localization/messages_es.js', array('validate-wrapper'), '1.0',true);
      }elseif(ICL_LANGUAGE_CODE=='zh-hans'){
         wp_enqueue_script('zh-validation-message-wrapper', TEMP_DIR_URI . '/js/validation/localization/messages_zh.js', array('validate-wrapper'), '1.0',true);
      }    



     // if (is_page_template( 'templates/news.php')):
    wp_enqueue_script('select2', TEMP_DIR_URI . '/js/select2.full.min.js', array('jquery'), '1.0',true);
     //  endif;
    wp_enqueue_script('jquery-kwicks', TEMP_DIR_URI . '/js/jquery.kwicks.min.js', array('jquery'), '1.0',true);
    wp_enqueue_script('all-jquery', TEMP_DIR_URI . '/js/all.min.js', array('jquery'), '1.0',true);
    //wp_enqueue_script('jquery-scroll-reveal', TEMP_DIR_URI . '/js/jquery.scroll.reveal.js', array('jquery'), '1.0', true);
    //wp_enqueue_script('jquery-scroller-reveal', TEMP_DIR_URI . '/js/skrollr.min.js', array('jquery'), '1.0', true);

     // wp_enqueue_script('iscript', TEMP_DIR_URI . '/js/iscript.js', array('jquery'), '1.0',true);
     // wp_enqueue_script('sticky-header', TEMP_DIR_URI . '/js/sticky-header.js', array('jquery'), '1.0',true);
      //wp_enqueue_script('mobile-menu', TEMP_DIR_URI . '/js/mobile-menu.js', array('jquery'), '1.0',true);
      //wp_enqueue_script('mega-menu', TEMP_DIR_URI . '/js/mega-menu.js', array('jquery'), '1.0',true);
     // wp_enqueue_script('main-slider', TEMP_DIR_URI . '/js/main-slider.js', array('jquery'), '1.0',true);
      


			/*Include only in home page */
       if (is_front_page()):
           // wp_enqueue_script('kwirks-effects', TEMP_DIR_URI . '/js/kwirks-effects.js', array('jquery'), '1.0',true);
           // wp_enqueue_script('map-scripts', TEMP_DIR_URI . '/js/map-scripts.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('ribbon-fix', TEMP_DIR_URI . '/js/ribbon-fix.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('dom-animations', TEMP_DIR_URI . '/js/dom-animations.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('cms-stuffs', TEMP_DIR_URI . '/js/cms-stuffs.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('hover-elements', TEMP_DIR_URI . '/js/hover-elements.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('responsive-table', TEMP_DIR_URI . '/js/responsive-table.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('videoplayer', TEMP_DIR_URI . '/js/yv-videoplayer.js', array('jquery'), '1.0',true);
            //wp_enqueue_script('plugin-extends', TEMP_DIR_URI . '/js/plugin-extends.js', array('jquery'), '1.0',true);
       	endif;


    

    if (is_page()):
       // wp_enqueue_script('cookie-jquery', TEMP_DIR_URI . '/js/jquery.cookie.js', array('jquery'), '1.0');
    endif;
}



add_action('wp_enqueue_scripts', 'ngl_scripts');


remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop', 12);



/* ================================================================================
  REMOVE ILLEGAL CHARACTERS FROM FILES DURING UPLOADING ON WORDPRESS
  ================================================================================== */
add_filter('wp_handle_upload_prefilter', 'custom_upload_filter', 1, 1);

function custom_upload_filter($file) {
    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $file['name']);
    return $file;
}

/* ================================================================================
  CREATE THE OPTIONS PAGE
  ================================================================================== */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Footer Options',
        'menu_title' => 'Footer Options'
    ));
}

// if (function_exists('acf_add_options_page')) {
//     acf_add_options_page(array(
//         'page_title' => 'Options',
//         'menu_title' => 'Options'
//     ));
// }







/* =========================================================
  Add custome taxonomy
  ======================================================== */

 //add_taxonomy('news_category', array('publication'), array('label' => 'Publication Categories', 'hierarchical' => true));



/* =========================================================
  Add custom post type
  ======================================================== */

add_post_type('product', array(
    'public' => true,
    'menu_icon' => 'dashicons-calendar',
    'label' => "All Products",
    'labels' => array(
        'add_new_item' => "Add new product"
    ),
    'supports' => array(
        'title'
       
    )
));







/* ================================================================================
  Function which remove Plugin Update Notices – nav-menu-images
  ================================================================================== */



add_filter('site_transient_update_plugins', 'disable_plugin_updates');

function disable_plugin_updates($value) {
    //debug($value);
    if (!empty($value))
        unset($value->response['nav-menu-images/nav-menu-images.php']);
    return $value;
}

function getLangFromQuery() {

    $langs = array('en', 'fr', 'de');

    $lang = 'fr';

    //get lang code

    if (isset($_GET['lang'])) {

        $lang = strtolower($_GET['lang']);

        if (!in_array($lang, $langs))
            $lang = 'fr';
    }

    return $lang;
}



/* ================================================================================
  Function to remove pingback header.
  ================================================================================== */


add_filter('wp_headers', 'remove_x_pingback_header');

function remove_x_pingback_header($headers) {
    unset($headers['X-Pingback']);
    return $headers;
}

add_filter('xmlrpc_enabled', '__return_false');

//
/* ================================================================================
  Removes the meta tag generator name and version that generated with the wp_head()function.
  ================================================================================== */
remove_action('wp_head', 'wp_generator');

function completely_remove_wp_version() {
    return ''; //returns nothing, exactly the point.
}

add_filter('the_generator', 'completely_remove_wp_version');

function remove_generator_from_rss2($gen, $type) {
    return '';
}

add_filter("get_the_generator_rss2", "remove_generator_from_rss2", 10, 2);

function get_month_by_language($code, $month) {
    $lan_arr = array(
        'fr' => array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'),
        'en' => array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
        'de' => array('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember')
    );
    return $lan_arr[$code][$month - 1];
}

function sort_terms() {
    
}

/*Map Co-ordinates Management from backend*/

require get_template_directory() . '/functions/function-map-helpers.php';


function wp_get_menu_array($current_menu) {
 
    $array_menu = wp_get_nav_menu_items($current_menu);
  //  debug($array_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {

            $image_url = wp_get_attachment_url(get_post_thumbnail_id($m->ID));   
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID']      =   $m->ID;
            $menu[$m->ID]['title']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['classes']  = $m->classes;
            $menu[$m->ID]['description']  = $m->post_content;

            $menu[$m->ID]['image']  = $image_url;

            $menu[$m->ID]['children']    =   array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {

        if ($m->menu_item_parent) {
            $image_url = wp_get_attachment_url(get_post_thumbnail_id($m->ID)); 
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['title']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $submenu[$m->ID]['classes']  = $m->classes;
            $submenu[$m->ID]['image']  = $image_url;
            $submenu[$m->ID]['description']  = $m->post_content;

            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
     
}

add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
     $classes[] = 'ipro-template';
    
    if ( is_page_template( 'templates/news.php' ) ) {
        $classes[] = 'ipro-template--newsEvent';
    }

    if ( is_page_template( 'templates/home-page.php' ) ) {
        $classes[] = 'ipro-template--home';
    }

    if ( is_page_template( 'templates/create_account.php' ) ) {
        $classes[] = 'ipro-template--createAccount';
    }

    if ( is_page_template( 'templates/forget_password.php' ) ) {
        $classes[] = 'ipro-template--changePassword';
    }

    if ( is_page_template( 'templates/my-account.php' ) ) {
        $classes[] = 'ipro-template--myAccount';
    }

    if ( is_page_template( 'templates/product.php' ) ) {
        $classes[] = 'ipro-template--productGuide';
    }

     if ( is_page_template( 'templates/subsidiaries.php' ) ) {
        $classes[] = 'ipro-template--subsidiaries';
    }

     if ( is_page_template( 'templates/contact-page.php' ) ) {
        $classes[] = 'ipro-template--contactus';
    }


     if (is_front_page()){
         $classes[] = 'pro-template--home';
     }

     if(is_single()){
        $classes[] = 'ipro-template--newsDetails';
     }

     if(is_page()){
        $classes[] = 'ipro-template--cms parallax-running stellar-running';
     }

    
    return $classes;
}

//add_action('init','hide_panel');

function hide_panel(){
   
    if ( ! current_user_can( 'manage_options' ) ) {
        show_admin_bar( false );
    }
}

function langcode_post_id($post_id){
    global $wpdb;
 
    $query = $wpdb->prepare('SELECT language_code FROM ' . $wpdb->prefix . 'icl_translations WHERE element_id="%d"', $post_id);
    $query_exec = $wpdb->get_row($query);
 
    return $query_exec->language_code;
}






//echo syncMailchimp();die();

function subscribedToMailchimp($data) {
       

$email=$data['email'];
$fname=$data['fname']; 
$lname=$data['lname']; 



$apikey = '6e406561ade2548e950dd3ae995ac922-us14';
$listId = 'bf40baa7ca';
$auth = base64_encode( 'user:'.$apikey );

$data = array(
    'apikey'        => $apikey,
    'email_address' => $email,
    'status'        => 'subscribed',
    'merge_fields'  => array(
        'FNAME' => $fname,
        'LNAME' => $lname
    )
);
$json_data = json_encode($data);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://us14.api.mailchimp.com/3.0/lists/'. $listId.'/members/');
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                            'Authorization: Basic '.$auth));
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                                                  

$result = curl_exec($ch);

    return $result;
}

//add_action('init','test1');

function test(){
 $parse_array = array(       
        '{SITE_URL}' =>SITEURL,
        '{HOMEURL}' => HOMEURL,
          '{TEMP_DIR_URI}' => TEMP_DIR_URI,
        '{CONTACT_US_LINK}' => CONTACT_URL,
        '{LINKEDIN_LINK}' => LINKEDIN_URL,
        '{NEWSLETTER_URL}' => NEWSLETTER_URL,
        '{DEMAND_URL}' => DEMAND_URL,
        '{CURRENT_YEAR}' => CURRENT_YEAR,
        '{FROM_USERNAME}' => 'ADMIN',
        '{FROM_EMAIL}' => get_option('admin_email'),
        '{MEMBER_NAME}' => ucfirst($user->display_name),
        '{MEMBER_EMAIL}' => $user->user_email    
       
    );

  $email_template = build_email_template($parse_array, 'account_approved','fr');
  echo $email_template;exit();
}




function wpcf7_before_send_mail_attachReceiver ($wpcf7) {

   $company_name = sanitize_text_field($_POST["TextBox_73676"]);
  $contact_person = sanitize_text_field($_POST["TextBox_73677"]);
  $firstname = sanitize_text_field($_POST["TextBox_73678"]); 
  $country = sanitize_email($_POST["TextBox_73679"]);
  $email = sanitize_email($_POST["TextBox_73680"]);
  $country = sanitize_text_field($_POST["TextBox_73679"]);
  $job_title = sanitize_text_field($_POST["TextBox_73682"]);
  $message = sanitize_email($_POST["TextArea_73681"]);
  $address1 = sanitize_text_field($_POST["TextArea_73683"]);
  $postal_code = sanitize_text_field($_POST["TextBox_73684"]);
  $city = sanitize_text_field($_POST["TextBox_73685"]);
  $telephone = sanitize_text_field($_POST["TextBox_73686"]);
          


   if (isset($_POST["Checkbox_76010"]) && $_POST["Checkbox_76010"]==1) {

            //insert into table newsletter 
            global $wpdb;
            $table=$wpdb->prefix.'newsletter';
           // $name = explode(' ', $fullname);
            //$fname = !empty($name[0]) ? $name[0] : '';
           // $lname = !empty($name[1]) ? $name[1] : '';

              $data = array(
                 'email' => $email,
                 'name' => $firstname,
                 // 'surname' => $lname,
                 'status' => 'C',
                 'http_referer' => site_url(),
             );

            $insert = $wpdb->insert( $table, $data); 
            
          
        }

  

       
        

  $properties = $wpcf7->get_properties();

  $wpcf7->set_properties($properties);

   $url ='https://secure.inescrm.com/InesWebFormHandler/Main.aspx';   
          //$url='http://posttestserver.com/post.php';   
        
           $response = wp_remote_post(
          $url,
          array(
              'body' => array(
                  'controlid'   => $_POST['controlid'],
                  'oid'   => $_POST['oid'],
                  'formid'   => $_POST['formid'],
                  'retURL'   => $_POST['retURL'],
                  'data'   => $_POST['data'],
                  'Alias'   => $_POST['Alias'],
                  'TextBox_73676'     => $company_name,
                  'TextBox_73677'     => $contact_person,
                  'TextBox_73678'     => $firstname,
                  'TextBox_73680'     => $email,
                  'TextBox_73679'     => $country,
                  'TextBox_73682'     => $job_title,
                  'TextArea_73681'     => $_POST["TextArea_73681"],
                  'TextArea_73683'     => $address1,
                  'TextBox_73684'     => $postal_code,
                  'TextBox_73685'     => $city,
                  'TextBox_73686'     => $telephone,
                  'Checkbox_76010'     => $_POST["Checkbox_76010"]
               
              )
          )
          );

}

add_action("wpcf7_before_send_mail", "wpcf7_before_send_mail_attachReceiver");


function get_respective_countryName($defaultCountry='en',$country='en'){


    $countryNames = array(
                    'en'=>array(
                            'en'=>'English',
                            'fr'=>'French',
                            'de'=>'German',
                            'zh-hans'=>'Chinese',
                            'es'=>'Spanish',
                            'it'=>'Italian'
                        ),
                    'fr'=>array(
                            'en'=>'Anglais',
                            'fr'=>'Français',
                            'de'=>'Allemand',
                            'zh-hans'=>'Chinois',
                            'es'=>'Espagnol',
                            'it'=>'Italien'
                        ),
                    'it'=>array(
                            'en'=>'Inglese',
                            'fr'=>'Francese',
                            'de'=>'Tedesco',
                            'zh-hans'=>'Cinese',
                            // 'es'=>'spagnolo',
                              'es'=>'Spanolo', 
                            // 'it'=>'Italiano'
                             'it'=>'Italiano'
                        ),
                    'es'=>array(
                            'en'=>'Inglés',
                            'fr'=>'Francés',
                            'de'=>'Alemán',
                            'zh-hans'=>'Chino',
                            'es'=>'Español',
                            'it'=>'Italiano'
                        ),
                    'zh-hans'=>array(
                            'en'=>'英語',
                            'fr'=>'法國',
                            'de'=>'德語',
                            'zh-hans'=>'中文',
                            'es'=>'西班牙語',
                            'it'=>'意大利'
                        ),
                    'de'=>array(
                            'en'=>'Englisch',
                            'fr'=>'Französisch',
                            'de'=>'Deutsche',
                            'zh-hans'=>'Chinesisch',
                            'es'=>'Spanisch',
                            'it'=>'Italienisch'
                        ),
                );
    return $countryNames[$defaultCountry][$country];
}

/*
 *
 */
/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function app_excerpt_more( $more ) {
    return '....';
}
add_filter( 'excerpt_more', 'app_excerpt_more' );

/*
 * Excerpt length
 */

function app_excerpt_length( $length ) {
    return 17;
}
add_filter( 'excerpt_length', 'app_excerpt_length', 999 );

/*
*  Ajax News Filter
*/
add_action( 'wp_ajax_display_news_contents', 'display_news_contents_callbck' );
add_action( 'wp_ajax_nopriv_display_news_contents', 'display_news_contents_callbck' );
function display_news_contents_callbck() {
    extract($_GET);
    include __DIR__.'/partial/filter-result/news-fliter-content.php';
    die();
}
/*
 *
 */

// define the wpcf7_form_action_url callback
/*function filter_wpcf7_form_action_url( $url ) {
    // make filter magic happen here...
    global $cf7;
    if($cf7->id() == 14828) {

        return 'https://extend.inescrm.com/InesWebFormHandler/Main.aspx';
    }
    return $url;
};

// add the filter
add_filter( 'wpcf7_form_action_url', 'filter_wpcf7_form_action_url', 10, 1 );*/
