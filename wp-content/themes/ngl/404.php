<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package iam
 */

get_header(); ?>

	<section class="error-404 not-found ipro-block--error text-center">
		<div class="ipro-container ipro-container--main">
			<header class="ipro-block__header">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'iam' ); ?></h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'iam' ); ?></p>
			</div><!-- .page-content -->
		</div>
	</section><!-- .error-404 -->

<?php
get_footer();
