<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package iam
 */

get_header(); ?>
<?php if (have_posts()) : 

  while (have_posts()) : the_post(); 
          $id = get_the_ID();
          ?>
<!-- NGL news details title banner -->
        <section class="ipro-banner ipro-banner--details">
            <div class="ipro-container ipro-container--main">
            
                <!-- Back to news details -->
                <div class="ipro-banner__link ipro-banner__link--newsDetails">
                 <a href="javascript:;" onclick="goBack()" class="ipro-link ipro-link--underline ipro-link--stomGrey"><em><?php echo _e('Return','ngl')?></em></a>    
                  
                </div><!-- /.#Back to news details block -->

                <!-- NGL news details banner title -->
                <div class="ipro-banner__title text-center">
                    <h2><?php echo the_title()?></h2>
                </div><!-- /.#NGL news details banner title block -->

            </div><!-- /.# NGL main container -->
        </section><!-- /.#NGL news details title banner -->

        <!-- NGL News details post content block -->
        <section class="ipro-block ipro-block--single ipro-block--post">
            <div class="ipro-container ipro-container--main">

                <div class="ipro-post ipro-post--single">
                    <div class="clearfix ipro-post__grid">
                        <!-- Post image -->
                        <figure class="ipro-post__thumbnail ipro-post__thumbnail--large">
                           <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($id));?>
                            <img src="<?php echo $image_url?>" class="ipro-post__img ipro-valign--middle" alt="Post detail image" />
                        </figure><!-- /.#Post image -->

                        <!-- Post body -->
                        <div class="ipro-post__body">
                            <p><?php the_content();?></p>
                        </div><!-- /.#Post body -->
                    </div>
                </div>

            </div><!-- /.# NGL main container -->
        </section><!-- /.#NGL News details post content block -->
    <?php 
            $events_slug_arr = array('events-zh-hans','events-de','events-it','events-es','events','events-2','events-zh-hans-2','events-de-2','events-it-2','events-es-2');
           
            $categories = get_the_category( $id );
            if($_GET['test']==1){
                //debug($categories );
            }
            
            if(!empty($categories)):
                 $category = $categories[0]->slug;
                 if(in_array($category,$events_slug_arr)):
                    
              
     ?>
        <!-- NGL News details post form block -->
                    <section class="ipro-block ipro-block--form">
                        <div class="ipro-container ipro-container--main">
                        
                            <!-- NGL post form -->
                             <div class="ipro-form-row text-center" <?php echo $category?>>
                           
                                <h3><?php echo __('GET IN CONTACT WITH YOUR TEAM','ngl')?></h3>
                          

                                <div class="ipro-form__wrap"> 
                                    <div class="ipro-form ipro-form--single" action="" method="post">


                                      <?php 
                                               if(ICL_LANGUAGE_CODE=='en'){
                                                     echo do_shortcode('[contact-form-7 id="4273" title="Event Post Form en"]');
                                                }elseif(ICL_LANGUAGE_CODE=='zh-hans'){
                                                     echo do_shortcode('[contact-form-7 id="6115" title="Event Post Form CN"]');
                                                }elseif(ICL_LANGUAGE_CODE=='de'){
                                                     echo do_shortcode('[contact-form-7 id="6112" title="Event Post Form de"]');
                                                }elseif(ICL_LANGUAGE_CODE=='es'){
                                                     echo do_shortcode('[contact-form-7 id="6113" title="Event Post Form es"]');
                                                }elseif(ICL_LANGUAGE_CODE=='it'){
                                                     echo do_shortcode('[contact-form-7 id="6114" title="Event Post Form it"]');
                                                }else{
                                                      echo do_shortcode('[contact-form-7 id="254" title="Event Post Form"]');
                                                
                                                }
                                  ?>
                                       
                                    </div>

                                </div>
                            </div> 
                            <!-- NGL post form -->
                            
                        </div><!-- /.# NGL main container -->
                    </section>
                <?php endif;
                endif;
                ?>
<?php endwhile;
endif;?>


<script type="text/javascript">
    function goBack() {
        window.history.back();
    }
</script>
<?php

get_footer();
