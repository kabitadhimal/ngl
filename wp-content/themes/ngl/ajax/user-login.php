<?php 
add_action('wp_ajax_nopriv_user_login', 'user_login');
add_action('wp_ajax_user_login', 'user_login');

function user_login(){
	$post = array();
    parse_str($_POST['data'], $post);
    
    $email = sanitize_email($post["email"]);
    $password = sanitize_text_field($post["password"]);

    $user_details = get_user_by('email', $email);

    if(!empty($user_details)){
    	$user_id = $user_details->ID;    	
    	$status = get_user_meta( $user_id , 'status', true );
    	if(!$status){
    			 echo json_encode(array('status' => 'error', 'msg' =>'Your account is in pending status. Please contact to Administrator'));exit();
    	}

        $creds = array();
        $encrypt_password = wp_set_password( $password, $user_id );

        $creds['user_login'] = $email;
        $creds['user_password'] = $password;
        
        $user = wp_signon( $creds, false );

        if ( is_wp_error($user) ){
            
             echo json_encode(array('status' => 'error', 'msg' =>'Invalid Login'));exit();
        }else{
             echo json_encode(array('status' => 'success'));
             exit();
        }

    }else{
         echo json_encode(array('status' => 'error', 'msg' =>'Sorry! This email isnot in our system'));exit();
    }

   
    //debug($_POST['data']);
	

}

?>