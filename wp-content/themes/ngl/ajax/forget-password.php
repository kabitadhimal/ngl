<?php 
add_action('wp_ajax_nopriv_forget_password', 'forget_password');
add_action('wp_ajax_forget_password', 'forget_password');

function forget_password(){
	$post = array();
    parse_str($_POST['data'], $post);
    
    $email = sanitize_email($post["email"]);
    $user_id = email_exists($email);

    if ($user_id) {
    	$user = get_userdata($user_id);
           $guid = get_user_meta($user_id, 'guid');
            $language = get_user_meta($user_id, 'language',true);
             if (!empty($guid)) {
          
                $subject = 'Reset Password';        
                
               


                    $parse_array = array(       
                            '{SITE_URL}' =>SITEURL,
                            '{HOMEURL}' => HOMEURL,
                            '{TEMP_DIR_URI}' => TEMP_DIR_URI,
                            '{CONTACT_US_LINK}' => CONTACT_URL,
                            '{LINKEDIN_LINK}' => LINKEDIN_URL,
                            '{NEWSLETTER_URL}' => NEWSLETTER_URL,
                            '{DEMAND_URL}' => DEMAND_URL,
                            '{CURRENT_YEAR}' => CURRENT_YEAR,                
                            '{FROM_EMAIL}' => get_option('admin_email'),
                            '{MEMBER_NAME}' => ucfirst($user->display_name),
                            '{MEMBER_EMAIL}' => $user->user_email,
                            '{LINK}' => home_url() .'/'.$language. '/reset-password?guid=' . $guid[0] 
                           
                        );
                       
                if($language=='fr'){
                     $email_template = build_email_template($parse_array, 'reset_password','fr');
                }else{
                     $email_template = build_email_template($parse_array, 'reset_password');
                 }  
                $to = $email;
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                mail($to, $subject, $email_template,$headers);
            }
             echo json_encode(array('status' => 'success', 'msg' => 'Please check your email. We have sent link to reset password on mail.'));
        exit();
    } else {
       
        echo json_encode(array('status' => 'error', 'msg' => 'The Email ' . $email . ' isnot exists in our system'));
        exit();
    }
}
?>