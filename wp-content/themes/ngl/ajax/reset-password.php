<?php 
add_action('wp_ajax_nopriv_reset_password', 'change_password');
add_action('wp_ajax_reset_password', 'change_password');

function change_password(){
	$post = array();
    parse_str($_POST['data'], $post);
    
    $password = sanitize_text_field($post["password"]);
    $user_id = $post["guid"];

    if (!empty($user_id)) {    
        $password = wp_set_password( $password, $user_id );
        $user_id = wp_update_user( array( 'ID' => $user_id, 'password' => $password)); 
        if ( is_wp_error( $user_id ) ) {
            echo json_encode(array('status' => 'error', 'msg' => "There was an error, probably that user doesn't exist"));
        } else {
            echo json_encode(array('status' => 'success', 'msg' => 'Your password has been changed successfully.'));
            exit();
        }       
       
    } else {
       
        echo json_encode(array('status' => 'error', 'msg' => 'Something went wrong.'));
        exit();
    }
}
?>