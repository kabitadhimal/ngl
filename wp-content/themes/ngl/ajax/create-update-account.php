<?php

add_action('wp_ajax_nopriv_account_creation', 'create_account');
add_action('wp_ajax_account_creation', 'create_account');

function create_account() {

    $post = array();
    parse_str($_POST['data'], $post);

    $email = sanitize_email($post["email"]);
    $user_id = username_exists($email);

    if (!$user_id and email_exists($email) == false) {
        $is_valid = 1;
    } else {
        $is_valid = 0;
        echo json_encode(array('status' => 'error', 'msg' => 'The Email ' . $email . ' is already exists'));
        exit();
    }



    if ($is_valid) {


        $fullname = sanitize_text_field($post["fullname"]);



//                     stdClass Object
// (
//     [type] => http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/
//     [title] => Invalid Resource
//     [status] => 400
//     [detail] => The domain portion of the email address is invalid (the portion after the @: gmail.com12)
//     [instance] => 
//)


        /* Mailchimp Membership */
        if (isset($post["newsletter"]) && $post["newsletter"]==1) {

            //insert into table newsletter 
            global $wpdb;
            $table=$wpdb->prefix.'newsletter';
            $name = explode(' ', $fullname);
            $fname = !empty($name[0]) ? $name[0] : '';
            $lname = !empty($name[1]) ? $name[1] : '';

              $data = array(
                 'email' => $email,
                 'name' => $fname,
                 'surname' => $lname,
                 'status' => 'C',
                 'http_referer' => site_url(),
             );

            $insert = $wpdb->insert( $table, $data); 
            
            // if(!$insert){
            //     echo json_encode(array('status' => 'error', 'msg' => __('The email already exists in our Newsletter. Please unsubcribe the newsletter subscription.', 'ngl')));
            //      exit();
            // }
            // $name = explode(' ', $fullname);
            // $fname = !empty($name[0]) ? $name[0] : '';
            // $lname = !empty($name[1]) ? $name[1] : '';
            // //debug($name);
            // $data = array(
            //     'email' => $email,
            //     'fname' => $fname,
            //     'lname' => $lname
            // );

            // $result = subscribedToMailchimp($data);
            // $result1 = json_decode($result);
            // //debug($result1);
            // if (!empty($result1->status) && $result1->status == 400) {
            //     echo json_encode(array('status' => 'error', 'msg' => __('The email already exists in our Newsletter. Please unsubcribe the newsletter subscription.', 'ngl')));
            //     exit();
            // }
        }


        // $surname = sanitize_text_field($post["surname"]);
        $password = sanitize_text_field($post["password"]);

        $encrypt_password = wp_set_password($password, $user_id);
        $user_id = wp_insert_user(array(
            'user_login' => $email,
            'user_pass' => $encrypt_password,
            'user_email' => $email,
            'display_name' => $fullname,
        ));

        /* added in usermeta */
        if ($user_id) {
            $company = $post["company"];
            $office = sanitize_text_field($post["office"]);
            $telephone_number = sanitize_text_field($post["telephone"]);
            $address = sanitize_text_field($post["address"]);
            $postal_code = sanitize_text_field($post["postal_code"]);
            $city = sanitize_text_field($post["city"]);
            $pays = sanitize_text_field($post["pays"]);


            add_user_meta($user_id, 'company', $company, true);
            add_user_meta($user_id, 'office', $office, true);
            add_user_meta($user_id, 'telephone_number', $telephone_number, true);
            add_user_meta($user_id, 'address', $address, true);
            add_user_meta($user_id, 'postal_code', $postal_code, true);
            add_user_meta($user_id, 'city', $city, true);
            add_user_meta($user_id, 'pays', $pays, true);


            /* added users company details in user meta table */

            add_user_meta($user_id, 'status', PENDING, true);
            add_user_meta($user_id, 'language',ICL_LANGUAGE_CODE, true);
            $guid = generate_user_guid();
            add_user_meta($user_id, 'guid', $guid, true);



            /* send email */
            if (!empty($email)) {
                $subject = 'New person has created an account';
                $parse_array = array(
                    '{SITE_URL}' => SITEURL,
                    '{HOMEURL}' => HOMEURL,
                    '{TEMP_DIR_URI}' => TEMP_DIR_URI,
                    '{CONTACT_US_LINK}' => CONTACT_URL,
                    '{LINKEDIN_LINK}' => LINKEDIN_URL,
                    '{NEWSLETTER_URL}' => NEWSLETTER_URL,
                    '{DEMAND_URL}' => DEMAND_URL,
                    '{CURRENT_YEAR}' => CURRENT_YEAR,
                    '{FROM_USERNAME}' => 'ADMIN',
                    '{FROM_EMAIL}' => get_option('admin_email'),
                    '{MEMBER_NAME}' => ucfirst($user->display_name),
                    '{MEMBER_EMAIL}' => $user->user_email
                );
                if (ICL_LANGUAGE_CODE == 'fr') {
                    $email_template = build_email_template($parse_array, 'account_approved', 'fr');
                } else {
                    $email_template = build_email_template($parse_array, 'account_approved');
                }

                //$email_template = build_email($parse_array, $member_registration);

                $to = get_option('admin_email');
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                mail($to, $subject, $email_template, $headers);
            }
        }

        echo json_encode(array('status' => 'success'));
        exit();
    }


    /* email */
//    add_filter('mail_content_type', 'set_content_type');
//wp_die(); // this is required to terminate immediately and return a proper response
}

add_action('wp_ajax_nopriv_account_update', 'account_update');
add_action('wp_ajax_account_update', 'account_update');

function account_update() {
    $post = array();
    parse_str($_POST['data'], $post);
    $email = sanitize_email($post["email"]);

    $exists_email = email_exists_check($email);

    if ($exists_email) {

        echo json_encode(array('status' => 'error', 'msg' => __('The email is already exists', 'ngl')));
    } else {

        $fullname = sanitize_text_field($post["fullname"]);
        $user_id = wp_update_user(array(
            'ID' => $post['user_id'],
            'user_login' => $email,
            'user_email' => $email,
            'display_name' => $fullname,
        ));

        /* added in usermeta */
        if ($user_id) {
            $company = $post["company"];
            $office = sanitize_text_field($post["office"]);
            $telephone_number = sanitize_text_field($post["telephone"]);
            $address = sanitize_text_field($post["address"]);
            $postal_code = sanitize_text_field($post["postal_code"]);
            $city = sanitize_text_field($post["city"]);
            $pays = sanitize_text_field($post["pays"]);


            update_user_meta($user_id, 'company', $company);
            update_user_meta($user_id, 'office', $office);
            update_user_meta($user_id, 'telephone_number', $telephone_number);
            update_user_meta($user_id, 'address', $address);
            update_user_meta($user_id, 'postal_code', $postal_code);
            update_user_meta($user_id, 'city', $city);
            update_user_meta($user_id, 'pays', $pays);
            echo json_encode(array('status' => 'success', 'msg' => __('The account has been updated successfully.', 'ngl')));
            exit();
        }
    }
}

?>