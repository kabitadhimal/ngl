<?php

function add_post_type($name, $args = array()) {
    add_action('init', function() use($name, $args) {
        $upper = ucwords($name);
        $name = strtolower(str_replace(' ', '_', $name));

        $args = array_merge(
                array(
            'public' => true,
            'label' => "$upper",
            'labels' => array('add_new_item' => "Add new $upper"),
            'supports' => array('title', 'editor', 'thumbnail')
                ), $args);

        register_post_type($name, $args);
    });
}

/*
 * Taxonomy
 */

function add_taxonomy($name, $post_type, $args = array()) {
    $name = strtolower($name);
    add_action('init', function() use($name, $post_type, $args) {
        $args = array_merge(
                array(
            'label' => ucwords($name),
            'show_ui' => true,
            'query_var' => true,
                ), $args);
     
        register_taxonomy($name, $post_type, $args);
    });
}

/*Account creation related function*/

function build_email_template($parse_data = '', $type = '',$lang='en') {
    $template = file_get_contents_curl(TEMP_DIR_URI .'/partial/email-templates/'.$lang.'/'. $type . '.html');
    $email_content = strtr($template, $parse_data);
    return $email_content;
}

function build_email($parse_data='', $template='') {
    $email_content = strtr($template, $parse_data);
    return $email_content;
}

function generate_user_guid($length = 16) {
    /* generate random string */
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $guid = '';
    for ($i = 0; $i < $length; $i++) {
        $guid .= $characters[rand(0, $charactersLength - 1)];
    }

    global $wpdb;
    $meta_key = 'guid';
    $sql = "SELECT count(*) FROM " . $wpdb->users . " as u
            JOIN " . $wpdb->usermeta . " as um "
            . "ON u.ID=um.user_id AND um.meta_key='" . $meta_key . "' "
            . "WHERE um.meta_value='" . $guid . "'";
    $count = $wpdb->get_var($sql);

    if ($count > 0) {
        generate_user_guid($length);
    }

    return $guid;
}

function check_valid_guid($guid = '') {
    global $wpdb;
    $meta_key = 'guid';
    $sql = "SELECT count(*) FROM " . $wpdb->users . " as u
            JOIN " . $wpdb->usermeta . " as um "
            . "ON u.ID=um.user_id AND um.meta_key='" . $meta_key . "' "
            . "WHERE um.meta_value='" . $guid . "'";
    $count = $wpdb->get_var($sql);
    return $count > 0 ? true : false;
}

function email_exists_check($email) {
    if ($user = get_user_by('email', $email)) {
        $current_user = wp_get_current_user();
        if ($user->user_email == $current_user->user_email)
            return false;
        else
            return $user->ID;
    }

    return false;
}

function getCountries(){

    if(ICL_LANGUAGE_CODE!='fr'){

    $countryArr= array(
            'Albania'=>'Albania',
            'Algeria'=>'Algeria',
            'American Samoa'=>'American Samoa',
            'Andorra'=>'Andorra',
            'Angola'=>'Angola',
            'Anguilla'=>'Anguilla',
            'Antigua & Barbuda'=>'Antigua & Barbuda',
            'Argentina'=>'Argentina',
            'Armenia'=>'Armenia',
            'Aruba'=>'Aruba',
            'Australia'=>'Australia',
            'Austria'=>'Austria',
            'Azerbaijan'=>'Azerbaijan',
            'Bahamas'=>'Bahamas',
            'Bahrain'=>'Bahrain',
            'Bangladesh'=>'Bangladesh',
            'Barbados'=>'Barbados',
            'Belarus'=>'Belarus',
            'Belgium'=>'Belgium',
            'Belize'=>'Belize',
            'Benin'=>'Benin',
            'Bermuda'=>'Bermuda',
            'Bhutan'=>'Bhutan',
            'Bolivia'=>'Bolivia',
            'Bonaire'=>'Bonaire',
            'Bosnia & Herzegovina'=>'Bosnia & Herzegovina',
            'Botswana'=>'Botswana',
            'Brazil'=>'Brazil',
            'British Indian Ocean Ter'=>'British Indian Ocean Ter',
            'Brunei'=>'Brunei',
            'Bulgaria'=>'Bulgaria',
            'Burkina Faso'=>'Burkina Faso',
            'Burundi'=>'Burundi',
            'Cambodia'=>'Cambodia',
            'Cameroon'=>'Cameroon',
            'Canada'=>'Canada',
            'Canary Islands'=>'Canary Islands',
            'Cape Verde'=>'Cape Verde',
            'Cayman Islands'=>'Cayman Islands',
            'Central African Republic'=>'Central African Republic',
            'Chad'=>'Chad',
            'Channel Islands'=>'Channel Islands',
            'Chile'=>'Chile',
            'China'=>'China',
            'Christmas Island'=>'Christmas Island',
            'Cocos Island'=>'Cocos Island',
            'Colombia'=>'Colombia',
            'Comoros'=>'Comoros',
            'Congo'=>'Congo',
            'Cook Islands'=>'Cook Islands',
            'Costa Rica'=>'Costa Rica',
            'Cote DIvoire'=>'Cote DIvoire',
            'Croatia'=>'Croatia',
            'Cuba'=>'Cuba',
            'Curaco'=>'Curaco',
            'Cyprus'=>'Cyprus',
            'Czech Republic'=>'Czech Republic',
            'Denmark'=>'Denmark',
            'Djibouti'=>'Djibouti',
            'Dominica'=>'Dominica',
            'Dominican Republic'=>'Dominican Republic',
            'East Timor'=>'East Timor',
            'Ecuador'=>'Ecuador',
            'Egypt'=>'Egypt',
            'El Salvador'=>'El Salvador',
            'england'=>'england',
            'Equatorial Guinea'=>'Equatorial Guinea',
            'Eritrea'=>'Eritrea',
            'Estonia'=>'Estonia',
            'Ethiopia'=>'Ethiopia',
            'Falkland Islands'=>'Falkland Islands',
            'Faroe Islands'=>'Faroe Islands',
            'Fiji'=>'Fiji',
            'Finland'=>'Finland',
            'France'=>'France',
            'French Guiana'=>'French Guiana',
            'French Polynesia'=>'French Polynesia',
            'French Southern Ter'=>'French Southern Ter',
            'Gabon'=>'Gabon',
            'Gambia'=>'Gambia',
            'Georgia'=>'Georgia',
            'Germany'=>'Germany',
            'Ghana'=>'Ghana',
            'Gibraltar'=>'Gibraltar',
            'Great Britain'=>'Great Britain',
            'Greece'=>'Greece',
            'Greenland'=>'Greenland',
            'Grenada'=>'Grenada',
            'Guadeloupe'=>'Guadeloupe',
            'Guam'=>'Guam',
            'Guatemala'=>'Guatemala',
            'Guinea'=>'Guinea',
            'Guyana'=>'Guyana',
            'Haiti'=>'Haiti',
            'Hawaii'=>'Hawaii',
            'Honduras'=>'Honduras',
            'Hong Kong'=>'Hong Kong',
            'Hungary'=>'Hungary',
            'Iceland'=>'Iceland',
            'India'=>'India',
            'Indonesia'=>'Indonesia',
            'Iran'=>'Iran',
            'Iraq'=>'Iraq',
            'Ireland'=>'Ireland',
            'Isle of Man'=>'Isle of Man',
            'Israel'=>'Israel',
            'Italy'=>'Italy',
            'Jamaica'=>'Jamaica',
            'Japan'=>'Japan',
            'Jordan'=>'Jordan',
            'Kazakhstan'=>'Kazakhstan',
            'Kenya'=>'Kenya',
            'Kiribati'=>'Kiribati',
            'Korea North'=>'Korea North',
            'Korea South'=>'Korea South',
            'Kuwait'=>'Kuwait',
            'Kyrgyzstan'=>'Kyrgyzstan',
            'Laos'=>'Laos',
            'Latvia'=>'Latvia',
            'Lebanon'=>'Lebanon',
            'Lesotho'=>'Lesotho',
            'Liberia'=>'Liberia',
            'Libya'=>'Libya',
            'Liechtenstein'=>'Liechtenstein',
            'Lithuania'=>'Lithuania',
            'Luxembourg'=>'Luxembourg',
            'Macau'=>'Macau',
            'Macedonia'=>'Macedonia',
            'Madagascar'=>'Madagascar',
            'Malaysia'=>'Malaysia',
            'Malawi'=>'Malawi',
            'Maldives'=>'Maldives',
            'Mali'=>'Mali',
            'Malta'=>'Malta',
            'Marshall Islands'=>'Marshall Islands',
            'Martinique'=>'Martinique',
            'Mauritania'=>'Mauritania',
            'Mauritius'=>'Mauritius',
            'Mayotte'=>'Mayotte',
            'Mexico'=>'Mexico',
            'Midway Islands'=>'Midway Islands',
            'Moldova'=>'Moldova',
            'Monaco'=>'Monaco',
            'Mongolia'=>'Mongolia',
            'Montserrat'=>'Montserrat',
            'Morocco'=>'Morocco',
            'Mozambique'=>'Mozambique',
            'Myanmar'=>'Myanmar',
            'Nambia'=>'Nambia',
            'Nauru'=>'Nauru',
            'Nepal'=>'Nepal',
            'Netherland Antilles'=>'Netherland Antilles',
            'Netherlands'=>'Netherlands',
            'Nevis'=>'Nevis',
            'New Caledonia'=>'New Caledonia',
            'New Zealand'=>'New Zealand',
            'Nicaragua'=>'Nicaragua',
            'Niger'=>'Niger',
            'Nigeria'=>'Nigeria',
            'Niue'=>'Niue',
            'Norfolk Island'=>'Norfolk Island',
            'Norway'=>'Norway',
            'Oman'=>'Oman',
            'Pakistan'=>'Pakistan',
            'Palau Island'=>'Palau Island',
            'Palestine'=>'Palestine',
            'Panama'=>'Panama',
            'Papua New Guinea'=>'Papua New Guinea',
            'Paraguay'=>'Paraguay',
            'Peru'=>'Peru',
            'Phillipines'=>'Phillipines',
            'Pitcairn Island'=>'Pitcairn Island',
            'Poland'=>'Poland',
            'Portugal'=>'Portugal',
            'Puerto Rico'=>'Puerto Rico',
            'Qatar'=>'Qatar',
            'Republic of Montenegro'=>'Republic of Montenegro',
            'Republic of Serbia'=>'Republic of Serbia',
            'Reunion'=>'Reunion',
            'Romania'=>'Romania',
            'Russia'=>'Russia',
            'Rwanda'=>'Rwanda',
            'St Barthelemy'=>'St Barthelemy',
            'St Eustatius'=>'St Eustatius',
            'St Helena'=>'St Helena',
            'St Kitts-Nevis'=>'St Kitts-Nevis',
            'St Lucia'=>'St Lucia',
            'St Maarten'=>'St Maarten',
            'St Pierre & Miquelon'=>'St Pierre & Miquelon',
            'St Vincent & Grenadines'=>'St Vincent & Grenadines',
            'Saipan'=>'Saipan',
            'Samoa'=>'Samoa',
            'Samoa American'=>'Samoa American',
            'San Marino'=>'San Marino',
            'Sao Tome & Principe'=>'Sao Tome & Principe',
            'Saudi Arabia'=>'Saudi Arabia',
            'Senegal'=>'Senegal',
            'Serbia'=>'Serbia',
            'Seychelles'=>'Seychelles',
            'Sierra Leone'=>'Sierra Leone',
            'Singapore'=>'Singapore',
            'Slovakia'=>'Slovakia',
            'Slovenia'=>'Slovenia',
            'Solomon Islands'=>'Solomon Islands',
            'Somalia'=>'Somalia',
            'South Africa'=>'South Africa',
            'Spain'=>'Spain',
            'Sri Lanka'=>'Sri Lanka',
            'Sudan'=>'Sudan',
            'Suriname'=>'Suriname',
            'Swaziland'=>'Swaziland',
            'Sweden'=>'Sweden',
            'Switzerland'=>'Switzerland',
            'Syria'=>'Syria',
            'Tahiti'=>'Tahiti',
            'Taiwan'=>'Taiwan',
            'Tajikistan'=>'Tajikistan',
            'Tanzania'=>'Tanzania',
            'Thailand'=>'Thailand',
            'Togo'=>'Togo',
            'Tokelau'=>'Tokelau',
            'Tonga'=>'Tonga',
            'Trinidad & Tobago'=>'Trinidad & Tobago',
            'Tunisia'=>'Tunisia',
            'Turkey'=>'Turkey',
            'Turkmenistan'=>'Turkmenistan',
            'Turks & Caicos Is'=>'Turks & Caicos Is',
            'Tuvalu'=>'Tuvalu',
            'Uganda'=>'Uganda',
            'Ukraine'=>'Ukraine',
            'United Arab Erimates'=>'United Arab Erimates',
            'United Kingdom'=>'United Kingdom',
            'United States of America'=>'United States of America',
            'Uraguay'=>'Uraguay',
            'Uzbekistan'=>'Uzbekistan',
            'Vanuatu'=>'Vanuatu',
            'Vatican City State'=>'Vatican City State',
            'Venezuela'=>'Venezuela',
            'Vietnam'=>'Vietnam',
            'Virgin Islands (Brit)'=>'Virgin Islands (Brit)',
            'Virgin Islands (USA)'=>'Virgin Islands (USA)',
            'Wake Island'=>'Wake Island',
            'Wallis & Futana Is'=>'Wallis & Futana Is',
            'Yemen'=>'Yemen',
            'Zaire'=>'Zaire',
            'Zambia'=>'Zambia',
            'Zimbabwe'=>'Zimbabwe');
}else{
 $countryArr= array( 
                    "Afghanistan" =>"Afghanistan",
                    "Albanie"=>"Albanie" ,
                    "Antarctique"=>"Antarctique" ,
                    "Algérie"=>"Algérie" ,
                    "Samoa Américaines"=>"Samoa Américaines" ,
                    "Andorre"=>"Andorre" ,
                    "Angola"=>"Angola" ,
                    "Antigua-et-Barbuda"=>"Antigua-et-Barbuda" ,
                    "Azerbaïdjan"=>"Azerbaïdjan" ,
                    "Argentine"=>"Argentine" ,
                    "Australie"=>"Australie" ,
                    "Autriche"=>"Autriche" ,
                    "Bahamas"=>"Bahamas" ,
                    "Bahreïn"=>"Bahreïn" ,
                    "Bangladesh"=>"Bangladesh" ,
                    "Arménie"=>"Arménie" ,
                    "Barbade"=>"Barbade" ,
                    "Belgique"=>"Belgique" ,
                    "Bermudes"=>"Bermudes" ,
                    "Bhoutan"=>"Bhoutan" ,
                    "Bolivie"=>"Bolivie" ,
                    "Bosnie-Herzégovine"=>"Bosnie-Herzégovine" ,
                    "Botswana"=>"Botswana" ,
                    "Île Bouvet"=>"Île Bouvet" ,
                    "Brésil"=>"Brésil" ,
                    "Belize"=>"Belize" ,
                    "Territoire Britannique de l'Océan Indien"=>"Territoire Britannique de l'Océan Indien" ,
                    "Îles Salomon"=>"Îles Salomon" ,
                    "Îles Vierges Britanniques"=>"Îles Vierges Britanniques" ,
                    "Brunéi Darussalam"=>"Brunéi Darussalam" ,
                    "Bulgarie"=>"Bulgarie" ,
                    "Myanmar"=>"Myanmar" ,
                    "Burundi"=>"Burundi" ,
                    "Bélarus"=>"Bélarus" ,
                    "Cambodge"=>"Cambodge" ,
                    "Cameroun"=>"Cameroun" ,
                    "Canada"=>"Canada" ,
                    "Cap-vert"=>"Cap-vert" ,
                    "Îles Caïmanes"=>"Îles Caïmanes" ,
                    "République Centrafricaine"=>"République Centrafricaine" ,
                    "Sri Lanka"=>"Sri Lanka" ,
                    "Tchad"=>"Tchad" ,
                    "Chili"=>"Chili" ,
                    "Chine"=>"Chine" ,
                    "Taïwan"=>"Taïwan" ,
                    "Île Christmas"=>"Île Christmas" ,
                    "Îles Cocos (Keeling)"=>"Îles Cocos (Keeling)" ,
                    "Colombie"=>"Colombie" ,
                    "Comores"=>"Comores" ,
                    "Mayotte"=>"Mayotte" ,
                    "République du Congo"=>"République du Congo" ,
                    "République Démocratique du Congo"=>"République Démocratique du Congo" ,
                    "Îles Cook"=>"Îles Cook" ,
                    "Costa Rica"=>"Costa Rica" ,
                    "Croatie"=>"Croatie" ,
                    "Cuba"=>"Cuba" ,
                    "Chypre"=>"Chypre" ,
                    "République Tchèque"=>"République Tchèque" ,
                    "Bénin"=>"Bénin" ,
                    "Danemark"=>"Danemark" ,
                    "Dominique"=>"Dominique" ,
                    "République Dominicaine"=>"République Dominicaine" ,
                    "Équateur"=>"Équateur" ,
                    "El Salvador"=>"El Salvador" ,
                    "Guinée Équatoriale"=>"Guinée Équatoriale" ,
                    "Éthiopie"=>"Éthiopie" ,
                    "Érythrée"=>"Érythrée" ,
                    "Estonie"=>"Estonie" ,
                    "Îles Féroé"=>"Îles Féroé" ,
                    "Îles (malvinas) Falkland"=>"Îles (malvinas) Falkland" ,
                    "Géorgie du Sud et les Îles Sandwich du Sud"=>"Géorgie du Sud et les Îles Sandwich du Sud" ,
                    "Fidji"=>"Fidji" ,
                    "Finlande"=>"Finlande" ,
                    "Îles Åland"=>"Îles Åland" ,
                    "France"=>"France" ,
                    "Guyane Française"=>"Guyane Française" ,
                    "Polynésie Française"=>"Polynésie Française" ,
                    "Terres Australes Françaises"=>"Terres Australes Françaises" ,
                    "Djibouti"=>"Djibouti" ,
                    "Gabon"=>"Gabon" ,
                    "Géorgie"=>"Géorgie" ,
                    "Gambie"=>"Gambie" ,
                    "Territoire Palestinien Occupé"=>"Territoire Palestinien Occupé" ,
                    "Allemagne"=>"Allemagne" ,
                    "Ghana"=>"Ghana" ,
                    "Gibraltar"=>"Gibraltar" ,
                    "Kiribati"=>"Kiribati" ,
                    "Grèce"=>"Grèce" ,
                    "Groenland"=>"Groenland" ,
                    "Grenade"=>"Grenade" ,
                    "Guadeloupe"=>"Guadeloupe" ,
                    "Guam"=>"Guam" ,
                    "Guatemala"=>"Guatemala" ,
                    "Guinée"=>"Guinée" ,
                    "Guyana"=>"Guyana" ,
                    "Haïti"=>"Haïti" ,
                    "Îles Heard et Mcdonald"=>"Îles Heard et Mcdonald" ,
                    "Saint-Siège (état de la Cité du Vatican)"=>"Saint-Siège (état de la Cité du Vatican)" ,
                    "Honduras"=>"Honduras" ,
                    "Hong-Kong"=>"Hong-Kong" ,
                    "Hongrie"=>"Hongrie" ,
                    "Islande"=>"Islande" ,
                    "Inde"=>"Inde" ,
                    "Indonésie"=>"Indonésie" ,
                    "République Islamique d'Iran"=>"République Islamique d'Iran" ,
                    "Iraq"=>"Iraq" ,
                    "Irlande"=>"Irlande" ,
                    "Israël"=>"Israël" ,
                    "Italie"=>"Italie" ,
                    "Côte d'Ivoire"=>"Côte d'Ivoire" ,
                    "Jamaïque"=>"Jamaïque" ,
                    "Japon"=>"Japon" ,
                    "Kazakhstan"=>"Kazakhstan" ,
                    "Jordanie"=>"Jordanie" ,
                    "Kenya"=>"Kenya" ,
                    "République Populaire Démocratique de Corée"=>"République Populaire Démocratique de Corée" ,
                    "République de Corée"=>"République de Corée" ,
                    "Koweït"=>"Koweït" ,
                    "Kirghizistan"=>"Kirghizistan" ,
                    "République Démocratique Populaire Lao"=>"République Démocratique Populaire Lao" ,
                    "Liban"=>"Liban" ,
                    "Lesotho"=>"Lesotho" ,
                    "Lettonie"=>"Lettonie" ,
                    "Libéria"=>"Libéria" ,
                    "Jamahiriya Arabe Libyenne"=>"Jamahiriya Arabe Libyenne" ,
                    "Liechtenstein"=>"Liechtenstein" ,
                    "Lituanie"=>"Lituanie" ,
                    "Luxembourg"=>"Luxembourg" ,
                    "Macao"=>"Macao" ,
                    "Madagascar"=>"Madagascar" ,
                    "Malawi"=>"Malawi" ,
                    "Malaisie"=>"Malaisie" ,
                    "Maldives"=>"Maldives" ,
                    "Mali"=>"Mali" ,
                    "Malte"=>"Malte" ,
                    "Martinique"=>"Martinique" ,
                    "Mauritanie"=>"Mauritanie" ,
                    "Maurice"=>"Maurice" ,
                    "Mexique"=>"Mexique" ,
                    "Monaco"=>"Monaco" ,
                    "Mongolie"=>"Mongolie" ,
                    "République de Moldova"=>"République de Moldova" ,
                    "Montserrat"=>"Montserrat" ,
                    "Maroc"=>"Maroc" ,
                    "Mozambique"=>"Mozambique" ,
                    "Oman"=>"Oman" ,
                    "Namibie"=>"Namibie" ,
                    "Nauru"=>"Nauru" ,
                    "Népal"=>"Népal" ,
                    "Pays-Bas"=>"Pays-Bas" ,
                    "Antilles Néerlandaises"=>"Antilles Néerlandaises" ,
                    "Aruba"=>"Aruba" ,
                    "Nouvelle-Calédonie"=>"Nouvelle-Calédonie" ,
                    "Vanuatu"=>"Vanuatu" ,
                    "Nouvelle-Zélande"=>"Nouvelle-Zélande" ,
                    "Nicaragua"=>"Nicaragua" ,
                    "Niger"=>"Niger" ,
                    "Nigéria"=>"Nigéria" ,
                    "Niué"=>"Niué" ,
                    "Île Norfolk"=>"Île Norfolk" ,
                    "Norvège"=>"Norvège" ,
                    "Îles Mariannes du Nord"=>"Îles Mariannes du Nord" ,
                    "Îles Mineures Éloignées des États-Unis"=>"Îles Mineures Éloignées des États-Unis" ,
                    "États Fédérés de Micronésie"=>"États Fédérés de Micronésie" ,
                    "Îles Marshall"=>"Îles Marshall" ,
                    "Palaos"=>"Palaos" ,
                    "Pakistan"=>"Pakistan" ,
                    "Panama"=>"Panama" ,
                    "Papouasie-Nouvelle-Guinée"=>"Papouasie-Nouvelle-Guinée" ,
                    "Paraguay"=>"Paraguay" ,
                    "Pérou"=>"Pérou" ,
                    "Philippines"=>"Philippines" ,
                    "Pitcairn"=>"Pitcairn" ,
                    "Pologne"=>"Pologne" ,
                    "Portugal"=>"Portugal" ,
                    "Guinée-Bissau"=>"Guinée-Bissau" ,
                    "Timor-Leste"=>"Timor-Leste" ,
                    "Porto Rico"=>"Porto Rico" ,
                    "Qatar"=>"Qatar" ,
                    "Réunion"=>"Réunion" ,
                    "Roumanie"=>"Roumanie" ,
                    "Fédération de Russie"=>"Fédération de Russie" ,
                    "Rwanda"=>"Rwanda" ,
                    "Sainte-Hélène"=>"Sainte-Hélène" ,
                    "Saint-Kitts-et-Nevis"=>"Saint-Kitts-et-Nevis" ,
                    "Anguilla"=>"Anguilla" ,
                    "Sainte-Lucie"=>"Sainte-Lucie" ,
                    "Saint-Pierre-et-Miquelon"=>"Saint-Pierre-et-Miquelon" ,
                    "Saint-Vincent-et-les Grenadines"=>"Saint-Vincent-et-les Grenadines" ,
                    "Saint-Marin"=>"Saint-Marin" ,
                    "Sao Tomé-et-Principe"=>"Sao Tomé-et-Principe" ,
                    "Arabie Saoudite"=>"Arabie Saoudite" ,
                    "Sénégal"=>"Sénégal" ,
                    "Seychelles"=>"Seychelles" ,
                    "Sierra Leone"=>"Sierra Leone" ,
                    "Singapour"=>"Singapour" ,
                    "Slovaquie"=>"Slovaquie" ,
                    "Viet Nam"=>"Viet Nam" ,
                    "Slovénie"=>"Slovénie" ,
                    "Somalie"=>"Somalie" ,
                    "Afrique du Sud"=>"Afrique du Sud" ,
                    "Zimbabwe"=>"Zimbabwe" ,
                    "Espagne"=>"Espagne" ,
                    "Sahara Occidental"=>"Sahara Occidental" ,
                    "Soudan"=>"Soudan" ,
                    "Suriname"=>"Suriname" ,
                    "Svalbard etÎle Jan Mayen"=>"Svalbard etÎle Jan Mayen" ,
                    "Swaziland"=>"Swaziland" ,
                    "Suède"=>"Suède" ,
                    "Suisse"=>"Suisse" ,
                    "République Arabe Syrienne"=>"République Arabe Syrienne" ,
                    "Tadjikistan"=>"Tadjikistan" ,
                    "Thaïlande"=>"Thaïlande" ,
                    "Togo"=>"Togo" ,
                    "Tokelau"=>"Tokelau" ,
                    "Tonga"=>"Tonga" ,
                    "Trinité-et-Tobago"=>"Trinité-et-Tobago" ,
                    "Émirats Arabes Unis"=>"Émirats Arabes Unis" ,
                    "Tunisie"=>"Tunisie" ,
                    "Turquie"=>"Turquie" ,
                    "Turkménistan"=>"Turkménistan" ,
                    "Îles Turks et Caïques"=>"Îles Turks et Caïques" ,
                    "Tuvalu"=>"Tuvalu" ,
                    "Ouganda"=>"Ouganda" ,
                    "Ukraine"=>"Ukraine" ,
                    "L'ex-République Yougoslave de Macédoine"=>"L'ex-République Yougoslave de Macédoine" ,
                    "Égypte"=>"Égypte" ,
                    "Royaume-Uni"=>"Royaume-Uni" ,
                    "Île de Man"=>"Île de Man" ,
                    "République-Unie de Tanzanie"=>"République-Unie de Tanzanie" ,
                    "États-Unis"=>"États-Unis" ,
                    "Îles Vierges des États-Unis"=>"Îles Vierges des États-Unis" ,
                    "Burkina Faso"=>"Burkina Faso" ,
                    "Uruguay"=>"Uruguay" ,
                    "Ouzbékistan"=>"Ouzbékistan" ,
                    "Venezuela"=>"Venezuela" ,
                    "Wallis et Futuna"=>"Wallis et Futuna" ,
                    "Samoa"=>"Samoa" ,
                    "Yémen"=>"Yémen" ,
                    "Serbie-et-Monténégro"=>"Serbie-et-Monténégro" ,
                    "Zambie"=>"Zambi");
}

return $countryArr;
}


function file_get_contents_curl($url) {
    $ch = curl_init();

//    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    $data = curl_exec($ch);

    return $data;

;
}



























































































































































































































































