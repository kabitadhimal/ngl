<?php 

/* Add custom  bulk action */
add_action('admin_footer', 'ebum_admin_footer');

function ebum_admin_footer() {
    global $pagenow;

    if ($pagenow == 'users.php') {
        ?>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('<option>').val('approved').text('Change status to Approved').appendTo("select[name='action']");
                jQuery('<option>').val('approved').text('Change status to Approved').appendTo("select[name='action2']");
            });
        </script>
        <?php

    }
}

add_action('load-edit.php', 'ebum_load');
add_action('load-users.php', 'ebum_load');

function ebum_load() {

    $wp_list_table = _get_list_table('WP_Users_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
    $action = $wp_list_table->current_action();

    $allowed_actions = array('approved');
    //debug($allowed_actions);
    if (!in_array($action, $allowed_actions))
        return;

    // security check
    check_admin_referer('bulk-users');

    // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
    if (isset($_REQUEST['users'])) {
        $user_ids = array_map('intval', $_REQUEST['users']);
    }

    if (empty($user_ids))
        return;

    // this is based on wp-admin/edit.php
    $sendback = remove_query_arg(array('rejected', 'closed', 'payment_requested', 'untrashed', 'deleted', 'ids'), wp_get_referer());
    if (!$sendback)
        $sendback = admin_url("users.php");

    $pagenum = $wp_list_table->get_pagenum();
    $sendback = add_query_arg('paged', $pagenum, $sendback);


    switch ($action) {
         
            
            case 'approved':
                //die('in approved');
            $approved = 0;

            foreach ($user_ids as $user_id) {

                if (!perform_user_approve($user_id))
                    wp_die(__('Error on approval of account.'));

                $approved++;
            }

            $sendback = add_query_arg(array('approved' => $approved, 'ids' => join(',', $user_ids)), $sendback);
            break;
            
        default: return;
    }

    $sendback = remove_query_arg(array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view'), $sendback);

    wp_redirect($sendback);
    exit();
}

function perform_user_approve($user_id) {
    update_user_meta($user_id, 'status', APPROVED);
   
   $user = get_userdata($user_id);
   $language = get_user_meta($user_id, 'language',true);
   
    /* send email to user with login credentials */
  
    $subject = 'Account Approved By NGL';

    $parse_array = array(       
        '{SITE_URL}' =>SITEURL,
        '{HOMEURL}' => HOMEURL,
        '{TEMP_DIR_URI}' => TEMP_DIR_URI,
        '{CONTACT_US_LINK}' => CONTACT_URL,
        '{LINKEDIN_LINK}' => LINKEDIN_URL,
        '{NEWSLETTER_URL}' => NEWSLETTER_URL,
        '{DEMAND_URL}' => DEMAND_URL,
        '{CURRENT_YEAR}' => CURRENT_YEAR,
        '{FROM_USERNAME}' => 'ADMIN',
        '{FROM_EMAIL}' => get_option('admin_email'),
        '{MEMBER_NAME}' => ucfirst($user->display_name),
        '{MEMBER_EMAIL}' => $user->user_email    
       
    );

    if($language=='fr'){
         $email_template = build_email_template($parse_array, 'account_approved','fr');
     }else{
         $email_template = build_email_template($parse_array, 'account_approved');
     }
   
  
    $to = $user->user_email;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    mail($to, $subject, $email_template,$headers);
   
    return true;
}


/* Add status column */
add_filter('manage_users_columns', 'custom_column_user_table');

function custom_column_user_table($column) {
    unset($column['role']);
    unset($column['posts']);
    $column['status'] = 'Status';
   
    $column['address'] = 'Address';
    $column['registered_date'] = 'Registered Date';
  

    return $column;
}

add_filter('manage_users_custom_column', 'custom_column_user_table_row', 10, 3);

function custom_column_user_table_row($val, $column_name, $user_id) {
    $user = get_userdata($user_id);
    /* not necessary for admins */
    if (!user_can($user_id, 'manage_options')) {
        $registered_date = date('d M , Y', strtotime($user->user_registered));
        $user_status = get_user_meta($user->ID, 'status', true);

        $address = get_user_meta($user->ID, 'address');      

       
      

        $status = '';
        switch ($user_status[0]) {
            case PENDING:
                $status = 'Pending';
                break;
            case APPROVED:
                $status = 'Approved';
                break;
            
        }

        //debug($status);

        switch ($column_name) {
            case 'status' :
                return !user_can($user_id, 'manage_options') ? $status : '';
                break;
            case 'registered_date':
                return $registered_date;
                break;
            case 'address':
                return !user_can($user_id, 'manage_options') ? $address[0] : '';
                break;
            default:
        }
        return $status;
    }
}

/* sortable custom user column */

function user_sortable_columns($columns) {

    $columns['status'] = 'status';
    $columns['registered_date'] = 'user_registered';
    $columns['address'] = 'address';
   
    return $columns;
}

add_filter('manage_users_sortable_columns', 'user_sortable_columns');

/* sorting of status which is lies in usermeta table */
add_action('pre_user_query', 'status_order_in_user_query');

function status_order_in_user_query($query) {

    global $wpdb;
    // debug($query);
    if ('status' == $query->query_vars['orderby']) {
        $query->query_fields = "u.*,um.*";
        $query->query_from = "FROM " . $wpdb->prefix . "users as u JOIN " . $wpdb->prefix . "usermeta as um ON um.user_id=u.ID AND um.meta_key='status'";
    }
    if ('status' == $query->query_vars['orderby']) {

        $query->query_orderby = " ORDER BY um.meta_value " . ($query->query_vars["order"] == "ASC" ? "asc " : "desc "); //set sort order
    }

    if (!empty($query->query_vars['role']) && $query->query_vars['role'] == 'subscriber' && 'status' == $query->query_vars['orderby']) {

        $query->query_where = ""; //set sort order
    }


    

    if ('address' == $query->query_vars['orderby']) {
        $query->query_fields = "u.*,um.*";
        $query->query_from = "FROM " . $wpdb->prefix . "users as u JOIN " . $wpdb->prefix . "usermeta as um ON um.user_id=u.ID AND um.meta_key='address'";
    }
    if ('address' == $query->query_vars['orderby']) {

        $query->query_orderby = " ORDER BY um.meta_value " . ($query->query_vars["order"] == "ASC" ? "asc " : "desc "); //set sort order
    }

    
}



?>