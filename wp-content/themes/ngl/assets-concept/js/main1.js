function pathPrepare ($el) {
    var lineLength = $el[0].getTotalLength();
    $el.css("stroke-dasharray", lineLength);
    $el.css("stroke-dashoffset", lineLength);
}
$(".story-path path.story-path__l").each(function(){
    pathPrepare($(this));
});


var controller = new ScrollMagic.Controller();

var tween0 = new TimelineMax()
    .add(TweenMax.to('#scene-1 .story-sep__icon', 0.3, {scale: 1, ease:Linear.easeNone}))

function calculateHeight($dom){
    $dom.innerHeight()
}
var scene0Height = calculateHeight($('#scene-0'));

new ScrollMagic.Scene({
    triggerElement: "#scene-0",
    triggerHook: "onLeave",
    //offset:-200,
    duration:scene0Height
})
    .setTween(tween0)
    //.setPin('#scene-0')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);


var tween1 = new TimelineMax()
    .add(TweenMax.to('#scene-1-path-1', 0.6, {strokeDashoffset: 0,ease:Linear.easeNone}))
    .add(TweenMax.to('#story-lab-slides-1', 0.9, {opacity: 1, ease:Linear.easeNone}))
    .add([
        TweenMax.to('#story-lab-slides-1', 0.9, {delay:0.6, opacity: 0, ease:Linear.easeNone}),
        TweenMax.to('#story-lab-slides-2', 0.9, {delay:1, opacity: 1, ease:Linear.easeNone})
    ])
    .add(TweenMax.to('#scene-1-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    .add(TweenMax.to('#scene-2 .story-sep__icon', 0.6, {scale: 1, ease:Linear.easeNone}));


new ScrollMagic.Scene({
    triggerElement: "#scene-1",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'400%'
})
    .setTween(tween1)
    .setPin('#scene-1')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);


//
var tween2 = new TimelineMax()
    .add(TweenMax.to('#scene-2-path-1', 0.9, {strokeDashoffset: 0,ease:Linear.easeNone}))
    /*
    .add(TweenMax.to('#story-map-slides-2', 0.9, {opacity: 1, ease:Cubic.easeInOut}))
    .add(TweenMax.to('#story-map-slides-3', 0.9, {transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}))    //marker
    .add([
        TweenMax.to('#story-map-slides-4', 0.9, {transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
        TweenMax.to('#story-map-slides-5', 0.9, {delay:0.2, transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
        TweenMax.to('#story-map-slides-6', 0.9, {delay:0.1, transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
        TweenMax.to('#story-map-slides-7', 0.9, {transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
        TweenMax.to('#story-map-slides-8', 0.9, {delay:0.2, transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
        TweenMax.to('#story-map-slides-9', 0.9, {delay:0.1, transform:'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
    ])
    .add(TweenMax.to('#scene-2-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    .add(TweenMax.to('#scene-3 .story-sep__icon', 0.6, {scale: 1, ease:Linear.easeNone}));
    */


new ScrollMagic.Scene({
    triggerElement: "#scene-2",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'300%'
})
    .setTween(tween2)
    .setPin('#scene-2')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);

/*
//
var tween3 = new TimelineMax()
    .add(TweenMax.to('#scene-3-path-1', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone }))
    .add(TweenMax.to('#scene-3-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    .add(TweenMax.to('#scene-4 .story-sep__icon', 0.6, {scale: 1, ease:Linear.easeNone}));;

new ScrollMagic.Scene({
    triggerElement: "#scene-3",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'300%'
})
    .setTween(tween3)
    .setPin('#scene-3')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);

//
var tween4 = new TimelineMax()
    .add(TweenMax.to('#scene-4-path-1', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone }))
    .add(TweenMax.to('#scene-4-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    .add(TweenMax.to('#scene-5 .story-sep__icon', 0.6, {scale: 1, ease:Linear.easeNone}));;

new ScrollMagic.Scene({
    triggerElement: "#scene-4",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'300%'
})
    .setTween(tween4)
    .setPin('#scene-4')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);


//
var tween5 = new TimelineMax()
    .add(TweenMax.to('#scene-5-path-1', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone }))
    .add(TweenMax.to('#scene-5-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    .add(TweenMax.to('#scene-6 .story-sep__icon', 0.6, {scale: 1, ease:Linear.easeNone}));;

new ScrollMagic.Scene({
    triggerElement: "#scene-4",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'300%'
})
    .setTween(tween5)
    .setPin('#scene-5')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);


//
var tween6 = new TimelineMax()
    .add(TweenMax.to('#scene-6-path-1', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone }))
    .add(TweenMax.to('#scene-6-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    //.add(TweenMax.to('#scene-6 .story-sep__icon', 0.6, {scale: 1, ease:Linear.easeNone}));;

new ScrollMagic.Scene({
    triggerElement: "#scene-6",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'300%'
})
    .setTween(tween6)
    .setPin('#scene-6')
    .addIndicators() // add indicators (requires plugin)
    .addTo(controller);
   */