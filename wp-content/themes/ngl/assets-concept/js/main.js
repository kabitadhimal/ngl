;(function($) {
    var $win = $(window),
        $header = $('#ipro-masthead'),
        lastScrollTop = 0;

    $win.on('scroll',function() {
        scrollPercentage = $(this).scrollTop();
        if(scrollPercentage > lastScrollTop) {
            $header.addClass('scroll-enabled');
        }else {
            $header.removeClass('scroll-enabled');
        }
        lastScrollTop = scrollPercentage;
    });
})(jQuery);

function pathPrepare ($el) {
    var lineLength = $el[0].getTotalLength();
    $el.css("stroke-dasharray", lineLength);
    $el.css("stroke-dashoffset", lineLength);
    $el.css("visibility", 'visible');
}
$(".story-path path.story-path__l").each(function(){ pathPrepare($(this));});


var controller = new ScrollMagic.Controller();

//picto ani
var pictos = [
    '#scene-1 .story-sep__icon',
    '#scene-2 .story-sep__icon',
    '#scene-3 .story-sep__icon',
    '#scene-4 .story-sep__icon',
    '#scene-5 .story-sep__icon',
    '#scene-6 .story-sep__icon',
    '#scene-1 .story-row',
    '#scene-2 .story-row',
    '#scene-3 .story-row',
    '#scene-4 .story-row',
    '#scene-5 .story-row'
];
for(var index in pictos) {
    new ScrollMagic.Scene({triggerElement: pictos[index],triggerHook: "onCenter"})
        .setClassToggle(pictos[index], "active")
        .addTo(controller);
}


new ScrollMagic.Scene({
    triggerElement: "#scene-1",
    triggerHook: "onLeave",
    //offset:-200,
    duration:'100%'
})
    .setTween(new TimelineMax()
        .add(TweenMax.to('#scene-1-path-1', 0.6, {
            strokeDashoffset: 0,
            ease:Linear.easeNone,
                onUpdate: function(){
                    if(this.progress() >= 1) {
                        $('#scene-1 .story-slides, #scene-1 .story-ani').addClass('active')
                    }else{
                        $('#scene-1 .story-slides, #scene-1 .story-ani').removeClass('active')
                    }
                }
        }
        ))
        /*
        .add([
            TweenMax.to('#story-lab-slides-1', 0.9, {transform: 'scale(1) translateY(0)', opacity: 1, ease:Linear.easeNone}),
            TweenMax.to('#scene-1 .story-ani-1', 0.9, {transform: 'translateY(0)', opacity: 1, ease:Cubic.easeInOut}),
            TweenMax.to('#scene-1 .story-ani-2', 0.9, {delay:0.1, transform: 'translateY(0)', opacity: 1, ease:Cubic.easeInOut})

        ])
        */
        .add(TweenMax.to('#scene-1-path-2', 0.9, {strokeDashoffset: 0, ease:Linear.easeNone}))
    )
    .setPin('#scene-1')
    //.addIndicators() // add indicators (requires plugin)
    .addTo(controller);

//
new ScrollMagic.Scene({
    triggerElement: "#scene-2",
    triggerHook: "onLeave",
    duration: '100%'
})
    .setTween(new TimelineMax()
        .add(TweenMax.to('#scene-2-path-1', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone, ease:Linear.easeNone,
            onUpdate: function(){
                if(this.progress() >= 1) {
                    $('#scene-2 .story-ani').addClass('active')
                }else{
                    $('#scene-2 .story-ani').removeClass('active')
                }
            }
        }))
        .add(TweenMax.to('#story-map-slides-2', 0.9, {opacity: 1, ease: Cubic.easeInOut }))
        .add(TweenMax.to('#story-map-slides-3', 0.9, {transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }))    //marker
        .add([
            TweenMax.to('#story-map-slides-4', 0.9, {transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }),
            TweenMax.to('#story-map-slides-5', 0.9, {delay: 0.3, transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }),
            TweenMax.to('#story-map-slides-6', 0.9, {delay: 0.1, transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }),
            TweenMax.to('#story-map-slides-7', 0.9, {transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }),
            TweenMax.to('#story-map-slides-8', 0.9, {delay: 0.6, transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }),
            TweenMax.to('#story-map-slides-9', 0.9, {delay: 0.4, transform: 'translateY(0)', opacity: 1, ease: Cubic.easeInOut }),
        ])
        .add(TweenMax.to('#scene-2-path-2', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone}))
    )
    .setPin('#scene-2')
    .addTo(controller);


 //
new ScrollMagic.Scene({
    triggerElement: "#scene-3",
    triggerHook: "onLeave",
    duration: '100%'
})
    .setTween(new TimelineMax()
        .add(TweenMax.to('#scene-3-path-1', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone,
            onUpdate: function(){
                if(this.progress() >= 1) {
                    $('#scene-3 .story-slides, #scene-3 .story-ani').addClass('active')
                }else{
                    $('#scene-3 .story-slides, #scene-3 .story-ani').removeClass('active')
                }
            }
        }))
        //.add(TweenMax.to('#story-blab-slides-1', 0.9, {opacity: 1, ease: Linear.easeNone}))
        /*
        .add([
            TweenMax.to('#story-blab-slides-1', 0.9, {opacity: 0, ease: Linear.easeNone}),
            TweenMax.to('#story-blab-slides-2', 0.9, {opacity: 1, ease: Linear.easeNone})
        ])
        */
        .add(TweenMax.to('#scene-3-path-2', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone}))
    )
    .setPin('#scene-3')
    .addTo(controller);

 //
new ScrollMagic.Scene({
    triggerElement: "#scene-4",
    triggerHook: "onLeave",
    duration: '100%'
})
    .setTween(new TimelineMax()
        .add(TweenMax.to('#scene-4-path-1', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone,
            onUpdate: function(){
                if(this.progress() >= 1) {
                    $('#scene-4 .story-slides, #scene-4 .story-ani').addClass('active')
                }else{
                    $('#scene-4 .story-slides, #scene-4 .story-ani').removeClass('active')
                }
            }
        }))
        /*
        .add(TweenMax.to('#story-filtre-slides-1', 0.9, {
            transform: 'scale(1) translateY(0)',
            opacity: 1,
            ease: Linear.easeNone
        }))
        */
        .add(TweenMax.to('#scene-4-path-2', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone}))
    )
    .setPin('#scene-4')
    //.addIndicators() // add indicators (requires plugin)
    .addTo(controller);


 //
new ScrollMagic.Scene({
    triggerElement: "#scene-5",
    triggerHook: "onLeave",
    duration: '100%'
})
    .setTween(new TimelineMax()
        .add(TweenMax.to('#scene-5-path-1', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone,
            onUpdate: function(){
                if(this.progress() >= 1) {
                    $('#scene-5 .story-slides, #scene-5 .story-ani').addClass('active')
                }else{
                    $('#scene-5 .story-slides, #scene-5 .story-ani').removeClass('active')
                }
            }
        }))
        /*
        .add(TweenMax.to('#story-present-slides-1', 0.9, {
            transform: 'scale(1) translateY(0)',
            opacity: 1,
            ease: Linear.easeNone
        }))
        */
        .add(TweenMax.to('#scene-5-path-2', 0.9, {strokeDashoffset: 0, ease: Linear.easeNone}))
    )
    .setPin('#scene-5')
    .addTo(controller);


