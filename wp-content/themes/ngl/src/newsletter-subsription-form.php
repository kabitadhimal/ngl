<!--<link rel="stylesheet" type="text/css" href="https://secure.inescrm.com/styles/sites.css">
<link rel="stylesheet" type="text/css" href="https://secure.inescrm.com/styles/bande.css">-->
<!--<script type="text/javascript" src="https://secure.inescrm.com/js/Base64.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/AjaxHelper.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/form_validation.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/md5.js"></script>
<script type="text/javascript" src="https://secure.inescrm.com/js/jcap.js"></script>-->
<script type="text/javascript">

    function checkformSubscription() {
        var str;
        if (str = validateMandatoryTextField('TextBox_109176', 'Veuillez saisir une valeur pour le champ obligatoire "Contact > Nom du contact"', 1)) {
            alert(str);
            return false;
        }
        ;
        if (str = validateMandatoryTextField('TextBox_109177', 'Veuillez saisir une valeur pour le champ obligatoire "Contact > Prénom"', 1)) {
            alert(str);
            return false;
        }
        ;
        if (str = validateEmail('TextBox_109178', 'Format de mail incorrect pour "Contact > Email 1"', 'Veuillez saisir une valeur pour le champ obligatoire "Contact > Email 1"', 1)) {
            alert(str);
            return false;
        }
        ;
        if (str = validateMandatoryTextField('TextBox_109175', 'Veuillez saisir une valeur pour le champ obligatoire "Société > Société - Nom"', 0)) {
            alert(str);
            return false;
        }
        ;
        /*
                if (str=validateMandatoryTextField('Select_109415','Veuillez saisir une valeur pour le champ obligatoire "Société > Langue"',1)){ alert(str); return false; };
        */
        /*
                if (str=validateMandatoryTextField('Checkbox_109416','Veuillez saisir une valeur pour le champ obligatoire "Website message > Newsletter"',1)){ alert(str); return false; };
        */

        /*    document.getElementById('submit').disabled = true;
            var capOK = jcap();
            if (!capOK)
                document.getElementById('submit').disabled = false;
            return capOK }*/
    }
</script>



<form id="WebLeadsForm_NGLCRM" action="https://extend.inescrm.com/InesWebFormHandler/Main.aspx" method="POST" onsubmit="return checkformSubscription()" class="newsletter-form ipro-form__wrap" >
    <input type=hidden name="controlid" id="controlid" value="757485064" />
    <input type=hidden name="oid" id="oid" value="e13f94c9-f977-495a-aaa4-590676109ce0" />
    <input type=hidden name="formid" id="formid" value="5512" />
    <input type=hidden name="retURL" id="retURL" value="<?php echo home_url('/'); ?>thank-you/" />
    <input type=hidden name="data" id="data" value="" />
    <input type=hidden name="Alias" id="Alias" value="NGLCRM" />
    <!--  ----------------------------------------------------------------------      -->

    <div class="row form-gap">
        <div class="col-sm-6">

            <!--<label id="Label_0" > Nom du contact <span class="texte4">*</span></label>-->


            <input id="TextBox_109176" class="txt" name="TextBox_109176" maxlength="40" size="45" placeholder="<?php _e("Last name","app"); ?> *" type="text" style="text-align:left;;text-transform:" required />


        </div>

        <div  class="col-sm-6">

            <!--<label id="Label_1" > Pr&#233;nom <span class="texte4">*</span></label>-->


            <input id="TextBox_109177" class="txt" name="TextBox_109177" maxlength="40" size="45" placeholder="<?php _e("First name","app"); ?>  *" type="text" style="text-align:left;;text-transform:" required />


        </div>
        </div>


<div class="row form-gap">
        <div class="col-sm-6 ">

            <!--<label id="Label_2" > Email 1 <span class="texte4">*</span></label>-->


            <input id="TextBox_109178" class="txt" name="TextBox_109178" maxlength="320" size="45" placeholder="<?php _e("Email address","app"); ?> *" type="text" style="text-align:left;;text-transform:" required />


        </div>

        <div class="col-sm-6"  >

            <!--<label id="Label_3" > Langue <span class="texte4">*</span></label>-->
            <input id="TextBox_109175" class="txt" name="TextBox_109175" maxlength="50" size="45" placeholder="<?php _e("Company","app"); ?> *" type="text" required  />

            <?php if(ICL_LANGUAGE_CODE=="zh-hans") {
                $displayLang="en" ;
            }elseif (ICL_LANGUAGE_CODE=="de"){
                $displayLang="GER" ;

            }else {
                $displayLang= ICL_LANGUAGE_CODE;
            }
            ?>
            <input type="text" readonly class="txt" style="display: none" id="Select_109415" name="Select_109415" value="<?php echo strtoupper($displayLang); ?>">
            <!--<select id="Select_109415" name="Select_109415" class="ipro-form__dropdown--primary"  required><option id="" value="" selected="selected" >-- Sélectionner *--</option>
                <option id="Select_109415_FR" value="FR"  >French</option>
                <option id="Select_109415_EN" value="EN"  >English</option>
                <option id="Select_109415_ES" value="ES"  >Spanish</option>
                <option id="Select_109415_GER" value="GER"  >German</option>
                <option id="Select_109415_PT" value="PT"  >Portuguese</option>
                <option id="Select_109415_IT" value="IT"  >Italian</option>
            </select>-->

        </div>
        </div>

<!--    <div class="row form-gap">
        <div class="col-sm-12 ">
                <div class="col-xs-5 col-sm-3">
                    <label id="Label_4" > Newsletter <span class="texte4">*</span></label>
                </div>
                    <div class="row">
                        <div class="ipro-form__group ipro-form__group--custom col-xs-6 col-sm-2">
                            <input type="radio" id="Checkbox_109416_Vrai" name="Checkbox_109416" checked class="hidden" value="1" required>
                            <label for="Checkbox_109416_Vrai">Vrai</label>
                        </div>

                        <div class="ipro-form__group ipro-form__group--custom col-xs-6 col-sm-2">
                            <input type="radio" id="Checkbox_109416_Faux" name="Checkbox_109416"  class="hidden" value="0">
                            <label for="Checkbox_109416_Faux">Faux</label>
                        </div>
                    </div>

<!--
            <input type="radio" id="Checkbox_109416_Vrai" name="Checkbox_109416" value="1" required>&nbsp;Vrai&nbsp;
            <input type="radio" id="Checkbox_109416_Faux" name="Checkbox_109416" value="0">&nbsp;Faux&nbsp;

        </div>
        </div>-->

 <!--   <div class="row">
        <div class="col-sm-6">
            <div><?php /*_e("Please enter the following code","app"); */?></div>
            <div id="captchaDiv-1"></div><script type="text/javascript">sjcap('captchaDiv-1','txtCaptcha',null);</script>
        </div>
    </div>-->
    <div class="ipro-form__group ipro-form__group--action text-center">
        <a href="javascript:void(0);" class="btn btn--goldenrod ipro-form__action ipro-form__action--medium">
            <input type="submit" name="submit" id="submit" value="<?php _e("Submit","app"); ?>">
        </a>
    </div>
</form>
</div>