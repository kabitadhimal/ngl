<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      
        <title>NGL</title>
         <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
       

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- #Bootstrap -->

     <!--    <link rel="shortcut icon" href="favicon.ico">        -->
        <!-- Importing google font {Open Sans} -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link rel='shortcut icon' href='<?php echo TEMP_DIR_URI; ?>/favicon1.ico' type='image/x-icon' />
        <!-- /.#Importing google font {Open Sans} -->

        <!-- Contact form and Subscription form -->
        <script type="text/javascript" src="https://secure.inescrm.com/js/Base64.js"></script>
        <script type="text/javascript" src="https://secure.inescrm.com/js/AjaxHelper.js"></script>
        <script type="text/javascript" src="https://secure.inescrm.com/js/form_validation.js"></script>
        <script type="text/javascript" src="https://secure.inescrm.com/js/md5.js"></script>
        <script type="text/javascript" src="https://secure.inescrm.com/js/jcap.js"></script>

        <?php wp_head(); ?>

<style type="text/css">
    #wpadminbar{
            z-index: 100001 !important;
    }
</style>

    </head>

    <body  <?php body_class(); ?>>

        <!-- NGL outer wrapper -->

        <div id="ipro-wrapper" class="ipro-wrapper" role="main">
            <div id="" class="ipro-wrapper__inner">

                <!-- NGL header -->
                <header id="ipro-masthead" class="ipro-header ipro-header--main" role="banner">

                    <!-- NGL top bar -->

                    <div class="ipro-bar ipro-bar--top">
                        <div class="ipro-bar__row">
                            <div class="ipro-container ipro-container--main">

                                <!-- NGL top bar list group row -->

                                <ul class="ipro-bar__list-group">
                                 <?php if(!is_user_logged_in()):
                                   /* ?>
                                    <li class="ipro-bar__item ipro-bar__item--account" data-target="ipro-popup--myaccount">
                                                             
                                        
                                        <a href="<?php echo home_url('create-an-account')?>" class="ipro-bar__link ipro-bar__link--account"><i class="icon icon-my-account"></i><span><?php echo __('Sign Up/Sign In','ngl')?></span></a>
                                        <!-- NGL top bar - My Account Popup -->

                                        <div class="ipro-popup ipro-popup--right ipro-popup--myaccount ipro-arrow--top <?php echo is_user_logged_in()?'logged-in':''?>">
                                            <!-- NGl top bar My Account Popup body -->
                                            <div class="ipro-popup__body">

                                                <!-- NGl top bar My Account Popup col group row -->
                                                <div class="ipro-row  ipro-flex">
                                                   
                                                    <div class="col-sm-6 col-xs-6  ipro-flex__col" data-color-type="heather">
                                                        <div class="ipro-col__content">
                                                            <h5><?php echo __('Login','ngl')?></h5>
                                                            <div class="ipro-myaccount__form-wrap">
                                                                <div class="errorDiv"></div>
                                                                <form id="login_form">
                                                                    <div class="ipro-form__group">
                                                                        <input type="email" value="" name="email" placeholder="<?php echo __('E-mail','ngl')?>" class="ipro-form__control ipro-form__control--round" />
                                                                    </div>
                                                                    <div class="ipro-form__group">
                                                                        <input type="password" value="" name="password" placeholder="<?php echo __('Password','ngl')?>" class="ipro-form__control ipro-form__control--round" />
                                                                    </div>
                                                                    <div class="ipro-form__group ipro-form__group--submit">
                                                                        <button type="submit" class="btn btn--grisfonce2 btn--small ipro-form__control--submit"><?php echo __('Log in','ngl')?></button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <a href="<?php echo home_url('forget-password')?>" class="ipro-link ipro-link--password"><?php echo __('Forgot your password?','ngl')?></a>
                                                        </div>
                                                    </div>
                                              
                                                <?php if(!is_user_logged_in()):?>
                                                     <div class="<?php echo is_user_logged_in()?'col-sm-12 col-xs-12':'col-sm-6 col-xs-6';?> ipro-flex__col" data-color-type="grisfonce2">
                                                        <div class="ipro-col__content">
                                                      
                                                            <h5><?php echo __('First visit?','ngl')?> </h5>
                                                            <div class="ipro-myaccount__desc-wrap">
                                                                <p><?php echo get_field('text','option');?></p>
                                                            </div>
                                                      
                                                        
                                                            <a href="<?php echo home_url('create-an-account')?>" class="btn btn--heather btn--small ipro-form__control--submit"><?php echo  __('Create Account','ngl'); ?></a>
                                                        </div>
                                                    </div> 
                                                <?php endif;?>

                                                </div>
                                                <!-- /.#NGl top bar My Account Popup col group row -->

                                            </div><!-- /.#NGl top bar My Account Popup body -->
                                        </div><!-- /.#NGL top bar - My Account Popup -->
                                    </li>
                                     <?php endif;?>

                                    <?php if(is_user_logged_in()):
                                            global $current_user;
                                            $fullname = $current_user->data->display_name;
                                    ?>
                                      <li class="ipro-bar__item ipro-bar__item--account">
                                         
                                        <a href="<?php echo home_url('my-account')?>" class="ipro-bar__link ipro-bar__link--account"><i class="icon icon-my-account"></i><span><?php echo __('Logged in as','ngl').' '.$fullname?></span></a>
                                      </li>
                                  <?php */ endif;?>



                                    <li class="ipro-bar__item ipro-bar__item--contact">
                                        <a href="<?php echo home_url('/contact')?>" class="ipro-bar__link ipro-bar__link--contact"><i class="icon icon-contact"></i><span><?php echo __('Contact','ngl')?></span></a>
                                    </li>

                                    <?php  $languages = icl_get_languages('skip_missing=1&orderby=KEY&order=DIR&link_empty_to=str');
                                    //debug($languages);
                                    ?>
                                    <li class="ipro-bar__item ipro-bar__item--lang" data-target="ipro-popup--lang">
                                    <?php if(!empty($languages)):
                                                foreach($languages as $language):
                                                    if($language['active']):?>
                                                         <a href="<?php echo $language['url']?>" class="ipro-bar__link ipro-bar__link--lang"><img src="<?php echo $language['country_flag_url']?>" class="ipro-valign--middle shape--circle" alt="<?php echo $language['native_name']?>" /></a>
                                                     <?php endif;
                                                endforeach;
                                    endif;?>
                                        <!-- NGL lang dropdown -->

                                        <div class="ipro-popup ipro-popup--right ipro-popup--lang ipro-arrow--top">
                                            <ul class="ipro-lang__group">
                                             <?php if(!empty($languages)):
                                                foreach($languages as $language):
												  if(!$language['active']):?>
                                                <li class="ipro-lang__item"><a href="<?php echo $language['url']?>"><img src="<?php echo $language['country_flag_url']?>" alt="<?php echo $language['native_name']?>" class="ipro-valign--middle shape--circle" /></a></li>
                                               <?php   endif; endforeach;
                                    endif;?>
                                               
                                            </ul>
                                        </div><!-- /.#NGL lang dropdown -->
                                    </li>
                                </ul>

                                <!-- /.#NGL top bar list group row -->

                            </div><!-- /.# NGL container -->
                        </div><!-- /.# NGL top bar row -->
                    </div><!-- /.#NGL top bar -->

                    <!-- NGL main header -->
                    <div class="ipro-header__row">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL header col wrapper row -->
                            <div class="row ipro-row--fixer clearfix">

                                <!-- NGL header logo col -->
                                <div class="col-sm-2 col-xs-12 ipro-valign--middle ipro-logo--col">
                                    <figure class="ipro-logo__wrapper">
                                        <a href="<?php echo home_url()?>">
                                            <img src="<?php echo TEMP_DIR_URI?>/images/logo.png" alt="" class="ipro-valign--middle" />
                                        </a>
                                    </figure>
                                </div><!-- /.#NGL header logo col -->

                                <!-- NGL main navigation col -->
                                <?php require TEMP_DIR . '/menu.php';?>
                               <!-- /.#NGL navigation logo col -->

                            </div><!-- /.#NGL header col wrapper row -->

                        </div><!-- /.#NGL container -->
                    </div><!-- /.# NGL header row -->
                </header><!-- /.#NGL header -->
                <script>
                    $(function(){
                         $("#login_form").validate({
                            rules: {
                                email: {
                                    required: true,
                                    email: true
                                },
                                'password': {
                                    required: true,                                   
                                }                       
                              
                                
                            },
                           
                            submitHandler: function(form) {
                               
                                $('.errorDiv').text('');                               
                               

                                var input_data = $('#login_form').serialize();
                                var datas = {action: 'user_login', data: input_data};
                                //console.log(datas);
                                ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
                              
                                $.ajax({
                                    url: ajaxurl,
                                    type: "POST",
                                    data: datas,
                                    dataType: "json",
                                    success: function(response) {
                                        //console.log(response);

                                        if (response.status == 'error') {                                           
                                            $('.errorDiv').html(response.msg).show();
                                           
                                            return false;
                                        } else if (response.status == 'success') {

                                            window.location.href = "<?php echo home_url('my-account'); ?>";
                                        } else {

                                            $('.submit').attr('disabled', false);
                                            $('.errorDiv').html('Something went wrong.').show();
                                            return false;
                                        }


                                    }
                                });
                                return false;
                            }
                        });
                    });
                </script>
                 <!-- NGL CMS block wrap -->
                <main id="ipro-main" class="ipro-main" role="main">