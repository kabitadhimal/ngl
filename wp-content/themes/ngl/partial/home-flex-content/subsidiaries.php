<section class="ipro-block ipro-block--map ipro-block--full js-anim-init text-center" data-theme="theme-palma" data-delay="0.15" data-animation="fadein" style="background: url(<?php echo TEMP_DIR_URI ?>/images/map-bkg-patter.png);">
<div class="ipro-block__row">
        <div class="ipro-block--map__row">
            <figure>
                <img src="<?php echo TEMP_DIR_URI ?>/images/map-bkg.png" class="ipro-valign--middle map-image" alt="" />
            </figure>

            <div id="ipro-block--map__marker" class="ipro-block--map__marker"></div>

            <div class="ipro-block--map__caption js-anim-init" data-delay="0.025" data-animation="fadein slideInDown">

                <?php if(!empty($block['title'])): ?>
                    <div id="ipro-block--map__title" class="ipro-block--map__title" data-delay="800">
                        <span class="ipro-title ipro-title--big"><?=$block['title']?></span>
                    </div>
                <?php endif; ?>

            <?php
            $button = $block['button'];
              if(!empty($button['url'])) :
				    if(!empty($button['target'])) { $target = 'target="_blank"'; }
                ?>
              <div class="ipro-block--map__button">
                    <a href="<?=$button['url']?>" class="btn btn--large btn--goldenrod" <?=$target?>><?=$button['title']?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
