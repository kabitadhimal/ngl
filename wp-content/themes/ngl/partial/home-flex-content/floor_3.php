<?php

 $text = $block['text'];
 $icons = $block['icons'];
 //var_dump($icons);
 $iconUrl1 = !empty($icons['icon_1_url']['url'])?$icons['icon_1_url']['url'] :'';
 //echo "Icon url ". $iconUrl1;
 $iconUrl2 =  !empty($icons['icon_2_url']['url']) ?$icons['icon_2_url']['url'] : "";
 $icon1 = $icons['icon_1'];
 $icon2 = $icons['icon_2'];

?>

<section class="section section-promo text-center">
    <div class="container">
		<?php if(!empty($text)): echo $text; endif; ?>
    <div class="icon-holder ipro-flex">
        <a href="<?=$iconUrl1?>">
            <img src="<?=$icon1?>" alt="logo">
        </a>

        <a href="<?=$iconUrl2?>">
            <img src="<?=$icon2?>" alt="logo">
        </a>
    </div>
    </div>
</section>
