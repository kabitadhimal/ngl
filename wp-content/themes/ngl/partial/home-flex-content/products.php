<?php $products = $block['products_with_category'];
    if(!empty($products)):
?>
    <section class="section section-featured-cols">

            <div class="row">
                <?php foreach ($products as $product) : ?>
                    <div class="col-sm-4">
                        <div class="featured-img bg-cover as-ratio-1-1" style="background-image: url('<?=$product['product_image']?>">
                            <h3 class="featured-title"><?=$product['product_title']?></h3>
                            <?php $productCat = $product['product_category']; ?>
                            <div class="featured-overlay">
                                <ul>
                                    <?php  foreach ($productCat as $cat) : ?>
                                        <li><a href="<?=$cat['link']?>"><?=$cat['title']?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

    </section>
<?php endif;