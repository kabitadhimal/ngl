<?php
 $bannerImage = $block['banner_image'];
 $title = $block['title'];
 $text = $block['text'];
 $btn1 = $block['button_1'];
 $btn2 = $block['button_2'];
?>

<section class="section static-banner bg-cover m-t-0" style="background-image: url('<?=$bannerImage?>')">
    <div class="container ">
        <div class="content">
		<?php if(!empty($title)) : ?><h1><?php echo $title; ?></h1> <?php endif; ?>
		<?php if(!empty($text)): ?> <div class="description"><?php echo $text; ?> <?php endif; ?>
        </div>
    </div>

        <div class="buttons-wrap">
            <?php
            $btn1= $block['button_1'];
            $image1 = $block['image_1'];
            $btn2 = $block['button_2'];
            $image2 = $block['image_2'];
            ?>
            <?php if(!empty($btn1['url'])) :
                if(!empty($btn1['target'])) { $target1 = 'target="_blank"'; }
                ?>
                <div class="button-list">
                    <?php if(!empty($btn1['url'])) : ?><a href="<?=$btn1['url']?>" class="btn btn--stromGrey btn--large" <?=$target1?>> <?php endif; ?>
                            <?php if(!empty($image1)): ?>
                            <figure>
                                <img src="<?php echo $image1; ?>" alt="image_1">
                            </figure>
                            <?php endif; ?>
                            <?=$btn1['title']?>
                        <?php if(!empty($btn1['url'])) : ?> </a> <?php endif; ?>
                </div>
            <?php endif ; ?>

            <?php if(!empty($btn2['url'])) :
                if(!empty($btn2['target'])) { $target2 = 'target="_blank"'; }
                ?>
                <div class="button-list">
                    <?php if(!empty($btn2['url'])) : ?><a href="<?=$btn2['url']?>"  class="btn btn--stromGrey btn--large" <?=$target2?>> <?php endif; ?>
                        <?php if(!empty($image2)): ?>
                        <figure>
                            <img src="<?php echo $image2; ?>" alt="image_2">
                        </figure>
                        <?php endif; ?>
                        <?=$btn2['title']?>
                        <?php if(!empty($btn2['url'])) : ?> </a> <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>