<?php
	$products = $block['products'];
	$title = $block['title']; 
	if(!empty($products)):
?>
<section class="section section-three-col text-center">
    <div class="container">
        <?php if(!empty($title)): ?><h2 class="title"><?=$title?></h2> <?php endif; ?>
        <div class="ipro-flex ">
		<?php foreach($products as $product): 
				$icon = $product['icon'];
				$title = $product['title'];
				$text = $product['text'];
		?>
            <div class="col-sm-4">
                <?php if(!empty($icon)): ?> <img class="img" src="<?=$icon?>" alt="icon"> <?php endif; ?>
                <?php if(!empty($title)): ?><h3 class="heading"><?=$title?></h3> <?php endif; ?>
				<?php if(!empty($text)): echo $text; endif; ?>
            </div>
		<?php endforeach; ?>
        </div>
		<?php $button = $block['products_button'];?>
			<?php if(!empty($button['url'])) :
					if(!empty($button['target'])) { $target = 'target="_blank"'; }
			?>
				<a href="<?=$button['url']?>" class="btn btn--stromGrey btn--large" <?=$target?>><?=$button['title']?></a>
				<?php endif; ?>
    </div>
</section>
<?php endif; 