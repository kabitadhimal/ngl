<section class="section section-news">
    <div class="container">
        <?php if(!empty($block['title'])) : ?>  <h2 class="title text-center"><?=$block['title']?></h2><?php endif; ?>
        <?php
        $newsList = $block['select_news'];
        $query = new WP_Query( array( 'post_type' => 'post', 'post__in' => $newsList ) );
        ?>

        <div class="ipro-flex row">
            <?php
            while($query->have_posts()): $query->the_post();
                // echo 'post id is '.$query->post->ID;
                ?>
               <!-- <div class="col-sm-4">
                    <div class="news-post">
                        <?php /* $image_url = wp_get_attachment_url(get_post_thumbnail_id($query->post->ID));*/?>
                        <img class="img" src="<?/*=$image_url*/?>" alt="news-image">
                        <div class="content">
                            <div class="news-date"><a href="<?php /*the_permalink(); */?>"><?php /*_e("News","app"); */?>- <?php /* the_date('d.m.Y'); */?></a></div>
                            <?php /*the_title('<h3>','</h3>'); */?>
                            <p> <?php /*echo $content = wp_trim_words(get_the_content(), 10, $more = '… '); */?></p>
                            <div class="btn-wrap text-center m-t-15">
                                <a href="<?php /*the_permalink(); */?>" class="btn btn--stromGrey btn--large"><?php /*_e("En savoir plus","app"); */?></a>
                            </div>
                        </div>
                    </div>
                </div>-->


                <article class="col-sm-4 col-xs-12 ipro-newsEvents__col">
                    <div>
                        <figure>
                            <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($id));?>
                            <img src="<?php echo $image_url?>" alt="" class="ipro-valign--middle" />

                            <a href="<?php echo the_permalink();?>" class="ipro-link ipro-link--overlay"></a>
                        </figure>
                        <div class="ipro-newsEvents__col-body">
                            <div class="ipro-newsEvents__meta">
                                <span class="ipro-newsEvents__cat"><span class="ipro-newsEvents__date"><?php echo get_the_date('d.m.Y',$id)?></span>
                            </div>
                            <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title()?></a></h3>
                            <p> <?php echo $content = wp_trim_words(get_the_content(), 10, $more = '… '); ?></p>
                            <?php if(!empty($category) && in_array($category,$news_slug_arr)):?>
                                <a href="<?php the_permalink();?>" class="btn btn--goldenrod btn--small btn--small-custom"><?php echo __('Read more','ngl'); ?></a>
                            <?php elseif(!empty($category) && in_array($category,$events_slug_arr)): ?>
                                <a href="<?php the_permalink();?>" class="btn btn--stromGrey btn--small btn--small-custom"><?php echo __('Meet us','ngl')?></a>
                            <?php else: ?>
                                <a href="<?php the_permalink();?>" class="btn btn--stromGrey btn--small btn--small-custom"><?php echo __('Read more','ngl')?></a>
                            <?php endif;?>


                        </div>
                    </div>
                </article>
            <?php endwhile; ?>

        </div>

        <?php
         $readMoreButton = $block['read_more_button'];
         $target="";
          if(!empty($readMoreButton['url'])):
              if(!empty($button['target'])) { $target = 'target="_blank"'; }
              ?>
            <div class="button-wrap text-center">
                <a href="<?=$readMoreButton['url']?>" class="btn btn--large btn--large-custom btn--goldenrod" <?=$target?>><?=$readMoreButton['title']?></a>
            </div>
          <?php endif; ?>
    </div>
</section>