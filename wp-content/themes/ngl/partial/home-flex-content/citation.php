<section class="ipro-block ipro-block--twoHalf ipro-block--full  section-testimonial">
    <div class="ipro-block__row">

        <div class="clearfix ipro-row ipro-flex">

            <?php $citationLeft = $block['left_citation'];
                if(!empty($citationLeft)):
            ?>
            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-block__gap--largecms ipro-block--text text-left bg-white">
                <div class="ipro-col__content">
                    <?php if(!empty($citationLeft['image'])) : ?>
                    <figure>

                        <?php
                        $link = $citationLeft['image_link'];

                        if(!empty($link)) {
                            ?>
                            <a href="<?=$link?>"><img src="<?=$citationLeft['image']?>" class="ipro-valign--middle" alt="logo"></a>
                        <?php
                        }else {
                            ?>
                            <img src="<?=$citationLeft['image']?>" class="ipro-valign--middle" alt="logo">

                        <?php } ?>


                    </figure>
                    <?php endif; ?>

                    <?php if(!empty($citationLeft['citation'])): ?>
                    <div class="quote">
                        <?=$citationLeft['citation']?>
                    </div>
                    <?php endif; ?>
                    <?php if(!empty($citationLeft['citation_by'])): ?>
                         <div class="name"><?=$citationLeft['citation_by']?></div>
                    <?php endif; ?>
                    <?php if(!empty($citationLeft['position'])): ?>
                            <div class="post">
                               <?=$citationLeft['position']?>
                            </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

        <?php $citationRight = $block['right_citation'];
            if(!empty($citationRight)):
        ?>
            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-block__gap--largecms ipro-block--text text-left bg-gray">
                <div class="ipro-col__content text-right">
                    <?php if(!empty($citationRight['image'])) : ?>
                        <figure>
                            <?php
                            $link = $citationLeft['image_link'];

                            if(!empty($link)) {
                                ?>
                                <a href="<?=$link?>"><img src="<?=$citationRight['image']?>" class="ipro-valign--middle" alt="logo"></a>
                                <?php
                            }else {
                                ?>
                                <img src="<?=$citationRight['image']?>" class="ipro-valign--middle" alt="logo">

                            <?php } ?>
                        </figure>
                    <?php endif; ?>

                    <?php if(!empty($citationRight['citation'])): ?>
                        <div class="quote">
                            <?=$citationRight['citation']?>
                        </div>
                    <?php endif; ?>

                    <?php if(!empty($citationRight['citation_by'])): ?>
                        <div class="name"><?=$citationRight['citation_by']?></div>
                    <?php endif; ?>
                    <?php if(!empty($citationRight['position'])): ?>
                        <div class="post">
                            <?=$citationRight['position']?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>

