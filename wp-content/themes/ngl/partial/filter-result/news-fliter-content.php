<?php
$args = ['post_type' => "post",'posts_per_page'=>-1,  'suppress_filters'  =>  false, 'post_status'=>'publish'];
if( !empty($_GET['cat'])&&  $_GET['cat']!="all") {
        $args['cat'] = $_GET['cat'];
}

if(!empty($_GET['date'])) :
    $termsArray = explode(', ',$_GET['cat']);
    $args['date_query'] = [
        [
            'year' => $date
        ]
    ];
endif;

$query = new WP_Query($args);
if ($query->have_posts()) :?>
    <?php
    $ind=0;

    $events_slug_arr = array('events-zh-hans','events-de','events-it','events-es','events','events-2');
    $news_slug_arr = array('news','news-2','news-zh-hans','news-de','news-it','news-es');
    //$steller_arr = array('1.5','1.8');

    while ($query->have_posts()) : $query->the_post();

        $status = get_field('status');
        $is_locked = get_field('is_locked');
        if(is_user_logged_in()){
            $is_locked= false;
        }
        $id=get_the_ID();
        $categories = get_the_category( $id );
        ///var_dump($categories);
        $catSlug ="";
        foreach ($categories as $cat ) {
            $catSlug = $cat->slug." ";
            //  echo $catSlug.'<br/>';
        }
        //   echo "categeory slug ".$catSlug;

        $type='';
        $category='';
        $categoryName='';
        $filterPost='';
        $filterStatus='';
        /*    if(!empty($categories)){
                $category = $categories[0]->slug;

                 if(in_array($category,$events_slug_arr)){
                    $type='ipro-newsEvents__col--isevent';
                    $categoryName = 'Events';
                    $filterPost='events';
               }elseif(in_array($category,$news_slug_arr)){
                     $type='ipro-newsEvents__col--isnews';
                     $categoryName = 'News';
                     $filterPost='news';
                }
            }*/


        if($status=='new'):
            $status_type='newProduct';
            $statusClass='ipro-newsEvents__col-isnewProduct';

        elseif($status=='new_product'):
            $status_type='recentProduct';
            $statusClass='ipro-newsEvents__col-isrecent';
            $filterStatus='new-product';

        else:
            $status_type='';
            $statusClass='';

        endif;
        ?>

        <article class="col-sm-4 col-xs-12 ipro-newsEvents__col <?php echo $is_locked==1?'ipro-newsEvents__col--isprotected':''?> <?php echo $type?> <?php echo $statusClass?> <?php echo $filterPost?> <?php echo $filterStatus?> <?=$catSlug?> date-<?php echo get_the_date("Y",$id); ?> all" data-order="<?php echo ++$ind?>">
            <div>
                <figure>
                    <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($id));?>
                    <img src="<?php echo $image_url?>" alt="" class="ipro-valign--middle" />
                    <?php if(!empty($status_type)): ?>

                        <div class="ribbon ribbon-abs ribbon-abs--topRight ribbon-<?php echo $status_type?>">
                            <div class="ribbon__inner">
                                <?php if(!empty($status) && $status=='new_product'):?>
                                    <span>New <br/>product</span>
                                <?php elseif(!empty($status) && $status=='new'): ?>
                                    <span>New</span>
                                <?php  endif; ?>
                            </div>
                        </div>
                    <?php endif;?>
                    <a href="<?php echo the_permalink();?>" class="ipro-link ipro-link--overlay"></a>
                </figure>
                <div class="ipro-newsEvents__col-body">
                    <div class="ipro-newsEvents__meta">
                        <span class="ipro-newsEvents__cat"><?php echo $categoryName?></span> - <span class="ipro-newsEvents__date"><?php echo get_the_date('d.m.Y',$id)?></span>
                    </div>
                    <h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title()?></a></h3>
                    <p> <?php echo $content = wp_trim_words(get_the_content(), 10, $more = '… '); ?></p>
                    <?php if(!empty($category) && in_array($category,$news_slug_arr)):?>
                        <a href="<?php the_permalink();?>" class="btn btn--goldenrod btn--small btn--small-custom"><?php echo __('Read more','ngl'); ?></a>
                    <?php elseif(!empty($category) && in_array($category,$events_slug_arr)): ?>
                        <a href="<?php the_permalink();?>" class="btn btn--stromGrey btn--small btn--small-custom"><?php echo __('Meet us','ngl')?></a>
                    <?php else: ?>
                        <a href="<?php the_permalink();?>" class="btn btn--stromGrey btn--small btn--small-custom"><?php echo __('Read more','ngl')?></a>
                    <?php endif;?>


                </div>
                <?php if($is_locked):?>
                    <i class="icon icon-abs icon-abs--mid icon-lock"></i>
                <?php endif;?>
            </div>
        </article>
    <?php endwhile; endif;?>