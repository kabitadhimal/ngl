<?php 

$subject = 'Account delete';
$user = wp_get_current_user();
$email = $user->data->user_email;
$display_name = $user->data->display_name;
 $language = get_user_meta($user_id, 'language',true);
$parse_array = array(       
            '{SITE_URL}' =>SITEURL,
            '{HOMEURL}' => HOMEURL,
            '{TEMP_DIR_URI}' => TEMP_DIR_URI,
            '{CONTACT_US_LINK}' => CONTACT_URL,
            '{LINKEDIN_LINK}' => LINKEDIN_URL,
            '{NEWSLETTER_URL}' => NEWSLETTER_URL,
            '{DEMAND_URL}' => DEMAND_URL,
            '{CURRENT_YEAR}' => CURRENT_YEAR,                
            '{FROM_EMAIL}' => get_option('admin_email'),
            '{MEMBER_NAME}' => ucfirst($user->display_name),
            '{MEMBER_EMAIL}' => $user->user_email                        
                       
         );                       
if($language=='fr'){
     $email_template = build_email_template($parse_array, 'account_deletion','fr');
}else{
     $email_template = build_email_template($parse_array, 'account_deletion');
}                
    
$to = get_option('admin_email');
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
mail($to, $subject, $email_template,$headers);
wp_delete_user( $user_id ); 
                        ?>