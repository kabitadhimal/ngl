  <!-- NGL shortcode Lightbox video -->
                    <section id="ipro-block-video" class="ipro-block ipro-block--videoLightbox js-anim-init" data-animation="fadeIn slideInUp">
                        <div class="ipro-block__row">
                            
                             <!-- NGL video play icon with light box reference -->
                             <div class="ipro-lightbox ipro-lightbox--abs ipro-lightbox--vcenter">
                                <a href="<?php echo $block['video_url']?>" class="ipro-lightbox-play"><i class="ipro-picto ipro-picto--large ipro-picto--videoPlay"></i></a>
                            </div><!-- /.#NGL video play icon with light box reference -->

                            <!-- NGL full screen video cover image -->
                            <img src="<?php echo $block['background_image']['url']?>" class="ipro-valign--middle" alt="" />
                            <!-- NGL full screen video cover image -->
                            
                        </div><!-- /.#ipro Block row -->
                    </section><!-- /.#NGL shortcode Lightbox video -->