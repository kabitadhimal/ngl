<!-- NGL CMS blocks :: Shortcodes twohalf - Years of experience -->
<section class="ipro-block ipro-block--twoHalf ipro-block--full js-anim-init"  data-theme="theme-palma-stormgrey" data-background="true" data-animation="fadein">
    <div class="ipro-block__row">

        <div class="clearfix ipro-row ipro-flex">

            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block--bkg ipro-block__gap--medium text-left js-anim-init" data-background="true" data-delay="0.25" data-animation="fadein slideInDown" style="background-image: url(<?php echo $block['left_background_image']['url']?>);">
            </div>

            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block--isoverlay ipro-block__gap--medium text-left js-anim-init" data-background="true" data-animation="fadein slideInDown" style="background-image: url(<?php echo $block['right_background_image']['url']?>);">
                <div class="ipro-col__content js-anim-init" data-delay="1" data-animation="fadein slideInDown">
                    <h2><?php echo $block['title']?></h2>short_description
                    <p><?php echo $block['short_description']?></p>
                    <a href="<?php echo $block['link_']?>" class="ipro-link ipro-link--underline ipro-link--palma"><?php echo __('Read more','ngl')?></a>
                </div>
            </div>
        </div><!-- /.# NGL tow half grid wrap -->

    </div><!-- /.# NGL container -->
</section><!-- /.#NGL CMS blocks :: Shortcodes twohalf - Years of experience -->
 <!-- NGL CMS blocks :: Shortcodes two half - quality services with logo -->
    <section class="ipro-block ipro-block--twoHalf ipro-block--full ipro-block--logo js-anim-init" data-theme="theme-default" data-background="true" data-animation="fadein" style="background-image: url(<?php echo TEMP_DIR_URI ?>/images/qService-bkg.jpg);">
        <div class="ipro-block__row">

            <div class="clearfix ipro-row ipro-flex">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--small text-right">
                    <div class="ipro-col__content">
                        <ul class="ipro-block__list-group">
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">Cleaning processes<br/><span>& chemicals</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">Water recycling<br/><span>processes & chemicals</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">decoating <br/> <span>technology</span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--small text-left">
                    <div class="ipro-col__content">
                        <ul class="ipro-block__list-group">
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">ultrasonics<br/><span>& spray chemicals</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">Water-based<br/><span>surface preparation</span></li>
                            <li class="js-anim-init" data-animation="fadein slideInDown" data-delay="0.05">ecological <br/> <span>cleaning solutions</span></li>
                        </ul>
                    </div>
                </div>

            </div><!-- /.# NGL tow half grid wrap -->

            <!-- /.#NGL CMS blocks :: two half - logo -->
            <div class="ipro-block__logo js-anim-init" data-animation="fadein">
                <a href=""><img src="<?php echo TEMP_DIR_URI ?>/images/swiss-qty-logo.png" alt="" class="ipro-valign--middle" /></a>
            </div><!-- /.#NGL CMS blocks :: two half - logo -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: two half - quality services with logo -->