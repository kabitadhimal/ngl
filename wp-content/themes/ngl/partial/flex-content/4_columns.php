 <!-- NGL block 2/3 cols grid layout -->
<?php 

if($block['select_background_color']=='white'):
      $color='white';
  elseif($block['select_background_color']=='heather'):
      $color='heather';
  else:
      $color='grey';
endif;?>

                                                <section class="ipro-block ipro-block--gridLayout ipro-block--<?php echo $color;?>     ipro-block--gridLayout-<?php echo $block['select_column']==2?'oneHalf':'oneThird'?>">
                                                    <div class="ipro-block__row">
                                                        <div class="ipro-container ipro-container--main">

                                                            <!-- Note :: This block may contain 2 or 3 col as per client's requirement so we need to add col-sm-4 and col-sm-6 class accordingly -->

                                                            <!-- Grid row -->
                                                            <div class="clearfix ipro-flex ipro-flex--alignItemsStart ipro-flex--gutter32">

                                                              <article class="col-sm-3 col-xs-12 ipro-flex__col ipro-postCol js-anim-init" data-delay="0.05" data-animation="fadeIn slideInDown">
                                                                    <div class="ipro-col__content">

                                                                        <!-- Grid col - image -->
                                                                        <figure>
                                                                            <img src="<?php echo $block['1st_column_image']['url']?>" class="ipro__media" alt="" />        
                                                                        </figure><!-- /.#Grid col - image -->
                                                                        <div class="ipro-postCol__body">
                                                                            <h3><?php echo $block['1st_column_title']?></h3>
                                                                           <?php echo $block['1st_column_description']?>
                                                                        </div>

                                                                    </div>
                                                                </article>
                                                                <article class="col-sm-3 col-xs-12 ipro-flex__col ipro-postCol js-anim-init" data-delay="0.05" data-animation="fadeIn slideInDown">
                                                                    <div class="ipro-col__content">

                                                                        <!-- Grid col - image -->
                                                                        <figure>
                                                                            <img src="<?php echo $block['2nd_column_image']['url']?>" class="ipro__media" alt="" />        
                                                                        </figure><!-- /.#Grid col - image -->
                                                                        <div class="ipro-postCol__body">
                                                                            <h3><?php echo $block['2nd_column_title']?></h3>
                                                                           <?php echo $block['2nd_column_description']?>
                                                                        </div>

                                                                    </div>
                                                                </article>
                                                                <article class="col-sm-3 col-xs-12 ipro-flex__col ipro-postCol js-anim-init" data-delay="0.05" data-animation="fadeIn slideInDown">
                                                                    <div class="ipro-col__content">

                                                                        <!-- Grid col - image -->
                                                                        <figure>
                                                                            <img src="<?php echo $block['3rd_column_image']['url']?>" class="ipro__media" alt="" />        
                                                                        </figure><!-- /.#Grid col - image -->
                                                                        <div class="ipro-postCol__body">
                                                                            <h3><?php echo $block['3rd_column_title']?></h3>
                                                                           <?php echo $block['3rd_column_description']?>
                                                                        </div>

                                                                    </div>
                                                                </article>
                                                                <article class="col-sm-3 col-xs-12 ipro-flex__col ipro-postCol js-anim-init" data-delay="0.05" data-animation="fadeIn slideInDown">
                                                                    <div class="ipro-col__content">

                                                                        <!-- Grid col - image -->
                                                                        <figure>
                                                                            <img src="<?php echo $block['4th_column_image']['url']?>" class="ipro__media" alt="" />        
                                                                        </figure><!-- /.#Grid col - image -->
                                                                        <div class="ipro-postCol__body">
                                                                            <h3><?php echo $block['4th_column_title']?></h3>
                                                                           <?php echo $block['4th_column_description']?>
                                                                        </div>

                                                                    </div>
                                                                </article>
                    

                                                          

                                                              

                                                            </div>
                                                            <!-- /.#Grid row -->

                                                        </div><!-- /.#NGL CMS block container -->
                                                    </div><!-- /.# NGL block row -->
                                                </section><!-- /.#NGL block 2/3 cols grid layout -->



                                                 