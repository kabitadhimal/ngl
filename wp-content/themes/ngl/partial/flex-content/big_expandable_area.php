 <section class="ipro-block ipro-block--accordion ipro-block--accordionBig">
    <div class="ipro-container ipro-container--full">

        <!-- NGL accordion UI -->
        <div class="ipro-accordion ipro-accordion--custom js-anim-init" data-delay="0.25" data-animation="fadeIn" data-scroll-off="true">
            <?php if(!empty($block['expandable_area'])):
            foreach($block['expandable_area'] as $ind=>$expandable_area):
                ?>
             <?php //echo $ind=='0'?'panel-col-active':''?>
            <div class="ipro-accordion__col ipro-accordion__col--vAlign">
                <div class="ipro-accordion__panel">
                    <?php //echo $ind=='0'?'--active':''?>
                    <div class="ipro-accordion__panelBody ipro-accordion__targetPanel">
                        <div class="ipro-accordion__content">
                            <div class="ipro-block__text ipro-block__text--light ipro-container--text text-center">
                                <h2><?php echo $expandable_area['title']?></h2>
                                 <?php echo $expandable_area['description']?>
                            </div>
                        </div>
                    </div>

                    <div class="ipro-accordion__panel-heading">
                        <?php //echo $ind=='0'?'closer':'opener'?>
                        <h2><a href="" class="opener toggler" data-collapse="ipro-accordion__targetPanel"><span><?php echo $expandable_area['title']?></span><i class="ipro__chevron ipro__chevron--down ipro__chevron--palma"></i></a></h2>
                    </div>
                </div>
            </div>

        <?php endforeach;
        endif;?>
            
        </div><!-- /.#NGL accordion UI -->

    </div><!-- /.#NGL CMS block container -->
</section>