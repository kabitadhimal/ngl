 <!-- NGL news details title banner -->
<section class="ipro-block ipro-block--simpleTitle <?php echo $block['background_color']=='grey'?'ipro-block--grey':'ipro-block--white'?>">
    <div class="ipro-container ipro-container--main">

        <!-- NGL news details banner title -->
        <div class="ipro-block__title text-center">
            <h2><?php echo $block['title']?></h2>
        </div><!-- /.#NGL news details banner title block -->

    </div><!-- /.# NGL main container -->
</section><!-- /.#NGL news details title banner -->