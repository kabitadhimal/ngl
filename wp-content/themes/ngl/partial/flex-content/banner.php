   <section class="ipro-block ipro-block--heroBanner ipro-block--heroBanner-lg text-center" data-parallax="true" style="background-image: url('<?php echo $block['background_image']['url'] ?>');">
    <div class="ipro-block__row">
        <!-- NGL hero banner caption -->
        <div class="ipro-block__caption ipro-block__caption--hero">
            <div class="ipro-block__content js-anim-init" data-delay="0.5" data-animation="fadeIn slideInDown">
                <h1 style="color:<?php echo $block['title_color']?>"><?php echo $block['title'] ?></h1>
                <p class="ipro__para ipro__para--big"><?php echo $block['description'] ?></p>
            </div>
        </div><!-- /.#NGL hero banner caption -->
    </div><!-- /.#NGL CMS block row -->
</section><!-- /.#NGL CMS block :: Hero banner -->

                     