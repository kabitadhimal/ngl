 <section class="ipro-block ipro-block--twoHalf ipro-block--full <?php echo $block['invert_blocks']==1?'ipro-block--invert ':''?> js-anim-init" data-theme="theme-<?php echo $block['need_blue_theme']==1?'darkGoldenrod':'palma-stormgrey'?>" data-background="false" data-animation="fadein">
    <div class="ipro-block__row">

        <div class="clearfix ipro-row ipro-flex">                          
        

 <?php  if($block['is_background_image']): ?>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--bkg js-anim-init fadeIn animated" data-background="true" data-delay="0.05" data-animation="fadeIn" style="background-image:url(<?php echo $block['background_image']['url']?>);"></div>
 <?php else:
?>
  
  <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--largecms ipro-block--img text-center">
                <div class="ipro-col__content js-anim-init" data-animation="fadein slideInDown">
                    <figure>
                        <img src="<?php echo $block['image']['url']?>" class="ipro-valign--middle" alt="" />
                    </figure>
                </div>
            </div>

  <?php endif;?>





            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--largecms ipro-block--text text-left">
                <div class="ipro-col__content js-anim-init" data-delay="0.05" data-animation="fadein <?php echo $block['invert_blocks']==1?'slideInLeft ':'slideInRight'?>">
                    <h2><?php echo $block['title']?></h2>
                    <?php echo $block['description']?>
                   
                </div>
            </div>

        </div><!-- /.# NGL tow half grid wrap -->

    </div><!-- /.# NGL container -->
</section>