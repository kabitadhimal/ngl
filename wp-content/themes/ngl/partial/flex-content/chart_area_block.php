<div class="content-block gray-style graph-block" style="background: url(<?php echo $block['background_image']['url']?>)">

    <div class="container">
        <h2><?php echo $block['chart_title'] ?><time datetime="2015-12-31" class="text-heading3"><?php echo $block['update_date'] ?></time></h2>
        <ul class="performance-list">
        <?php if(!empty($block['results'])):
        			foreach($block['results'] as $result):?>
            <li><p><strong><?php echo $result['result_number']?></strong><?php echo $result['text_explication_of_number']?></p></li>
        <?php 	endforeach;
      	  endif;?>
       	     
        </ul>
    </div>
</div>
