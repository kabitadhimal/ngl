<section class="ipro-block ipro-block--videoEmbedded js-anim-init" data-animation="fadeIn slideInUp">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL youtube / Vimeo embedded video row -->
                            <div class="ipro__wrapper ipro__wrapper--video ipro__wrapper--responsive">
                                <iframe class="ipro__video ipro__media ipro__media--responsive" data-src="<?php echo $block['link']?>" frameborder="0" allowfullscreen></iframe>
                            </div><!-- /.#NGL youtube / Vimeo embedded video row -->

                        </div><!-- /.#NGL CMS block container -->
                    </section>