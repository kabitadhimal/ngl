 <!-- NGL CMS blocks :: Shortcodes two half - Image/Text with large gap -->
                    
<section class="ipro-block ipro-block--twoHalf ipro-block--twoHalfSmall <?php echo $block['invert_blocks']==1?'ipro-block--invert ':''?>ipro-block--grey js-anim-init" data-theme="theme-palma-stormgrey" data-background="false" data-animation="fadein">
    <div class="ipro-block__row">

        <!-- /.NGL container -->
        <div class="ipro-container ipro-container--main">
            <div class="clearfix ipro-row ipro-flex ipro-flex--gutter34">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--tiny ipro-block--text text-left">
                    <div class="ipro-col__content js-anim-init" data-delay="0.05" data-animation="fadein <?php echo $block['invert_blocks']==1?'slideInRight ':'slideInLeft'?>">
                        <h2><?php echo $block['title']?></h2>
                        <?php echo $block['description']?>
                       <!--  <a href="" class="ipro-link ipro-link--underline ipro-link__gap--large ipro-link--darkGoldenrod">En savoir plus</a> -->
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--tiny ipro-block--img js-anim-init" data-delay="0.05" data-animation="fadeIn">
                    <figure class="ipro-col__content">
                        <img src="<?php echo $block['image']['url']?>" alt="Image alternate title" />
                    </figure>
                </div>

            </div><!-- /.#NGL row half grid wrap -->
        </div><!-- /.#NGL container -->

    </div><!-- /.# NGL container -->
</section><!-- /.#NGL CMS blocks :: Shortcodes two half - Image/Text with large gap -->                   
                   