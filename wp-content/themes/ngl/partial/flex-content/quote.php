  <!-- NGL CMS block :: Full width Quote -->
                    <section class="ipro-block ipro-block--full ipro-block--goldenrod ipro-block--quote text-center js-anim-init" data-delay="0.5" data-animation="fadeIn" data-theme="theme-default">
                        <div class="ipro-block__row">
                            <div class="ipro-container ipro-container--main">

                                <div class="ipro-block__text js-anim-init" data-delay="0.5" data-animation="fadeIn slideInDown">
                                    <h2><?php echo $block['title']?></h2>
                                    <h5><em><?php echo $block['text']?></em></h5>
                                </div>

                            </div><!-- /.#NGL CMS block container -->
                        </div><!-- /.#NGL CMS block row -->
                    </section><!-- /.#NGL CMS block :: Full width Quote -->