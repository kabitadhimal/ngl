    <!-- NGL CMS blocks :: Shortcodes News / Events -->
    <section id="ipro-latestEvent-block" class="ipro-block ipro-block--newsEvents text-center" data-theme="theme-default" data-speed="0.5" data-delay="0.2" style="background-image: url(images/hp-news-bkg.png);">
        <div class="ipro-container ipro-container--large">

            <header class="ipro-block__header ipro-block__header--small">
                <h2>News / Events</h2>
                <a href="" class="ipro-link ipro-link--default ipro-link--underline">Voir plus d’actualités</a>
            </header><!-- /.# NGL CMS Components :: section header -->

            <div class="ipro-block__content ipro-block__gutter ipro-block__gutter--small">
                <div class="row ipro-row ipro-flex clearfix">

			<?php if(!empty($block['select_newsevents'])):
                        //debug($block['select_newsevents']);
					foreach($block['select_newsevents'] as $ind=>$news):
                        $status = get_field('status',$news->ID);
                        $is_locked = get_field('is_locked',$news->ID);
                        $categories = get_the_category( $news->ID );
                        $type='';
                        $category='';
                        $categoryName='';
                        if(!empty($categories)){
                            $category = $categories[0]->slug;
                            if($category=='events'){
                                $type='ipro-newsEvents__col--isevent';
                                $categoryName = 'Events';
                            }elseif($category=='news'){
                                 $type='ipro-newsEvents__col--isnews';
                                 $categoryName = 'News';
                            }
                        }
                        
                        
                        if($status=='new'):
                            $status_type='newProduct';
                            $statusClass='ipro-newsEvents__col-isnewProduct';  

                        elseif($status=='new_product'):
                            $status_type='recentProduct';
                            $statusClass='ipro-newsEvents__col-isrecent'; 
                            
                        else:
                            $status_type='';
                            $statusClass='';

                        endif;
                    
                        ?>
	                    <article class="col-sm-4 col-xs-12 ipro-newsEvents__col <?php echo $is_locked==1?'ipro-newsEvents__col--isprotected':''?> <?php echo $type?> ipro-newsEvents__col--isstrormGray <?php echo $statusClass?>" data-order="<?php echo ++$ind?>" data-stellar-ratio="1.35" data-stellar-horizontal-offset="0">
	                        <div class="js-sprite-animatex js-anim-init" data-delay="0.035" data-animation="fadeIn">
	                            <figure>
                                <?php  $image_url = wp_get_attachment_url(get_post_thumbnail_id($news->ID));?>
	                                <img src="<?php echo $image_url?>" alt="" class="ipro-valign--middle" />
                                    <?php if(!empty($status_type)): ?>

                                            <div class="ribbon ribbon-abs ribbon-abs--topRight ribbon-<?php echo $status_type?>">
                                                <div class="ribbon__inner">

                                                <?php if(!empty($status) && $status=='new_product'):?>
                                                    <span>New <br/>product</span>
                                                <?php elseif(!empty($status) && $status=='new'): ?>
                                                     <span>New</span>
                                                <?php  endif; ?>
                                                </div>
                                            </div>
                                <?php endif;?>

	                            </figure>
	                            <div class="ipro-newsEvents__col-body">
	                                <div class="ipro-newsEvents__meta">
	                                    <span class="ipro-newsEvents__cat"><?php echo $categoryName?></span> - <span class="ipro-newsEvents__date"><?php echo date('d.m.Y',strtotime($news->post_date))?></span>
	                                </div>
	                                <h3><?php echo $news->post_title?></h3>
                                   <p> <?php echo $content = wp_trim_words($news->post_content, 17, $more = '… '); ?></p>
                                    <?php if(!empty($category) && $category=='news'):?>
                                     <a href="<?php echo $news->guid;?>" class="ipro-link ipro-link--italic ipro-link--underline ipro-link--springBud">Lire la suite</a>
                                 <?php else: ?>
	                                <a href="<?php echo $news->guid;?>" class="btn btn--stromGrey btn--large btn--large-custom">Nous rencontrer</a>
                                <?php endif;?>
	                            </div>
                                <?php if($is_locked):?>
	                            <i class="icon icon-abs icon-abs--mid icon-lock"></i>
                            <?php endif;?>
	                        </div>
	                    </article>

                <?php endforeach;
                endif;?>


                </div><!-- /.# NGL CMS Components :: news/events post entry row -->
            </div><!-- /.# NGL CMS Components :: section post content -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: Shortcodes News / Events -->