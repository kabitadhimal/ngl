  <!-- NGL CMS blocks :: Home page kwirks effect box section -->
  <?php if(!empty($block['blocks'])):?>
    <section class="ipro-block ipro-block--kwirksBox ipro-block--full js-anim-init" data-theme="theme-default" data-animation="fadein">
        <div class="ipro-block__row">

            <div class="ipro-row clearfix ipro-block--kwirksBox__row">
            	<?php if(!empty($block['blocks'][0]['acf_fc_layout']) && $block['blocks'][0]['acf_fc_layout']=='left_block'):?>
	
                <div class="col-sm-4 col-xs-12 ipro-block__col ipro-block__col--noPadding kwirks-expand" data-box="kwirks-box" data-order="1">
                    <div class="kwirks-box__row clearfix">

                        <div class="kwirks-box__hidden kwirks-box__hidden--list">
                            <div class="kwirks-box__hidden-content clearfix">
                                <div class="col-sm-6 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                    <?php if(!empty($block['blocks'][0]['1st_column'])):
                                    			foreach($block['blocks'][0]['1st_column'] as $product):
                                    		?>
                                       			 <li><a href=""><?php echo $product['text']?></a></li>
                                    <?php 		endforeach;
                                    	endif;?>
                                        
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                    <?php if(!empty($block['blocks'][0]['2nd_column'])):
                                    			foreach($block['blocks'][0]['2nd_column'] as $product):
                                    		?>
                                       			 <li><a href=""><?php echo $product['text']?></a></li>
                                    <?php 		endforeach;
                                    	endif;?>
                                      
                                      
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="kwirks-box__inner">
                            <div class="ipro-col__content js-anim-init" data-animation="fadein">
                                <figure>
                                    <img src="<?php echo $block['blocks'][0]['image']['url']?>" class="ipro-valign--middle ipro-media ipro-media--full" alt="" />
                                </figure>
                                <div class="ipro-block__col-caption js-anim-init" data-animation="fadein slideInDownx">
                                    <h2>
                                        <span><?php echo $block['blocks'][0]['title']?></span>
                                        <i class="ipro-chevron ipro-chevron--right visible-xs"></i>
                                    </h2>
                                </div>
                            </div>
                            <a href="" class="ipro-link--overlay ipro-kwirks__mbl-trigger"></a>
                        </div>

                    </div>
                </div>

                <?php endif;?>
			<?php if(!empty($block['blocks'][2]['acf_fc_layout']) && $block['blocks'][2]['acf_fc_layout']=='middle_column'):?>
                <div class="col-sm-4 col-xs-12 ipro-block__col ipro-block__col--noPadding" data-box="kwirks-box" data-order="2">
                    <div class="kwirks-box__row clearfix">

                        <div class="kwirks-box__inner">
                            <div class="ipro-col__content js-anim-init" data-animation="fadein">
                                <figure>
                                    <img src="<?php echo $block['blocks'][2]['background_image']['url']?>" class="ipro-valign--middle ipro-media ipro-media--full" alt="" />
                                </figure>
                                <div class="ipro-block__col-caption js-anim-init" data-animation="fadein slideInDownx">
                                    <h2>
                                        <span><?php echo $block['blocks'][2]['title']?></span>
                                        <i class="ipro-chevron ipro-chevron--right visible-xs"></i>
                                    </h2>
                                </div>
                            </div>
                            <a href="<?php echo $block['blocks'][2]['link']?>" class="ipro-link--overlay"></a>
                        </div>

                    </div>
                </div>
            <?php endif;?>

<?php if(!empty($block['blocks'][1]['acf_fc_layout']) && $block['blocks'][1]['acf_fc_layout']=='right_block'):?>
                <div class="col-sm-4 col-xs-12 ipro-block__col ipro-block__col--noPadding kwirks-expand" data-box="kwirks-box" data-order="3">
                    <div class="kwirks-box__row clearfix">

                        <div class="kwirks-box__hidden kwirks-box__hidden--list">
                            <div class="kwirks-box__hidden-content clearfix">
                                <div class="col-sm-6 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                    <?php if(!empty($block['blocks'][1]['1st_column'])):
                                    			foreach($block['blocks'][1]['1st_column'] as $product):
                                    		?>
                                       			 <li><a href=""><?php echo $product['text']?></a></li>
                                    <?php 		endforeach;
                                    	endif;?>
                                       
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-xs-12 kwirks-box__hidden-col">
                                    <ul class="kwirks-box__hidden-group">
                                        <?php if(!empty($block['blocks'][1]['2nd_column'])):
                                    			foreach($block['blocks'][1]['2nd_column'] as $product):
                                    		?>
                                       			 <li><a href=""><?php echo $product['text']?></a></li>
                                    <?php 		endforeach;
                                    	endif;?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="kwirks-box__inner">
                            <div class="ipro-col__content js-anim-init" data-animation="fadein">
                                <figure>
                                    <img src="<?php echo $block['blocks'][1]['background_image']['url']?>" class="ipro-valign--middle ipro-media ipro-media--full" alt="" />
                                </figure>
                                <div class="ipro-block__col-caption js-anim-init" data-animation="fadein slideInDownx">
                                    <h2>
                                        <span><?php echo $block['blocks'][1]['title']?></span>
                                        <i class="ipro-chevron ipro-chevron--right visible-xs"></i>
                                    </h2>
                                </div>
                            </div>
                            <a href="" class="ipro-link--overlay ipro-kwirks__mbl-trigger"></a>
                        </div>

                    </div>
                </div>
                <?php endif;?>

            </div><!-- /.# NGL one thirds grid wrap -->

        </div><!-- /.#NGL block row -->
    </section><!-- /.#NGL CMS blocks :: Home page kwirks effect box section -->

<?php endif;?>