    <!-- NGL CMS blocks :: Shortcodes two half - Image/Text with large gap -->    
    <section class="ipro-block ipro-block--twoHalf ipro-block--full js-anim-init" data-background="false" data-animation="fadein">
        <div class="ipro-block__row">

            <div class="clearfix ipro-row ipro-flex">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--img text-center">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInDown">
                        <figure>
                            <img src="<?php echo $block['image_at_left']['url']?>" class="ipro-valign--middle" alt="" />
                        </figure>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--text text-left">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInRight">
                        <blockquote><?php echo $block['citation_1']?></blockquote>
                        <h3 class="ipro__h2 ipro__h2--author"><?php echo $block['citation_1_from']?></h3>
                    </div>
                </div>

            </div><!-- /.# NGL tow half grid wrap -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: Shortcodes two half - Image/Text with large gap -->


  
    <!-- NGL CMS blocks :: Shortcodes two half - Image/Text[Inverted] with large gap -->
    <section class="ipro-block ipro-block--twoHalf ipro-block--full ipro-block--invert js-anim-init" data-background="false" data-animation="fadein">
        <div class="ipro-block__row">

            <div class="clearfix ipro-row ipro-flex">

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--img text-center">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInDown">
                        <figure>
                            <img src="<?php echo $block['image_at_right']['url']?>" class="ipro-valign--middle" alt="" />
                        </figure>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--center ipro-block__gap--large ipro-block--text text-right">
                    <div class="ipro-col__content js-anim-init" data-animation="fadein slideInLeft">
                        <blockquote><?php echo $block['citation_2_'];?></blockquote>
                        <h3 class="ipro__h2 ipro__h2--author"><?php echo $block['citation_2_from']?></h3>
                    </div>
                </div>

            </div><!-- /.# NGL tow half grid wrap -->

        </div><!-- /.# NGL container -->
    </section><!-- /.#NGL CMS blocks :: Shortcodes two half - Image/Text[Inverted] with large gap -->

