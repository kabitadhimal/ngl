   <section class="ipro-block ipro-block--full ipro-block--<?php echo $block['background_color']=='white'?'white':'grey' ?>  ipro-block--simpleText text-center" data-theme="theme-palma-stormgrey">
                        <div class="ipro-block__row">
                            <div class="ipro-container ipro-container--text">

                                <div class="ipro-block__text ipro-block__text--light js-anim-init" data-delay="0.25" data-animation="fadeIn">
                                    <h2><?php echo $block['title'] ?></h2>
                                    <h5><em><?php echo $block['sub_title'] ?></em></h5>
                                    <?php echo $block['description'] ?>
                                   <?php if(!empty($block['button_text'])): ?>
                                    <a href="<?php echo $block['button_link'] ?>" class="btn btn--<?php echo $block['choose_button_type']=='blue'?'goldenrod':'heather'?>  btn-gapTop--large btn--medium"><?php echo $block['button_text'] ?></a>
                                    <?php endif;?>
                                </div>

                            </div><!-- /.#NGL CMS block container -->
                        </div><!-- /.#NGL CMS block row -->
                    </section><!-- /.#NGL CMS block :: Simple Text -->