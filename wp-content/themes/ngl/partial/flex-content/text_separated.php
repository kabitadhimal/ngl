<section class="ipro-block ipro-block--twoHalfSepText ipro-block--full js-anim-init" data-background="false" data-animation="fadein">
    <div class="ipro-block__row">

        <div class="clearfix ipro-row ipro-flex">

            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--fstart text-right ipro-block__gap--sepText ipro-block--bkg" data-color="<?php echo $block['background_color']?>">
                <div class="ipro-col__content js-anim-init" data-delay="0.025" data-animation="fadein slideInLeft">
                    <h2><?php echo $block['title']?></h2>
                    <?php echo $block['description']?>
                </div>
            </div>

            <div class="col-sm-6 col-xs-12 ipro-flex__col ipro-flex__col--fstart text-left ipro-block__gap--sepText ipro-block--text" data-color="<?php echo $block['background_color_right_column']?>">
                <div class="ipro-col__content js-anim-init" data-delay="0.025" data-animation="fadein slideInRight">
                    <h2><?php echo $block['title_right_column']?></h2>
                    <?php echo $block['description_right_column']?>
                </div>
            </div>

        </div><!-- /.# NGL tow half grid wrap -->

    </div><!-- /.# NGL container -->
</section>