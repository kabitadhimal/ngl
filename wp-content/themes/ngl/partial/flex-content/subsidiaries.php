 <!-- NGL CMS blocks :: Home page map section -->
    <section class="ipro-block ipro-block--map ipro-block--full js-anim-init text-center" data-theme="theme-palma" data-delay="0.15" data-animation="fadein" style="background: url(<?php echo TEMP_DIR_URI ?>/images/map-bkg-patter.png);">
        <div class="ipro-block__row">

            <!-- NGL homepage map row -->
            <div class="ipro-block--map__row">

                <figure>
                    <img src="<?php echo TEMP_DIR_URI ?>/images/map-bkg.png" class="ipro-valign--middle map-image" alt="" />
                </figure>

                <!-- Map marker group -->
                <div id="ipro-block--map__marker" class="ipro-block--map__marker">


                    <?php if(!empty($block['add_subsidiary'])):
                    ?>

                    <?php
                        foreach($block['add_subsidiary'] as $ind=>$map):
                            ?>
                         <a href="" class="icon icon-map-pin map__marker map__marker--<?php echo $ind==0?'geneva':''?> map__marker--<?php echo $ind==0?'large':'small'?> map__marker--<?php echo $ind==0?'goldenrod':'palma'?> js-anim-init" data-delay="<?php echo $ind==0?'0.2':'0.7'?>" data-animation="fadeIn"  style="top:<?php echo $map['x_co-ordinates']?>%;left:<?php echo $map['y_co-ordinates']?>%"></a>
                            <?php endforeach;
                    endif;?> 
                   
                    
                </div><!-- Map marker group -->

                <!-- Map caption -->
                <div class="ipro-block--map__caption js-anim-init" data-delay="0.025" data-animation="fadein slideInDown">
                    <div id="ipro-block--map__title" class="ipro-block--map__title" data-delay="3000">
                        <span class="ipro-title ipro-title--big">DECOUVREZ NGL GROUP</span>
                    </div>
                    <div class="ipro-block--map__button">
                        <a href="" class="btn btn--large btn--goldenrod">Découvrez NGL Group</a>
                    </div>
                </div><!-- /.#Map caption -->
            </div><!-- /.#NGL homepage map row -->

        </div><!-- /.#NGL block row -->
    </section><!-- /.#NGL CMS blocks :: Home page map section -->