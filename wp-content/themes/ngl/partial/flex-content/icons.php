  <!-- NGL CMS blocks :: Shortcodes pictos with title -->
                    <section class="ipro-block ipro-block--picto ipro-block--heather text-center" data-theme="theme-default">
                        <div class="ipro-container ipro-container--main">
<div class="ipro__slider ipro__slider--mobile" data-delay="3000">
                            <div class="row clearfix ipro-row ipro-flex swiper-wrapper ipro-slider__swiper-wrapper">
                                <?php if(!empty($block['select_icons'])):
                                    foreach ($block['select_icons'] as $icon):
                                                ?>
                                        <div class="col-sm-3 col-xs-12 ipro-flex__col swiper-slide ipro__slider-slide js-anim-init" data-delay="" data-animation="fadeIn slideInDown">
                                            <div class="ipro-col__content">
                                                <a href="<?php echo $icon['link']?>">
                                                    <figure class="ipro-block--picto__circle">
                                                        <i class="icon <?php echo $icon['icon']?>"></i>
                                                        <figcaption><span><?php echo $icon['title']?></span></figcaption>
                                                    </figure>
                                                </a>
                                            </div>
                                        </div>
                                       
                                   <?php endforeach;
                                ?>
                               
                            <?php endif;?>

                                </div>

                                 <!-- Slider controls -->
                                        <div class="swiper-button-prev ipro__slider-button ipro__slider-button--prev"></div>
                                        <div class="swiper-button-next ipro__slider-button ipro__slider-button--next"></div>

                            </div><!-- /.# NGL CMS blocks :: Picto row -->

                        </div>
                    </section><!-- /.#NGL CMS blocks :: Shortcodes pictos with title -->