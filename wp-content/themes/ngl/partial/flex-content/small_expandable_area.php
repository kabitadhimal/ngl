 <section class="ipro-block ipro-block--accordion ipro-block--accordionSmall">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL accordion UI -->
                            <div class="ipro-accordion ipro-accordion--custom js-anim-init" data-delay="0.5" data-animation="fadeIn">

                                <?php if(!empty($block['expandable_area'])):
                                foreach($block['expandable_area'] as $ind=>$expandable_area):
                                    ?>
                                     <div class="ipro-accordion__col ipro-accordion__col--vAlign">
                                    <div class="ipro-accordion__panel">
                                        <div class="ipro-accordion__panel-heading">
                                            <?php //echo $ind=='0'?'closer':'opener'?>
                                            <h4><a href="" class="opener toggler" data-collapse="ipro-accordion__targetPanel"><?php echo $expandable_area['title']?></a></h4>
                                        </div>
                                        <!-- ipro-accordion__panelBody<?php //echo $ind=='0'?'--active':''?> -->
                                        <div class="ipro-accordion__panelBody ipro-accordion__targetPanel">
                                            <div class="ipro-accordion__content">
                                                <?php echo $expandable_area['description']?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            

                                    <?php endforeach;
                                endif;?>
                                        

                            
                                
                            </div><!-- /.#NGL accordion UI -->

                        </div><!-- /.#NGL CMS block container -->
                    </section>