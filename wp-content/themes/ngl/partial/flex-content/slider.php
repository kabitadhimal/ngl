<!-- NGL home page hero slider/banner -->
<section id="ipro-home-heroSlider" class="ipro-heroSlider ipro-heroSlider--home text-center" data-theme="theme-default">
    <div class="ipro-slider__row">

        <!-- If users/clients opt to display image slider then show this slider programtically -->
        <!-- NGL home page slider row -->
        <div class="swiper-container ipro-slider ipro-slider--full ipro-slider--loading" data-autoplay-speed="6000">
            <div class="swiper-wrapper ipro-slider__swiper-wrapper">
                <!-- NGL slider slides group -->
                <?php
                if (!empty($block['home_slider'])):
                    foreach ($block['home_slider'] as $slider):
                        ?>
                        <div class="ipro-slider__slide swiper-slide">
                            <img src="<?php echo $slider['image']['url'];?>" class="ipro-valign--middle" alt="" />
                            <div class="ipro-slider__caption">
                                <h1><?php echo $slider['title'];?></h1>
                                <h6><?php echo $slider['text'];?></h6>
                                <a href="<?php echo $slider['button_link'];?>" class="btn btn--stromGrey btn--large"><?php echo $slider['button'];?></a>
                            </div>
                        </div>

                        <?php
                    endforeach;
                    ?>


                <?php endif; ?>

                
            </div>

            <!-- Slider pagination -->
            <div class="swiper-pagination ipro-slider__pagination"></div><!-- /.# slider pagination -->

            <!-- Slider controls -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>

        </div><!-- NGL home page slider row -->

    </div>
</section>

<!-- /.#NGL home page hero slider/banner -->