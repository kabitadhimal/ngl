
<?php get_header(); ?>

   <?php while(have_posts()): the_post(); ?>
   	  <!-- NGL news details title banner -->
                    <section class="ipro-banner ipro-block--loggedIn ipro-banner--account">
                        <div class="ipro-container ipro-container--main">

                            <!-- NGL news details banner title -->
                            <div class="ipro-banner__title text-center">
                                <h2><?php the_title();?></h2>
                                <p><em><?php the_content();?></em></p>
                            </div><!-- /.#NGL news details banner title block -->

                   




 </section>
    <?php endwhile; ?>
 
<?php get_footer(); ?>