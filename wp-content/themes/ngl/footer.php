</main><!-- /.#NGL CMS block wrap -->
        <footer id="ipro-colphon" class="ipro-footer ipro-footer--main" role="contentinfo">
            <div class="ipro-container ipro-container--main">

                <!-- NGL footer - Top copy row -->
                <div class="ipro-footer__row ipro-footer__row--top">

                    <!-- NGL CMS blocks :: Shortcodes pictos with title -->
                    <section class=" ipro-block ipro-block--picto" data-theme="theme-default">
                        <div class="ipro-container ipro-container--main">
                        <div class="ipro__slider ipro__slider--mobile" data-delay="3000" data-collapse-parent="true">
                            <div class="row clearfix ipro-row ipro-flex swiper-wrapper ipro-slider__swiper-wrapper">
                                <?php $footer_options = get_field('footer_section','option');
                                        $delay_arr = array('0.075','0.080','0.085','0.090');
                                    //print_r($footer_options);
                                   if($footer_options):
                                        foreach($footer_options as $ind=>$option):
                                ?>
                                <div class="col-sm-3 col-xs-12 ipro-flex__col swiper-slide ipro__slider-slide js-anim-init" data-delay="<?php echo $delay_arr[$ind]?>" data-animation="fadeIn slideInDown">
                                    <div class="ipro-col__content">
                                        <a href="<?php echo $option['link']?>" <?php echo $option['icon']=='icon-linkedin'?'target="_blank"':''?> <?php echo $ind === 3 ? 'data-collapse-target="ipro__collapsible--newsletter"' : '' ?>>
                                            <figure class="ipro-block--picto__circle">
                                                <i class="icon <?php echo $option['icon']?>"></i>
                                                <figcaption><span><?php echo $option['title']?></span></figcaption>
                                            </figure>
                                        </a>
                                    </div>
                                </div>
                                     <?php endforeach;
                                    endif;
                            ?>
                            </div>

                                <!-- Slider controls -->
                                <div class="swiper-button-prev ipro__slider-button ipro__slider-button--prev"></div>
                                <div class="swiper-button-next ipro__slider-button ipro__slider-button--next"></div>

                                <?php if($ind === 3) : ?>
                                    <!-- Newsletter block -->
                                    <div class="ipro__collapsible ipro__collapsible--newsletter">
                                        <span class="close icon icon-abs icon-close"></span>
                                        <div class="newsletter-form__wrap">
                                            <script type="text/javascript">
                                    /*        //<![CDATA[
                                            if (typeof newsletter_check !== "function") {
                                            window.newsletter_check = function (f) {
                                                var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                                                if (!re.test(f.elements["ne"].value)) {
                                                    alert("The email is not correct");
                                                    return false;
                                                }
                                                for (var i=1; i<20; i++) {
                                                if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                                                    alert("");
                                                    return false;
                                                }
                                                }
                                                if (f.elements["ny"] && !f.elements["ny"].checked) {
                                                    alert("You must accept the privacy statement");
                                                    return false;
                                                }

                                                return true;
                                            }
                                            }
                                            //]]>*/
                                            </script>

                                            <?php
                                            /*
                                             * https://www.thenewsletterplugin.com/documentation/newsletter-forms
                                             */
                                            ?>

                                            <div class="tnp-wrap tnp-subscription-wrap">

                                                      <div class="ipro-form__group">
                                                          <?php $newsLetterFormTitle = get_field("newsletter_title","options");
                                                            if(!empty($newsLetterFormTitle)) { echo '<h2>'.$newsLetterFormTitle.'</h2>' ;}
                                                          $newsLetterForm = get_field("newsletter_form","options");
                                                           $newsLetterFormID = $newsLetterForm[0]->ID;
                                                           $newsLetterTitle = $newsLetterForm[0]->post_title ;
                                                         /*  if(!empty($newsLetterFormID)):
                                                                echo do_shortcode( '[contact-form-7 id="'.$newsLetterFormID.'" title="'.$newsLetterTitle.'" html_class="newsletter-form"]' );
                                                           endif*/;
                                                          ?>

                                                          <?php include "src/newsletter-subsription-form.php"; ?>

                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div><!-- /.# NGL CMS blocks :: Picto row -->

                            <div class="section-sitemap">
                                <div class="row">
                                    <?php $contactInfo = get_field('contact_info','option');
                                    if(!empty($contactInfo)):
                                    ?>
                                        <div class="col-sm-6 js-anim-init" data-delay="0.10" data-animation="fadeIn slideInDown" >
                                            <h3><?php _e("Contact","app"); ?></h3>
                                            <address><?php echo $contactInfo; ?></address>
                                        </div>
                                    <?php endif; ?>
                                    <div class="col-sm-6 js-anim-init" data-delay="0.15" data-animation="fadeIn slideInDown">
                                        <h3> <?php _e("Sitemap","app"); ?></h3>
                                        <ul>
                                        <?php
                                        wp_nav_menu( array(
                                            'theme_location'=>'footer_menu',
                                            'menu_class'=>'',
                                            'container' => false ,
                                            'items_wrap'=>'%3$s'
                                        ) ); ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section><!-- /.#NGL CMS blocks :: Shortcodes pictos with title -->
                </div><!-- /.#NGL footer - Top row -->

                <div class="ipro-footer__row ipro-footer__row--bottom">
                    <ul class="ipro-footer__copyright-group js-anim-init" data-delay="0.05" data-animation="fadeIn slideInDown">
                        <li>Copyright <?php echo date('Y')?> - NGL Group</li>
                        <li>Be sure to take a look at our <a href="">Terms of Use </a> and <a href="">Privacy Policy</a> </li>
                        <li>Website by <a href="https://www.procab.ch/" target="_blank">Procab</a></li>
                    </ul>
                </div><!-- /.#NGL footer - Bottom copy right info row -->

            </div><!-- /.# NGL container -->
        </footer><!-- /.#NGL footer --><!-- /.#NGL footer -->

    </div><!-- /.#NGL inner wrapper -->
</div><!-- /.#NGL outer wrapper -->

    <!-- NGL back to top -->

    <div class="ipro__backTop-wrapper">
        <a href="" id="ipro__backTop" class="ipro__backTop"><i class="top__arrow"></i></a>
    </div><!-- /.#NGL back to top -->

    <?php if(is_page_template('templates/create_account.php') || is_page_template('templates/my-account.php') || is_page_template('templates/reset_password.php') || is_page_template('templates/forget_password.php')): ?>
         <!-- NGL filter listing page preloader -->
    <div class="ipro__loader-wrap">
        <div class="loader">Loading...</div>
    </div><!-- /.#NGL filter listing page preloader -->
    <?php endif;?>
<?php wp_footer(); ?>

<script>
    jQuery(document).ready(function(jQuery){
        jQuery("#company").prop('required',true);
        jQuery("#lname").prop('required',true);
        jQuery("#fname").prop('required',true);
        jQuery("#country").prop('required',true);
        jQuery("#email").prop('required',true);
        jQuery("#message").prop('required',true);
        jQuery("#companynewsletter").prop('required',true);
        jQuery("#lastnamenewsletter").prop('required',true);
        jQuery("#fnamenewsletter").prop('required',true);
        jQuery("#emailnewsletter").prop('required',true);
        
        
       jQuery(".wpcf7-form").removeAttr('novalidate');
    });

<?php if(!is_page_template('templates/news.php')):?>
        $(function(){
            $('.ipro__loader-wrap').hide();
        });
        <?php endif;?>
    /*Google tracking code*/
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-26232120-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>
