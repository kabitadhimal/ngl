$(window).on('scroll', function() {
    //return false;
    var scrollTop = $(this).scrollTop();
    var windowHeight = $(window).height();
    var headerHeight = $('#ipro-masthead').outerHeight();

    //var scrollPercent = 100 * $(window).scrollTop() / ($(document).height() - $(window).height());
    //console.log(scrollPercent);


    $('.concept-block').each(function() {
        var $self = $(this);
        var topDistance = $(this).offset().top;
        var bottomDistance = topDistance + $(this).outerHeight() - windowHeight;

        var scrollPercent = ($(window).scrollTop() + headerHeight - $self.offset().top) / ($(this).outerHeight() - $(window).height());
        var $container = $(this).find('.ipro-block__row');
        var scrollPercentY = ($(window).scrollTop() + (headerHeight * 2.15) - $self.offset().top) / ($(this).outerHeight() - $(window).height());

        //var $clipper = $('#qualtyClipRectTop');
        // var clipperXPos = 900 - scrollPercent * 900;

        // $clipper.css({
        //     y: -clipperXPos + 'px'
        // });

        //var count = $self.attr('data-items');
        $self.find(".active").removeClass('active');
        $self.find(".concept__picto").removeClass('active');

        if(scrollPercent > 0.5){
            $self.find(".concept__picture--second").addClass('active');
        }else{
            $self.find(".concept__picture--first").addClass('active');
        }

        if(scrollPercentY > 0.5){            
            $self.find(".concept__picto").addClass('active');
        }else{
            $self.find(".concept__picto").removeClass('active');
        }


        //console.log(topDistance + " " + scrollTop)



        var yPos =  0;

        if (topDistance <= scrollTop && bottomDistance >=scrollTop ) {
            //console.log('lock')
        }else{
            if(topDistance >scrollTop){
                yPos = $self.offset().top;
            }else{
                yPos = $self.offset().top + $self.outerHeight() - $(window).height()
            }
            yPos -= scrollTop;


        }

        $container.css({
            // 'transform':'translateY('+yPos+'px)',
            // '-webkit-transform':'-webkit-translateY('+yPos+'px)'
        });
    });
});