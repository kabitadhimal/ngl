<?php class Walker_Quickstart_Menu extends Walker_Nav_Menu {
    // add classes to ul sub-menus

    function start_lvl( &$output, $depth ) {
        // depth dependent classes

        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent

        // build html

        if($depth >= 1) {

            $output .= "\n" . $indent . ' <ul class="lesrcoh-megaitem-list js-row">' . "\n";

        }else {

            $output .= "\n" . $indent . ' <div class="lesroch-megamenu-wrapper"><ul class="lesroch-megamenu-dropdown megamenu-dropdown"><div class="lesroch-megamenu-container"><div class="row clearfix lesroch-row">' . "\n";
        }
    }

    // add main/sub classes to li's and links

    function start_el( &$output, $item, $depth, $args ) {

        global $wp_query;

        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        $depth_classes = 'menu-item-depth-' . $depth.' col-xs-12 lesroch-megaitem-col same-height-col js-last-col ';
        //$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes

        $dClass = "col-xs-12 lesroch-megaitem-col same-height-col js-last-col";
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        // build html
        //  echo $class_names.'<br/>';
        if(in_array('menu-item-has-children',$item->classes) && ($depth>=1)) {
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $dClass . ' ' . $class_names . ' lesroch-megaitem">';
        } else if (in_array("menu-item-has-children", $classes)) {
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . ' has-megamenu">';
        } else if(in_array("sub-sub-menu-item", $classes)){
        }else {

            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
        }
        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        if(in_array('menu-item-has-children',$item->classes) && ($depth>=1)){
            $item_output = sprintf( '<h2>%1$s</h2>',
                apply_filters( 'the_title', $item->title, $item->ID )

                );
        } else {
            $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
                $args->before,
                $attributes,
                $args->link_before,
                apply_filters( 'the_title', $item->title, $item->ID ),
                $args->link_after,
                $args->after
                );
        }
        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}